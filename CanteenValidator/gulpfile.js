﻿/// <binding AfterBuild='minify' />
var gulp = require("gulp");
var uglify = require("gulp-uglify");
var concat = require("gulp-concat");

// minify Javascript
function minify() {
    return gulp.src(["wwwroot/js/shared/*.js"])
        .pipe(concat("canteen.min.js"))
        .pipe(uglify())
        .pipe(gulp.dest("wwwroot/dist/"));
}

function minify_specific() {
    return gulp.src(["wwwroot/js/*.js"])
        .pipe(uglify())
        .pipe(gulp.dest("wwwroot/dist/"));
}

//minify CSS
function styles() {
    return gulp.src(["wwwroot/css/**/*.css"])
        .pipe(concat("canteen.min.css"))
        .pipe(uglify())
        .pipe(gulp.dest("wwwroot/dist/"));
}

exports.minify = minify;
exports.minify_specific = minify_specific;
exports.styles = styles;

exports.default = gulp.parallel(minify, minify_specific);