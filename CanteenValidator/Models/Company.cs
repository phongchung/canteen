﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CanteenValidator.Models
{
    public partial class Company
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
