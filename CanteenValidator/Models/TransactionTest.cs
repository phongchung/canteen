﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CanteenValidator.Models
{
    public partial class TransactionTest
    {
        public int Id { get; set; }
        public DateTime? Date { get; set; }
        public string StaffId { get; set; }
        public int? Used { get; set; }
        public int? Available { get; set; }
    }
}
