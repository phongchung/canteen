﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CanteenValidator.Models
{
    public class EditStaffViewModel
    {
        public int staffID { get; set; }
        public string name { get; set; } 
        public string role { get; set; }
        public int departmentID { get; set; }
        public int companyID { get; set; }
        public DateTime startOfDate { get; set; }
        public int staffType { get; set; }
        public string email { get; set; }
        public string id { get; set; }
    }
}
