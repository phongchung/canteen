﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CanteenValidator.Models
{
    public partial class IndexViewModel
    {
        public int Count { get; set; }
        public List<TransactionsViewModel> Transactions { get; set; }
    }
}
