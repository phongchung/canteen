﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CanteenValidator.Models
{
    public class SearchSpecificModel
    {
        public string company { get; set; } 
        public int staffID { get; set; } 
        public DateTime date { get; set; }

    }
}
