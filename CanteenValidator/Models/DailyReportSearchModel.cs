﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CanteenValidator.Models
{
    public class DailyReportSearchModel
    {
        public int companyID { get; set; }
        public int month { get; set; }
        public int year { get; set; }
    }
}
