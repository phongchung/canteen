﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CanteenValidator.Models
{
    public partial class HolidayViewModel
    {
        public int Id { get; set; }
        public DateTime FromDate { get; set; }

        public DateTime ToDate { get; set; }

        public string FromDay { get; set; }

        public string ToDay { get; set; }

        public int TotalDays { get; set; }

        public string Description { get; set; }
    }
}
