﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CanteenValidator.Models
{
    public partial class TicketType
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
