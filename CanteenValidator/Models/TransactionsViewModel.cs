﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CanteenValidator.Models
{
    public class TransactionsViewModel
    {
        public int ID { get; set; }

        [DisplayName("Công ty")]
        public string Company { get; set; }

        [DisplayName("Phòng ban")]
        public string Department { get; set; }

        [DisplayName("Họ tên")]
        public string Name { get; set; }

        [DisplayName("Thời gian")]
        public DateTime Date { get; set; }

        [DisplayName("Số phiếu đã dùng")]
        public int? Used { get; set; }

        [DisplayName("Số phiếu còn lại")]
        public int? Available { get; set; }

        public string Type { get; set; }

        public int StaffID { get; set; }

        public int CounterID { get; set; }

    }
}
