﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CanteenValidator.Models
{
    public partial class Transaction
    {
        public int Id { get; set; }
        public DateTime CheckDate { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public int StaffId { get; set; }
        public int? Used { get; set; }
        public int? Available { get; set; }
        public int? TicketTypeId { get; set; }
        public string Note { get; set; }
        public int CounterID { get; set; }     
    }
}
