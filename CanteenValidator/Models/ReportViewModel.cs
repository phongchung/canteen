﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CanteenValidator.Models
{
    public class ReportViewModel
    {
        public string Name { get; set; }
        public int? Used { get; set; }
        public int? Available { get; set; }
        public string Company { get; set; }
        public string Department { get; set; }
        public string Type { get; set; }

    }
}
