﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace CanteenValidator.Models
{
    public class SearchViewModel
    {
        [Required]
        [Display(Name = "Company")]
        public string SelectedCompanyCode { get; set; }
        public List<Company_Search> Companies { get; set; }

        [Required]
        [Display(Name = "Department")]
        public string SelectedDepartmentCode { get; set; }
        public List<Department> Departments { get; set; }

        [Required]
        [Display(Name = "StaffName")]
        public string SelectedNameCode { get; set; }
        public List<StaffName> StaffNames { get; set; }

        [Required]
        [Display(Name = "Type")]
        public string SelectedTypeCode { get; set; }
        public List<ConsumerType> Types{ get; set; }

        //[Required]
        //[Display(Name = "Day")]
        //public string SelectedDayCode { get; set; }
        //public IEnumerable<SelectListItem> Days
        //{
        //    get
        //    {
        //        List<int> days = new List<int>();
        //        int totalDay = DateTime.DaysInMonth(int.Parse(SelectedYearCode), int.Parse(SelectedMonthCode));
        //        days.Add(-1);
        //        for(int i = 1; i <= totalDay; i++)
        //        {
        //            days.Add(i);
        //        }
        //        return days.Select(dayValue => new SelectListItem
        //        {

        //            Value = dayValue.ToString(),
        //            Text = dayValue == -1 ? "Ngày" : "Ngày " + dayValue.ToString()
        //        });
        //    }
        //}

        [Required]
        [Display(Name = "Month")]
        public string SelectedMonthCode { get; set; }
        public IEnumerable<SelectListItem> Months
        {
            get
            {
                List<int> months = new List<int>();
                for (int i = 0; i < 12; i++)
                {
                    months.Add(i);
                }
                return months.Select(index => new SelectListItem
                {
                    Value = (index + 1).ToString(),
                    Text = "Tháng " + (index + 1).ToString()
                });

            }
        }

        [Required]
        [Display(Name = "Years")]
        public string SelectedYearCode { get; set; }
        public IEnumerable<SelectListItem> Years
        {
            get
            {
                DateTime localDate = DateTime.Now;
                List<int> year = new List<int>();

                for(int i = localDate.Year - 1; i <= localDate.Year; i++)
                {
                    year.Add(i);
                }

                return year.Select(yearValue => new SelectListItem
                {
                    Value = yearValue.ToString(),
                    Text = "Năm " + yearValue.ToString()
                });

            }
        }
    }
}
