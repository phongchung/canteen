﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CanteenValidator.Models
{
    public partial class WorkDaysOfMonth
    {
        public int WorkMonth { get; set; }
        public int WorkDays { get; set; }
    }
}
