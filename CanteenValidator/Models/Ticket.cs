﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CanteenValidator.Models
{
    public partial class Ticket
    {
        public int Id { get; set; }
        public int StaffId { get; set; }
        public int GrantedMonth { get; set; }
        public int GrantedYear { get; set; }
        public int Granted { get; set; }
        public int Used { get; set; }
        public int Available { get; set; }
    }
}
