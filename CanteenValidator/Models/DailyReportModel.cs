﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CanteenValidator.Models
{
    public class DailyReportModel
    {
        public DailyReportModel()
        {
            UsedList = new List<List<int>>();
            CompanyList = new List<Company>();
        }

        public List<List<int>> UsedList { get; set; }

        public List<Company> CompanyList { get; set; }
    }
}
