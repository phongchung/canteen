﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CanteenValidator.Models
{
    public class MonthlyReportModel
    {
        public string Company { get; set; }

        public int Used { get; set; }
    }
}
