﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CanteenValidator.Models
{
    public class Company_Search
    {
        public int Code { get; set; }

        public string Name { get; set; }

    }

    public class Department
    {
        public int Code { get; set; }
        
        public string Name { get; set; }
    }

    public class StaffName
    {
        public int Code { get; set; }

        public string Name { get; set; }
    }

    public class ConsumerType
    {
        public int Code { get; set; }

        public string Name { get; set; }
    }
}
