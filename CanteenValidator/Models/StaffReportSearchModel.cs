﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CanteenValidator.Models
{
    public class StaffReportSearchModel
    {
        public int companyID { get; set; }
        public int departmentID { get; set; }
        public int typeID { get; set; }
        public DateTime fromDate { get; set; }
        public DateTime toDate { get; set; }
    }
}
