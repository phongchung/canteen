﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CanteenValidator.Models
{
    public class SearchModel
    {
        public SearchModel()
        {
            SearchViewModel = new SearchViewModel();
            TransactionsViewModel = new List<TransactionsViewModel>();
        }

        public SearchViewModel SearchViewModel { get; set; }

        public List<TransactionsViewModel> TransactionsViewModel { get; set; }
        public List<Staff> Staff { get; set; }

    }
}
