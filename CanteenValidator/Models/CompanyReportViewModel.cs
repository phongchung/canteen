﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CanteenValidator.Models
{
    public class CompanyReportViewModel
    {
        public int? Used { get; set; }
        public string Company { get; set; }
        public string Type { get; set; }
    }
}
