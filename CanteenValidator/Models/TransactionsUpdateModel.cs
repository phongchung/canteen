﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CanteenValidator.Models
{
    public class TransactionsUpdateModel
    {
        public string QRCode { get; set; }

        public DateTime Date { get; set; }

        public int CounterID { get; set; }

    }
}
