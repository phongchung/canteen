﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CanteenValidator.Models
{
    public partial class Staff
    {
        public int Id { get; set; }
        public string QRHash { get; set; }
        public string QrcodeId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Role { get; set; }
        public DateTime? StartOfDay { get; set; }
        public string CompanyId { get; set; }
        public string Department { get; set; }
        public int StaffType { get; set; }
    }
}
