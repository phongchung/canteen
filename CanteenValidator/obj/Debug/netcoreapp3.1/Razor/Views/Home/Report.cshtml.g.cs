#pragma checksum "C:\Users\Lancelot\Downloads\CanteenValidator\CanteenValidator\CanteenValidator\Views\Home\Report.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "2a0e2e7f0c091c4c6e19b6ad7474b60288e52b42"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Report), @"mvc.1.0.view", @"/Views/Home/Report.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\Lancelot\Downloads\CanteenValidator\CanteenValidator\CanteenValidator\Views\_ViewImports.cshtml"
using CanteenValidator;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\Lancelot\Downloads\CanteenValidator\CanteenValidator\CanteenValidator\Views\_ViewImports.cshtml"
using CanteenValidator.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"2a0e2e7f0c091c4c6e19b6ad7474b60288e52b42", @"/Views/Home/Report.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"0111a16e1ce3f2f8d555ab6356b913f9d59f86d5", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Report : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/lib/jquery-ajax-unobtrusive/jquery.unobtrusive-ajax.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("<input hidden id=\"home\" value=\"0\" />\r\n");
#nullable restore
#line 2 "C:\Users\Lancelot\Downloads\CanteenValidator\CanteenValidator\CanteenValidator\Views\Home\Report.cshtml"
  
    ViewData["Title"] = "Báo cáo";

#line default
#line hidden
#nullable disable
            WriteLiteral(@"<input hidden id=""pageNumber"" value=""1"" />

<div id=""page-content-wrapper"">
    <div class=""container px-0 ml-0"" style=""max-width: 100%;"">
        <main role=""main"" style=""height: 100%"">
            <div class=""d-block d-md-none header"">

                <div class=""row px-1 px-md-3 mr-0"">
                    <div class=""dropdown"" style=""width:100%;"">
                        <button id=""sidebarToggleTop"" class=""dropbtn dropopen d-md-none mr-3"">
                            <i class=""fa fa-bars""></i>
                            <span>Quản lý phiếu ăn</span>
                        </button>
                    </div>
                </div>
                <div style=""height: 10px; min-height: 10px; background-color: #254b8b; height: 1.5vh""></div>
            </div>

            <div class=""mainwindow mx-2 mt-2 pb-0"" id=""report-container"">
");
            WriteLiteral(@"                <div>
                    <ul class=""nav nav-tabs justify-content-start"" style=""border-bottom:solid 2px black"">
                        <li class=""nav-item"">
                            <a class=""nav-link active"" data-toggle=""tab"" role=""tab"" aria-controls=""StaffReport""
                               href=""#StaffReport"">Báo cáo theo nhân viên</a>
                        </li>
                        <li class=""nav-item"">
                            <a class=""nav-link"" data-toggle=""tab"" role=""tab"" aria-controls=""CompanyReport""
                               href=""#CompanyReport"">Báo cáo theo công ty</a>
                        </li>
                    </ul>
                </div>

                <div class=""tab-content"" id=""myTabContent"">
                    <div class=""tab-pane fade show active"" id=""StaffReport"" role=""tabpanel"" aria-labelledby=""home-tab"">

                    </div>
                    <div class=""tab-pane fade"" id=""CompanyReport"" role=""tabpanel"" aria-labelle");
            WriteLiteral("dby=\"profile-tab\">\r\n\r\n                    </div>\r\n\r\n                </div>\r\n            </div>\r\n        </main>\r\n    </div>\r\n</div>\r\n\r\n\r\n");
            DefineSection("scripts", async() => {
                WriteLiteral("\r\n    ");
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "2a0e2e7f0c091c4c6e19b6ad7474b60288e52b426163", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                WriteLiteral(@"
    <script>
        $(function () {
            $.ajax({
                url: ""/Home/StaffReport"",
                type: ""get"",
                success: function (result) {
                    $(""#StaffReport"").html(result);
                }
            })

            $.ajax({
                url: ""/Home/CompanyReport"",
                type: ""get"",
                success: function (result) {
                    $(""#CompanyReport"").html(result);
                }
            })

        })

        function firstCompleted() {
            $('a[href=""#StaffReport""]').tab('show');
        }
        function secondCompleted() {
            $('a[href=""#CompanyReport""]').tab('show');
        }

    </script>
");
            }
            );
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
