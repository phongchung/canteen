#pragma checksum "C:\Users\Lancelot\Downloads\CanteenValidator\CanteenValidator\CanteenValidator\Views\Home\Search.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "4c9844f0cad47226da0c91ecdea3869de9c74f30"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Search), @"mvc.1.0.view", @"/Views/Home/Search.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\Lancelot\Downloads\CanteenValidator\CanteenValidator\CanteenValidator\Views\_ViewImports.cshtml"
using CanteenValidator;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\Lancelot\Downloads\CanteenValidator\CanteenValidator\CanteenValidator\Views\_ViewImports.cshtml"
using CanteenValidator.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"4c9844f0cad47226da0c91ecdea3869de9c74f30", @"/Views/Home/Search.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"0111a16e1ce3f2f8d555ab6356b913f9d59f86d5", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Search : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<CanteenValidator.Models.SearchModel>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/js/export-excel.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/js/search.js?v=6"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 1 "C:\Users\Lancelot\Downloads\CanteenValidator\CanteenValidator\CanteenValidator\Views\Home\Search.cshtml"
  
    ViewData["Title"] = "Tra cứu";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "4c9844f0cad47226da0c91ecdea3869de9c74f304145", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(@"

<input hidden id=""pageNumber"" value=""2"" />
<input hidden id=""home"" value=""0"" />
<div id=""page-content-wrapper"">
    <div class=""container pb-0 px-0 ml-0"" style=""max-width: 100%;"">
        <main role=""main"" style=""height: 100%"">
            <div class=""d-block d-md-none header"">

                <div class=""row px-1 px-md-3 mr-0"">
                    <div class=""dropdown"" style=""width:100%;"">
                        <button id=""sidebarToggleTop"" class=""dropbtn dropopen d-md-none mr-3"">
                            <i class=""fa fa-bars""></i>
                            <span>Quản lý phiếu ăn</span>
                        </button>
                    </div>
                </div>
                <div style=""height: 10px; min-height: 10px; background-color: #254b8b; height: 1.5vh""></div>
            </div>

            <div class=""mainwindow mx-2 h-100"">
");
            WriteLiteral("                <div class=\"py-2\" style=\"background-color:white;\">\r\n                    <div class=\"row justify-content-between\">\r\n                        <div class=\"col-6 col-md-2 form-group pl-0\">\r\n                            ");
#nullable restore
#line 31 "C:\Users\Lancelot\Downloads\CanteenValidator\CanteenValidator\CanteenValidator\Views\Home\Search.cshtml"
                       Write(Html.DropDownListFor(x => Model.SearchViewModel.SelectedCompanyCode, new SelectList(Model.SearchViewModel.Companies, "Code", "Name"), htmlAttributes: new { @class = "form-control", id = "Companies" }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                            ");
#nullable restore
#line 32 "C:\Users\Lancelot\Downloads\CanteenValidator\CanteenValidator\CanteenValidator\Views\Home\Search.cshtml"
                       Write(Html.ValidationMessageFor(x => x.SearchViewModel.SelectedCompanyCode, "", new { @class = "text-danger" }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                        </div>\r\n                        <div class=\"col-6 col-md-3 form-group pr-0 pr-md-3\">\r\n                            ");
#nullable restore
#line 35 "C:\Users\Lancelot\Downloads\CanteenValidator\CanteenValidator\CanteenValidator\Views\Home\Search.cshtml"
                       Write(Html.DropDownListFor(x => Model.SearchViewModel.SelectedDepartmentCode, new SelectList(Model.SearchViewModel.Departments, "Code", "Name"), htmlAttributes: new { @class = "form-control", id = "Departments" }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                            ");
#nullable restore
#line 36 "C:\Users\Lancelot\Downloads\CanteenValidator\CanteenValidator\CanteenValidator\Views\Home\Search.cshtml"
                       Write(Html.ValidationMessageFor(x => x.SearchViewModel.SelectedDepartmentCode, "", new { @class = "text-danger" }));

#line default
#line hidden
#nullable disable
            WriteLiteral(@"

                        </div>
                        <div class=""autocomplete col-12 col-md-3 form-group px-0 px-md-3"" style=""line-height: calc(1.7rem + 0.5vw + 0.5vh)"">
                            <input class=""form-control"" type=""text"" placeholder=""Tìm kiếm họ tên.."" id=""StaffName"">
");
            WriteLiteral(@"                        </div>
                        <div class=""col-6 col-md-2 form-group pl-0 px-md-3"">
                            <input class=""form-control input-date"" type=""date"" id=""FromDate"">
                        </div>
                        <div class=""col-6 col-md-2 form-group pr-0 pl-md-3"">
                            <input class=""form-control input-date"" type=""date"" id=""ToDate"">
                        </div>
");
            WriteLiteral("                    </div>\r\n");
            WriteLiteral(@"                    <div class=""d-flex justify-content-between"">
                        <div class=""title pl-0"" style=""align-self:center;"">
                            <span class=""pr-1"">Tổng số phiếu đã dùng:</span>
                            <span id=""totalTransactions"" style=""font-weight:bold;""></span>
                        </div>
                        <div class="" px-0"" style=""margin-left:auto; display:flex;"">
                            <button type=""button"" onclick=""getStaffReport()"" style=""width:100%; line-height: initial"">
                                <i class=""fa fa-file-excel-o""></i>
                                Xuất Excel
                            </button>
");
            WriteLiteral("                        </div>\r\n                    </div>\r\n");
            WriteLiteral("                </div>\r\n\r\n");
            WriteLiteral(@"                <div class=""tableSearch"">
                    <table class=""table p-0 m-0 align-self"" id=""ListTable"" data-cols-width=""7, 15, 15, 30, 25"">
                        <thead style=""background-color:dimgray; "">
                            <tr class=""d-none"" data-height=""50"">
                                <td colspan=""5"" data-f-sz=""20"" data-a-h=""center"" data-a-v=""middle"" data-f-bold=""true"">
                                    BÁO CÁO THỐNG KÊ GIAO DỊCH
                                </td>
                            </tr>
                            <tr class=""d-none"" data-height=""35"">
                                <td id=""StatisticsReportTimeHeader"" colspan=""5"" data-f-sz=""14"" data-a-h=""center"" data-a-v=""middle"">
                                </td>
                            </tr>
                            <tr>
                                <th style="" border-bottom: 2px solid black"" data-a-h=""center"" data-f-bold=""true"" data-b-a-s=""thin"">
                                    ");
            WriteLiteral(@"<label>STT</label>
                                </th>
                                <th id=""sortTh"" onclick=""sortPaginatedTable('ListTable', 1, 'string', 2)"" style="" border-bottom: 2px solid black"" data-a-h=""center"" data-f-bold=""true"" data-b-a-s=""thin"">
                                    <label id=""sortTh"">Công ty</label>
                                    <i class=""fa fa-sort""></i>
                                </th>
                                <th id=""sortTh"" onclick=""sortPaginatedTable('ListTable', 2, 'string', 2)"" style="" border-bottom: 2px solid black"" data-a-h=""center"" data-f-bold=""true"" data-b-a-s=""thin"">
                                    <label id=""sortTh"">Phòng ban</label>
                                    <i class=""fa fa-sort""></i>
                                </th>
                                <th id=""sortTh"" onclick=""sortPaginatedTable('ListTable', 3, 'string', 2)"" style="" border-bottom: 2px solid black"" data-a-h=""center"" data-f-bold=""true"" data-b-a-s=""thin"">
    ");
            WriteLiteral("                                <label id=\"sortTh\">Họ và tên</label>\r\n                                    <i class=\"fa fa-sort\"></i>\r\n                                </th>\r\n");
            WriteLiteral(@"                                <th id=""sortTh"" onclick=""sortPaginatedTable('ListTable', 4, 'string', 2)"" style="" border-bottom: 2px solid black"" data-a-h=""center"" data-f-bold=""true"" data-b-a-s=""thin"">
                                    <label id=""sortTh"">Thời gian</label>
                                    <i class=""fa fa-sort""></i>
                                </th>
");
            WriteLiteral(@"                            </tr>
                        </thead>
                        <tbody id=""listSearch"">
                        </tbody>
                    </table>

                </div>
            </div>
        </main>
    </div>
</div>

");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "4c9844f0cad47226da0c91ecdea3869de9c74f3013121", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<CanteenValidator.Models.SearchModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
