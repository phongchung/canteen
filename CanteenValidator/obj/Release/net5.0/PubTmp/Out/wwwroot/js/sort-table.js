function sortPaginatedTable_2(n, type = 'string') {
    console.time('doSomething');

    var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
    table = document.getElementById("ListTable");
    var index = document.getElementsByClassName("active").item(0).innerHTML;
    var comp = new Intl.Collator('vn');

    switching = true;
    // Set the sorting direction to ascending:
    dir = "asc";
    /* Make a loop that will continue until
    no switching has been done: */
    while (switching) {
        // Start by saying: no switching is done:
        switching = false;
        rows = table.rows;

        /* Loop through all table rows (except the
        first, which contains table headers): */
        for (i = 1; i < (rows.length - 1); i++) {
            // Start by saying there should be no switching:
            shouldSwitch = false;
            /* Get the two elements you want to compare,
            one from current row and one from the next: */
            if (i == 1) {
                x = rows[i].getElementsByTagName("TD")[n];
                y = rows[i + 1].getElementsByTagName("TD")[n];
            }
            else {
                x = y;
                y = rows[i + 1].getElementsByTagName("TD")[n];
            }
            /* Check if the two rows should switch place,
            based on the direction, asc or desc: */
            if (type == "string") {
                if (dir == "asc") {
                    
                    if (comp.compare(x.innerHTML, y.innerHTML) > 0) {
                        // If so, mark as a switch and break the loop:
                        shouldSwitch = true;
                        break;
                    }
                } else if (dir == "desc") {
                    if (comp.compare(x.innerHTML, y.innerHTML) < 0) {
                        // If so, mark as a switch and break the loop:
                        shouldSwitch = true;
                        break;
                    }
                }
            }
            else {
                x_number = parseInt(x.innerHTML);
                y_number = parseInt(y.innerHTML);
                if (dir == "asc") {
                    if (x_number > y_number) {
                        // If so, mark as a switch and break the loop:
                        shouldSwitch = true;
                        break;
                    }
                } else if (dir == "desc") {
                    if (x_number < y_number) {
                        // If so, mark as a switch and break the loop:
                        shouldSwitch = true;
                        break;
                    }
                }
            }
        }
        if (shouldSwitch) {
            /* If a switch has been marked, make the switch
            and mark that a switch has been done: */
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
            // Each time a switch is done, increase this count by 1:
            switchcount++;
        } else {
            /* If no switching has been done AND the direction is "asc",
            set the direction to "desc" and run the while loop again. */
            if (switchcount == 0 && dir == "asc") {
                dir = "desc";
                switching = true;
            }
        }
    }
    console.timeEnd('doSomething');

    SelectPage(index - 1);
}

var dir = "desc";
function sortPaginatedTable(n, type = 'string', skip = 0) {
    console.time('doSomething');

    var table, rows, i;
    table = document.getElementById("ListTable");
    var index = document.getElementsByClassName("active").item(0).innerHTML;
    var comp = new Intl.Collator('vn');
    var max_idx = 0, min_idx = 0;
    var values = [];

    rows = table.rows;
    if (type == "string") {
        values.push("");
        for (i = 1 + skip; i < rows.length; i++) {
            values.push(rows[i].getElementsByTagName("TD")[n].innerHTML);
        }
        /* Loop through all table rows (except the
        first, which contains table headers): */
        if (window.dir == "desc") {
            for (i = 1 + skip; i < rows.length - 1; i++) {
                max_idx = i;

                for (j = i + 1; j < rows.length; j++) {
                    if (comp.compare(values[max_idx], values[j]) < 0) {
                        // If so, mark as a switch and break the loop:
                        max_idx = j;
                    }
                }
                if (max_idx != i) {
                    rows[i].parentNode.insertBefore(rows[max_idx], rows[i]);
                    values.splice(i, 0, values[max_idx]);
                    values.splice(max_idx + 1, 1);
                }
            }
            window.dir = "asc";
        }
        else if (window.dir == "asc") {
            for (i = 1 + skip; i < rows.length - 1; i++) {

                min_idx = i;

                for (j = i + 1; j < rows.length; j++) {
                    if (comp.compare(values[min_idx], values[j]) > 0) {
                        // If so, mark as a switch and break the loop:
                        min_idx = j;
                    }
                }
                if (min_idx != i) {
                    rows[i].parentNode.insertBefore(rows[min_idx], rows[i]);
                    values.splice(i, 0, values[min_idx]);
                    values.splice(min_idx + 1, 1);
                }
            }
            window.dir = "desc";
        }
    }
    else {
        values.push(-1);
        for (i = 1 + skip; i < rows.length; i++) {
            values.push(parseInt(rows[i].getElementsByTagName("TD")[n].innerHTML));
        }

        /* Loop through all table rows (except the
        first, which contains table headers): */
        if (window.dir == "desc") {
            for (i = 1 + skip; i < rows.length - 1; i++) {
                max_idx = i;

                for (j = i + 1; j < rows.length; j++) {
                    if (values[max_idx] < values[j]) {
                        // If so, mark as a switch and break the loop:
                        max_idx = j;
                    }
                }
                if (max_idx != i) {
                    rows[i].parentNode.insertBefore(rows[max_idx], rows[i]);
                    values.splice(i, 0, values[max_idx]);
                    values.splice(max_idx + 1, 1);
                }
            }
            window.dir = "asc";
        }
        else if (window.dir == "asc") {
            for (i = 1 + skip; i < rows.length - 1; i++) {

                min_idx = i;

                for (j = i + 1; j < rows.length; j++) {
                    if (values[min_idx] > values[j]) {
                        // If so, mark as a switch and break the loop:
                        min_idx = j;
                    }
                }
                if (min_idx != i) {
                    rows[i].parentNode.insertBefore(rows[min_idx], rows[i]);
                    values.splice(i, 0, values[min_idx]);
                    values.splice(min_idx + 1, 1);
                }
            }
            window.dir = "desc";
        }
    }
    
    console.timeEnd('doSomething');

    SelectPage(index - 1);
}

function sortTable(n, type = 'string') {
    var table, rows, i;
    table = document.getElementById("ListTable");
    var comp = new Intl.Collator('vn');
    var max_idx = 0, min_idx = 0;
    var values = [];
    var dir = "desc";

    rows = table.rows;
    if (type == "string") {
        values.push("");
        for (i = 1; i < rows.length; i++) {
            values.push(rows[i].getElementsByTagName("TD")[n].innerHTML);
        }
        /* Loop through all table rows (except the
        first, which contains table headers): */
        if (dir == "desc") {
            for (i = 1; i < rows.length - 1; i++) {
                max_idx = i;

                for (j = i + 1; j < rows.length; j++) {
                    if (comp.compare(values[max_idx], values[j]) < 0) {
                        // If so, mark as a switch and break the loop:
                        max_idx = j;
                    }
                }
                if (max_idx != i) {
                    rows[i].parentNode.insertBefore(rows[max_idx], rows[i]);
                    values.splice(i, 0, values[max_idx]);
                    values.splice(max_idx + 1, 1);
                }
            }
            dir = "asc";
        }
        else if (dir == "asc") {
            for (i = 1; i < rows.length - 1; i++) {

                min_idx = i;

                for (j = i + 1; j < rows.length; j++) {
                    if (comp.compare(values[min_idx], values[j]) > 0) {
                        // If so, mark as a switch and break the loop:
                        min_idx = j;
                    }
                }
                if (min_idx != i) {
                    rows[i].parentNode.insertBefore(rows[min_idx], rows[i]);
                    values.splice(i, 0, values[min_idx]);
                    values.splice(min_idx + 1, 1);
                }
            }
            dir = "desc";
        }
    }
    else {
        values.push(-1);
        for (i = 1; i < rows.length; i++) {
            values.push(parseInt(rows[i].getElementsByTagName("TD")[n].innerHTML));
        }

        /* Loop through all table rows (except the
        first, which contains table headers): */
        if (dir == "desc") {
            for (i = 1; i < rows.length - 1; i++) {
                max_idx = i;

                for (j = i + 1; j < rows.length; j++) {
                    if (values[max_idx] < values[j]) {
                        // If so, mark as a switch and break the loop:
                        max_idx = j;
                    }
                }
                if (max_idx != i) {
                    rows[i].parentNode.insertBefore(rows[max_idx], rows[i]);
                    values.splice(i, 0, values[max_idx]);
                    values.splice(max_idx + 1, 1);
                }
            }
            dir = "asc";
        }
        else if (dir == "asc") {
            for (i = 1; i < rows.length - 1; i++) {

                min_idx = i;

                for (j = i + 1; j < rows.length; j++) {
                    if (values[min_idx] > values[j]) {
                        // If so, mark as a switch and break the loop:
                        min_idx = j;
                    }
                }
                if (min_idx != i) {
                    rows[i].parentNode.insertBefore(rows[min_idx], rows[i]);
                    values.splice(i, 0, values[min_idx]);
                    values.splice(min_idx + 1, 1);
                }
            }
            dir = "desc";
        }
    }

    console.timeEnd('doSomething');

}