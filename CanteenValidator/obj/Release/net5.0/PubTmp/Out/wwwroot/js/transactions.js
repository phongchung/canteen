﻿$(document).ready(function () {
    $("#success-alert").hide();
    loadTransactions();
});

function tempAlert(msg, duration) {
    var el = document.getElementById("popup1");
    var content = document.getElementById("content");
    content.innerHTML = msg;
    setTimeout(function () {
        window.location.href = "#";
    }, duration);
    window.location.href = "#popup1"
}

function loadTransactions() {
    $.get("/api/Transactions", null, function (response) {
        bindTransactions(response);
    });
}

function bindTransactions(transactions) {
    var html = "";
    $("#listTransacions").html("");
    var count = 0;
    $.each(transactions,
        function (index, transaction) {
            if (index == 4) {
                html += "<tr id='recent' value=" + transaction.id + ">";
                $("#image").attr("src","image/" + transaction.staffId + ".jpg");
                $("#name").html(transaction.name);
                $("#corporation").html(transaction.corporation);
                $("#department").html(transaction.department);
            }
            else html += "<tr>";

            html += "<td>" + transaction.corporation + "</td>";
            html += "<td class='text-link' id=" + index + " value=" + transaction.staffId + " href='#' onclick='searchSpecificTransaction(" + index + ", 1)'>" + transaction.name + "</td>";
            html += "<td style='text-align: center;'>" + moment(transaction.date).format('DD/MM/YYYY HH:mm:ss') + "</td>";
            html += "<td style='text-align: end;'>" + transaction.used + "</td>";
            html += "<td style='text-align: end;'>" + transaction.available + "</td>";

            html += "</tr>";
            count++;
            if (count == 5) return false;
        });

    $("#listTransactions").empty();
    $("#listTransactions").append(html);
    sortTable(2);

    // Blink the first table row to indicate
    $('tbody tr:first').addClass('blink_me');
}

delay = false;
var delayInMilliseconds = 3000; //2 second
var success = document.getElementById('success_sound');
var outOfTickets = document.getElementById('outOfTickets_sound');
var invalidId = document.getElementById('invalidId_sound');

function addTransactions(id) {
    if (delay == false) {
        delay = true;
        $.get("/api/Staff", null, function (response) {
            $.each(response,
                function (index, staff) {
                    if (staff.staffid = id) {
                        var data =
                        {
                            corporation: staff.corporation,
                            staffid: staff.staffid,
                            date: new Date()
                        }

                        $.ajax({
                            type: 'PUT',
                            url: 'api/Transactions',
                            contentType: 'application/json',
                            data: JSON.stringify(data)
                        }).done(function (response) {
                            if (response == "Success") {
                                success.play();
                                //snap();
                                loadTransactions();
                                tempAlert("Thanh toán thành công!", 2000);
                                $('.btn-success').prop('disabled', false);
                            }
                            else if (response == "OutOfTickets") {
                                outOfTickets.play();
                                tempAlert("Không đủ số lần khả dụng!", 2000);
                            }
                            else if (response == "InvalidId") {
                                invalidId.play();
                                tempAlert("Không tìm được thông tin người dùng!", 2000);
                            }
                        });

                        return false;
                    }

                });
        });
        setTimeout(function () {
            delay = false;
        }, delayInMilliseconds);
    }
}

function searchSpecificTransaction(index, type) {
    var value = document.getElementById(index).getAttribute('value');

    _corporationId = 0;
    _departmentId = 0;
    _staffId = 0;

    if (type == 1) {
        _staffId = value;
    }
    else if (type == 2) {
        _corporationId = value;
    }
    else if (type == 3) {
        _departmentId = value;
    }

    $.get("/api/SearchSpecificTransaction", data = {
        corporationId: _corporationId,
        departmentId: _departmentId,
        staffId: _staffId
    }, function (response) {
            var url = $('#RedirectTo').val();
            location.href = url;

    });
}

function undoTransaction() {
    var value = document.getElementById('recent').getAttribute('value');

    var data = {}
    data.id = parseInt(value);
    $.ajax({
        type: 'DELETE',
        url: 'api/Transactions',
        contentType: 'application/json',
        data: JSON.stringify(data),
        datatype: "JSON",
    }).done(function (response) {
        if (response == "Success") {
            success.play();
            loadTransactions();
            tempAlert("Hoàn tác thành công!", 2000);
            $('.btn-success').prop('disabled', true);
        }
        else if (response == "Failed") {
            invalidId.play();
            tempAlert("Hoàn tác không thành công!", 2000);
        }
    });
}

var video = document.querySelector('video');
var canvas = document.querySelector('canvas');
var context = canvas.getContext('2d');
var w, h, ratio;

//add loadedmetadata which will helps to identify video attributes
video.addEventListener('loadedmetadata', function () {
    ratio = video.videoWidth / video.videoHeight;
    w = 640;
    h = 480;
    canvas.width = w;
    canvas.height = h;
}, false);

function snap() {
    context.fillRect(0, 0, w, h);
    context.drawImage(video, 0, 0, w, h);
    var link = document.getElementById('link');
    link.setAttribute('download', 'MintyPaper.jpg');
    link.setAttribute('href', canvas.toDataURL("image/jpg").replace("image/jpg", "image/octet-stream"));
    link.click();
}