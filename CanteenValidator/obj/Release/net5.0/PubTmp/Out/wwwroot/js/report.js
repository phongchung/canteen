﻿$("#Corporations").change(function () {
    getReport();
});

$("#Departments").change(function () {
    getReport();
});

$("#Months").change(function () {
    getReport();
});

$("#Years").change(function () {
    getReport();
});

$(document).ready(function () {
    $('#ListTable').after('<div id="nav" class="d-flex justify-content-center p-1 pt-0 page-nav"></div>');
    getReport();
});

var rowsShown = 10;

function getReport() {

    $.get("/api/Report", data = {
        corporationId: $("#Corporations").val(),
        departmentId: $("#Departments").val(),
        month: $("#Months").val(),
        year: $("#Years").val()
    }, function (response) {
            bindReport(response);
    });
}


function bindReport(result) {
    var html = "";
    var totalUsed = 0;
    var totalAvailable = 0;
    $.each(result, function (index, model) {
        var idx = index + 1;
        totalUsed += model.used;
        totalAvailable += model.available;

        html += "<tr>";

        html += "<td style='text-align: end;' data-a-h='center' data-b-a-s='thin' data-t='n'>" + idx  + "</td>";
        html += "<td data-b-a-s='thin'>" + model.name + "</td>";
        html += "<td data-exclude='true'>" + model.department + "</td>";
        html += "<td class='d-none' data-a-h='center' data-b-a-s='thin'>" + model.corporation + "</td>";
        html += "<td style='text-align: end;' data-a-h='right' data-b-a-s='thin' data-t='n'>" + model.used + "</td>";
        html += "<td style='text-align: end;' data-exclude='true' data-b-a-s='thin'>" + model.available + "</td>";

        html += "</tr>";

    });

    $("#totalUsed").html(totalUsed);
    $("#totalAvailable").html(totalAvailable);
    $("#hiddenTotalUsed").html(totalUsed);
    $("#hiddenTotalAvailable").html(totalAvailable);
    $("#listReport").empty();
    $("#listReport").append(html);
    $("#nav").empty();

    var rowsTotal = $('#ListTable tbody tr').length;
    var numPages = Math.ceil(rowsTotal / rowsShown);

    $('#nav').append('<em class="pl-2" style="left: 0; position: absolute;">Tổng số kết quả:'
        + '<em style="font-weight:bold;"> ' + rowsTotal + '</em></em> ');    
    $('#nav').append('<span onclick="Previous()" class="d-none p-2 span-link" href="#" ">Quay lại</span>');
    for (i = 0; i < numPages; i++) {
        if (i == 2) {
            $('#nav').append('<span id="decoratorL" class="d-none">...</span> ');
        }
        else if (i == numPages - 2) {
            $('#nav').append('<span id="decoratorR" class="d-none">...</span> ');
        }
        var pageNum = i + 1;
        $('#nav').append('<a onclick="SelectPage(' + i + ')" class="p-2" href="#" rel="' + i + '" id="' + i + '">' + pageNum + '</a> ');
    }
    $('#nav').append('<span onclick="Next()" class="p-2 span-link" href="#" ">Tiếp theo</span>');
    if (numPages < 2) {
        $('#nav span:last').removeClass('d-block');
        $('#nav span:last').addClass('d-none');
    }

    $('#nav a').addClass('d-none');
    for (j = 0; j < 5; j++) {
        var nav = $('#' + j.toString());
        nav.removeClass('d-none');
        nav.addClass('d-block');
    }

    $('#ListTable tbody tr').hide();
    $('#ListTable tbody tr').slice(0, rowsShown).show();
    $('#nav a:first').addClass('active');

    if (numPages == 6) {
        $('#nav a:first').removeClass('d-none');
        $('#nav a:first').addClass('d-block');
    }
    else if (numPages > 6) {
        $('#decoratorR').removeClass('d-none');
        $('#decoratorR').addClass('d-block');
        $('#nav a:last').removeClass('d-none');
        $('#nav a:last').addClass('d-block');
    }
    //if (numPages > 7) {
    //    $('#nav').append('<input id="jumpId" style="width:50px;"></input>');
    //    $('#nav').append('<span onclick="PageJump()" class="p-2 span-link" href="#" ">Đến</span>');
    //}
    $('#nav a').bind('click', function () {
        $('#nav a').removeClass('active');
        $(this).addClass('active');
        var currPage = $(this).attr('rel');
        var startItem = currPage * rowsShown;
        var endItem = startItem + rowsShown;
        $('#ListTable tbody tr').css('opacity', '0.0').hide().slice(startItem, endItem).
            css('display', 'table-row').animate({ opacity: 1 }, 300);
    });
}

function SelectPage(i) {
    if (isNaN(i)) return false;
    var rowsTotal = $('#ListTable tbody tr').length;
    var numPages = Math.ceil(rowsTotal / rowsShown);

    var startIdx = i - 2;
    var endIdx = i + 3;

    if (startIdx < 0) {
        endIdx -= startIdx;
        startIdx = 0;
    }

    if (endIdx > numPages) {
        startIdx -= endIdx - numPages;
        endIdx = numPages;
    }

    $('#nav a').removeClass('d-block');
    $('#nav a').addClass('d-none');
    if (i > 3) {
        if (numPages > 6) {
            $('#decoratorL').removeClass('d-none');
            $('#decoratorL').addClass('d-block');
        }
        $('#nav a:first').removeClass('d-none');
        $('#nav a:first').addClass('d-block');
    }
    else if (i == 3) {
        $('#decoratorL').removeClass('d-block');
        $('#decoratorL').addClass('d-none');
        $('#nav a:first').removeClass('d-none');
        $('#nav a:first').addClass('d-block');
    }
    else {
        $('#decoratorL').removeClass('d-block');
        $('#decoratorL').addClass('d-none');
    }

    if (i < numPages - 4) {
        $('#decoratorR').removeClass('d-none');
        $('#decoratorR').addClass('d-block');
        $('#nav a:last').removeClass('d-none');
        $('#nav a:last').addClass('d-block');
    }
    else if (i == numPages - 4) {
        $('#decoratorR').removeClass('d-block');
        $('#decoratorR').addClass('d-none');
        $('#nav a:last').removeClass('d-none');
        $('#nav a:last').addClass('d-block');
    }
    else {
        $('#decoratorR').removeClass('d-block');
        $('#decoratorR').addClass('d-none');
    }
    for (j = startIdx; j < endIdx; j++) {
        var nav = $('#' + j.toString());
        nav.removeClass('d-none');
        nav.addClass('d-block');
    }

    if (i != 0) {
        $('#nav span:first').removeClass('d-none');
        $('#nav span:first').addClass('d-block');
    }
    else {
        $('#nav span:first').removeClass('d-block');
        $('#nav span:first').addClass('d-none');
    }

    if (i != numPages - 1) {
        $('#nav span:last').removeClass('d-none');
        $('#nav span:last').addClass('d-block');
    }
    else {
        $('#nav span:last').removeClass('d-block');
        $('#nav span:last').addClass('d-none');
    }
    $('#ListTable tbody tr').hide();
    $('#ListTable tbody tr').slice(i * rowsShown, i * rowsShown + rowsShown).show();

    $('#nav a').removeClass('active');
    $('#' + i + '').addClass('active');
    var startItem = i * rowsShown;
    var endItem = startItem + rowsShown;
    $('#ListTable tbody tr').css('opacity', '0.0').hide().slice(startItem, endItem).
        css('display', 'table-row').animate({ opacity: 1 }, 300);
}

function Previous() {
    var index = document.getElementsByClassName("active").item(0).innerHTML;
    SelectPage(parseInt(index) - 2);
}

function Next() {
    var index = document.getElementsByClassName("active").item(0).innerHTML;
    SelectPage(parseInt(index));
}

function getDateFormat(date) {
    var day = date.getDate();
    var displayMonth = date.getMonth() + 1;
    var year = date.getFullYear();
    day = day < 10 ? '0' + day : day;
    displayMonth = displayMonth < 10 ? '0' + displayMonth : displayMonth;
    return `${day}/${displayMonth}/${year}`;
}

function dayInMonth(month, year, day) {
    return new Date(year, month, day);
}

function getMonthlyReport() {
    $.get("/api/MonthlyReport", data = {
        month: $("#Months").val(),
        year: $("#Years").val()
    }, function (response) {
        bindMonthlyReport(response);
    });
}

function bindMonthlyReport(result) {
    html = "";
    var totalUsed = 0;
    var month = $("#Months").val();
    var year = $("#Years").val();

    $('#monthlyReportTimeHeader').html("Tháng " + month + " năm " + year);

    $("#listMonthlyReport").html("");
    $.each(result, function (index, model) {
        var idx = index + 1;
        totalUsed += model.used;

        html += "<tr>";

        html += "<td style='text-align: end;' data-a-h='center' data-b-a-s='thin' data-t='n'>" + idx + "</td>";
        html += "<td data-a-h='center' data-b-a-s='thin'>" + model.corporation + "</td>";
        html += "<td style='text-align: end;' data-a-h='right' data-b-a-s='thin' data-t='n'>" + model.used + "</td>";

        html += "</tr>";

    });
    $("#listMonthlyReport").append(html);
    $("#hiddenTotalUsedMonthly").html(totalUsed);

    TableToExcel.convert(document.getElementById("MonthlyReportTable"), {
        name: "Table.xlsx",
        sheet: {
            name: "Sheet1"
        }
    });
} 

function getStaffReport() {
    var month = $("#Months").val() - 1;
    var year = $("#Years").val();
    var fromDate = getDateFormat(dayInMonth(month, year, 1));
    var toDate = getDateFormat(dayInMonth(month + 1, year, 0));

    $('#staffReportTimeHeader').html("Từ " + fromDate + " đến " + toDate);

    TableToExcel.convert(document.getElementById("ListTable"), {
        name: "Table.xlsx",
        sheet: {
            name: "Sheet1"
        }
    });
}

