﻿$("#Corporations").change(function () {
    searchTransactions();
});

$("#Departments").change(function () {
    searchTransactions();
});

$("#StaffNames").change(function () {
    searchTransactions();
});

$("#Days").change(function () {
    searchTransactions();
});

$("#Months").change(function () {
    searchTransactions();
});

$("#Years").change(function () {
    searchTransactions();
});

$(document).ready(function () {
    $('#ListTable').after('<div id="nav" class="d-flex justify-content-center p-1 pt-0 page-nav"></div>');
    searchTransactions();
});

var rowsShown = 10;

function searchTransactions() {
    $.get("/api/Search", data= {
        corporationId: $("#Corporations").val(),
        departmentId: $("#Departments").val(),
        staffId: $("#StaffNames").val(),
        day: $("#Days").val(),
        month: $("#Months").val(),
        year: $("#Years").val()
    }, function (response) {
            bindSearch(response);
    });
}


function bindSearch(result) {
    var html = "";
    $("#listSearch").html("");
    $.each(result.transactionsViewModel,
        function (index, model) {
            var idx = index + 1;
            html += "<tr>";

            html += "<td style='text-align: end;'>" + idx + "</td>";
            html += "<td>" + model.department + "</td>";
            html += "<td>" + model.name + "</td>";
            html += "<td style='text-align: end;'>" + model.used + "</td>";
            html += "<td style='text-align: end;'>" + model.available + "</td>";
            html += "<td style='text-align: center;'>" + moment(model.date).format('DD/MM/YYYY HH:mm:ss') + "</td>";

            html += "</tr>";

        });

    $("#listSearch").empty();
    $("#listSearch").append(html);
    $("#nav").empty();

    var rowsTotal = $('#ListTable tbody tr').length;
    var numPages = Math.ceil(rowsTotal / rowsShown);

    $('#nav').append('<em class="pl-2" style="left: 0; position: absolute;">Tổng số kết quả:' 
        + '<em style="font-weight:bold;"> ' + rowsTotal + '</em></em>');

    $('#nav').append('<span onclick="Previous()" class="d-none p-2 span-link" href="#" ">Quay lại</span>');
    for (i = 0; i < numPages; i++) {
        if (i == 2) {
            $('#nav').append('<span id="decoratorL" class="d-none p-2">...</span> ');
        }
        else if (i == numPages - 2) {
            $('#nav').append('<span id="decoratorR" class="d-none p-2">...</span> ');
        }
        var pageNum = i + 1;
        $('#nav').append('<a onclick="SelectPage(' + i + ')" class="p-2" href="#" rel="' + i + '" id="' + i + '">' + pageNum + '</a> ');
    }
    $('#nav').append('<span onclick="Next()" class="p-2 span-link" href="#" ">Tiếp theo</span>');
    if (numPages < 2) {
        $('#nav span:last').removeClass('d-block');
        $('#nav span:last').addClass('d-none');
    }

    $('#nav a').addClass('d-none');
    for (j = 0; j < 5; j++) {
        var nav = $('#' + j.toString());
        nav.removeClass('d-none');
        nav.addClass('d-block');
    }

    $('#ListTable tbody tr').hide();
    $('#ListTable tbody tr').slice(0, rowsShown).show();
    $('#nav a:first').addClass('active');
    if (numPages == 6) {
        $('#nav a:first').removeClass('d-none');
        $('#nav a:first').addClass('d-block');
    }
    else if (numPages > 6) {
        $('#decoratorR').removeClass('d-none');
        $('#decoratorR').addClass('d-block');
        $('#nav a:last').removeClass('d-none');
        $('#nav a:last').addClass('d-block');
    }
    //if (numPages > 7) {
    //    $('#nav').append('<input id="jumpId" style="width:50px;"></input>');
    //    $('#nav').append('<span onclick="PageJump()" class="p-2 span-link" href="#" ">Đến</span>');
    //}
    $('#nav a').bind('click', function () {
        $('#nav a').removeClass('active');
        $(this).addClass('active');
        var currPage = $(this).attr('rel');
        var startItem = currPage * rowsShown;
        var endItem = startItem + rowsShown;
        $('#ListTable tbody tr').css('opacity', '0.0').hide().slice(startItem, endItem).
            css('display', 'table-row').animate({ opacity: 1 }, 300);
    });
}

function SelectPage(i) {
    if (isNaN(i)) return false;
    var rowsTotal = $('#ListTable tbody tr').length;
    var numPages = Math.ceil(rowsTotal / rowsShown);

    var startIdx = i - 2;
    var endIdx = i + 3;

    if (startIdx < 0) {
        endIdx -= startIdx;
        startIdx = 0;
    }

    if (endIdx > numPages) {
        startIdx -= endIdx - numPages;  
        endIdx = numPages;
    }

    $('#nav a').removeClass('d-block');
    $('#nav a').addClass('d-none');
    if (i > 3) {
        if (numPages > 6) {
            $('#decoratorL').removeClass('d-none');
            $('#decoratorL').addClass('d-block');
        }
        $('#nav a:first').removeClass('d-none');
        $('#nav a:first').addClass('d-block');
    }
    else if (i == 3)
    {
        $('#decoratorL').removeClass('d-block');
        $('#decoratorL').addClass('d-none');
        $('#nav a:first').removeClass('d-none');
        $('#nav a:first').addClass('d-block');
    }
    else {
        $('#decoratorL').removeClass('d-block');
        $('#decoratorL').addClass('d-none');
    }

    if (i < numPages - 4) {
        $('#decoratorR').removeClass('d-none');
        $('#decoratorR').addClass('d-block');
        $('#nav a:last').removeClass('d-none');
        $('#nav a:last').addClass('d-block');
    }
    else if (i == numPages - 4)
    {
        $('#decoratorR').removeClass('d-block');
        $('#decoratorR').addClass('d-none');
        $('#nav a:last').removeClass('d-none');
        $('#nav a:last').addClass('d-block');
    }
    else {
        $('#decoratorR').removeClass('d-block');
        $('#decoratorR').addClass('d-none');
    }
    for (j = startIdx; j < endIdx; j++) {
        var nav = $('#' + j.toString());
        nav.removeClass('d-none');
        nav.addClass('d-block');
    }

    if (i != 0) {
        $('#nav span:first').removeClass('d-none');
        $('#nav span:first').addClass('d-block');
    }
    else {
        $('#nav span:first').removeClass('d-block');
        $('#nav span:first').addClass('d-none');
    }

    if (i != numPages - 1) {
        $('#nav span:last').removeClass('d-none');
        $('#nav span:last').addClass('d-block');
    }
    else {
        $('#nav span:last').removeClass('d-block');
        $('#nav span:last').addClass('d-none');
    }
    $('#ListTable tbody tr').hide();
    $('#ListTable tbody tr').slice(i * rowsShown, i * rowsShown + rowsShown).show();

    $('#nav a').removeClass('active');
    $('#' + i + '').addClass('active');
    var startItem = i * rowsShown;
    var endItem = startItem + rowsShown;
    $('#ListTable tbody tr').css('opacity', '0.0').hide().slice(startItem, endItem).
        css('display', 'table-row').animate({ opacity: 1 }, 300);
}

function PageJump() {
    SelectPage(parseInt(document.getElementById("jumpId").value));
}

function Previous() {
    var index = document.getElementsByClassName("active").item(0).innerHTML;
    SelectPage(parseInt(index) - 2);
}

function Next() {
    var index = document.getElementsByClassName("active").item(0).innerHTML;
    SelectPage(parseInt(index));
}

function dayInMonth(month, year, day) {
    return new Date(year, month, day);
}

function getDailyReport() {
    $.get("/api/DailyReport", data = {
        month: $("#Months").val(),
        year: $("#Years").val()
    }, function (response) {
        bindDailyReport(response);
    });
}

function getDateFormat(date) {
    var day = date.getDate();
    var displayMonth = date.getMonth() + 1;
    var year = date.getFullYear();
    day = day < 10 ? '0' + day : day;
    displayMonth = displayMonth < 10 ? '0' + displayMonth : displayMonth;
    return `${day}/${displayMonth}/${year}`;
}

function bindDailyReport(result) {
    html = "";
    var idx;
    var totalUsed = 0;
    var dateTotalUsed;
    var corpTotalUsed = Array(result.corporationList.length).fill(0);
    var month = $("#Months").val() - 1;
    var date = dayInMonth(month, $("#Years").val(), 1);
    var fromDate = getDateFormat(date);
    var toDate = getDateFormat(dayInMonth(month + 1, $("#Years").val(), 0));

    var columnWidth = "10, 25";
    $.each(result.corporationList, function () {
        columnWidth += ", 15";
    });
    columnWidth += ",18";
    $('#DailyReportTable').attr("data-cols-width", columnWidth);
    $('#dailyReportHeader').attr("colspan", 3 + result.corporationList.length);
    $('#dailyReportTimeHeader').attr("colspan", 3 + result.corporationList.length);
    $('#dailyReportTimeHeader').html("Từ " + fromDate + " đến " + toDate);


    $("#listDailyReport").html("");

    html += "<tr>";

    html += "<td rowspan='2' data-a-h='center' data-a-v='middle' data-b-a-s='thin' data-f-bold='true'>STT</td>";
    html += "<td rowspan='2' data-a-h='center' data-a-v='middle' data-b-a-s='thin' data-f-bold='true'>Ngày</td>";

    html += "<td colspan=" + result.corporationList.length + " data-a-h='center' data-a-v='middle' data-b-a-s='thin' data-f-bold='true'>Phiếu cơm đã sử dụng</td>";

    html += "<td rowspan='2' data-a-h='center' data-a-v='middle' data-b-a-s='thin' data-f-bold='true'>Tổng cộng</td>";

    html += "</tr>";

    html += "<tr>";

    $.each(result.corporationList, function (corpIdx, corp) {

        html += "<td data-a-h='center' data-b-a-s='thin' data-f-bold='true'>" + corp + "</td>";

    });

    html += "</tr>";

    while (date.getMonth() == month) {
        dateTotalUsed = 0;
        idx = date.getDate() - 1;

        var time = getDateFormat(date);

        html += "<tr>";

        html += "<td data-a-h='center' data-b-a-s='thin' data-t='n'>" + idx + 1+ "</td>";
        html += "<td data-a-h='center' data-b-a-s='thin'>" + time + "</td>";
        $.each(result.corporationList, function (corpIdx, corp) {
            dateTotalUsed += result.usedList[corpIdx][idx];
            corpTotalUsed[corpIdx] += result.usedList[corpIdx][idx];

            html += "<td data-a-h='right' data-b-a-s='thin' data-t='n'>" + result.usedList[corpIdx][idx] + "</td>";

        });

        html += "<td data-a-h='right' data-b-a-s='thin' data-t='n'>" + dateTotalUsed + "</td>";

        html += "</tr>";

        totalUsed += dateTotalUsed;
        date.setDate(date.getDate() + 1);
    }

    html += "<tr>";

    html += "<th colspan='2' data-f-bold='true' data-a-h='center' data-b-a-s='thin'>Tổng cộng</th>";
    $.each(result.corporationList, function (corpIdx, corp) {

        html += "<th data-f-bold='true' data-a-h='right' data-t='n' data-b-a-s='thin'>" + corpTotalUsed[corpIdx] + "</th>";

    });
    html += "<th data-f-bold='true' data-a-h='right' data-t='n' data-b-a-s='thin'>" + totalUsed + "</th>";

    html += "</tr>";

    $("#listDailyReport").append(html);
    $("#hiddenTotalUsedDaily").html(totalUsed);

    TableToExcel.convert(document.getElementById("DailyReportTable"), {
        name: "Table.xlsx",
        sheet: {
            name: "Sheet1"
        }
    });
} 