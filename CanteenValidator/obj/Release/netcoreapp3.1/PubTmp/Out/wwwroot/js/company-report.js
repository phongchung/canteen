﻿$("#Companies_C").change(function () {
    getDailyReport();
});

//$("#Types").change(function () {
//    getReport();
//});

$("#Months_C").change(function () {
    getDailyReport();
});

$("#Years_C").change(function () {
    getDailyReport();
});

$(document).ready(function () {
    $('#ListTable_C').after('<div id="nav_C" class="d-flex justify-content-center pt-0 page-nav"></div>');
    getDailyReport();
});

var rowsShown = 10;

function SelectPage_C(i) {
    if (isNaN(i)) return false;
    var rowsTotal = $('#ListTable_C tbody tr').length;
    var numPages = Math.ceil(rowsTotal / rowsShown);

    var startIdx = i - 2;
    var endIdx = i + 3;

    if (startIdx < 0) {
        endIdx -= startIdx;
        startIdx = 0;
    }

    if (endIdx > numPages) {
        startIdx -= endIdx - numPages;
        endIdx = numPages;
    }

    if (numPages > 6) {
        $('#nav_C a').removeClass('d-block');
        $('#nav_C a').addClass('d-none');
        if (i > 3) {
            $('#decoratorL_C').removeClass('d-none');
            $('#decoratorL_C').addClass('d-block');
            $('#nav_C a:first').removeClass('d-none');
            $('#nav_C a:first').addClass('d-block');
        } else if (i == 3) {
            $('#decoratorL_C').removeClass('d-block');
            $('#decoratorL_C').addClass('d-none');
            $('#nav_C a:first').removeClass('d-none');
            $('#nav_C a:first').addClass('d-block');
        } else {
            $('#decoratorL_C').removeClass('d-block');
            $('#decoratorL_C').addClass('d-none');
        }
    }

    if (i < numPages - 4) {
        if (numPages > 6) {
            $('#decoratorR_C').removeClass('d-none');
            $('#decoratorR_C').addClass('d-block');
            $('#nav_C a:last').removeClass('d-none');
            $('#nav_C a:last').addClass('d-block');
        }
    } else if (i == numPages - 4) {
        $('#decoratorR_C').removeClass('d-block');
        $('#decoratorR_C').addClass('d-none');
        $('#nav_C a:last').removeClass('d-none');
        $('#nav_C a:last').addClass('d-block');
    } else {
        $('#decoratorR_C').removeClass('d-block');
        $('#decoratorR_C').addClass('d-none');
    }
    for (j = startIdx; j < endIdx; j++) {
        var nav = $('#C_' + j.toString() + '_C');
        nav.removeClass('d-none');
        nav.addClass('d-block');
    }

    if (numPages > 5) {
        if (i != 0) {
            $('#nav_C span:first').removeClass('d-none');
            $('#nav_C span:first').addClass('d-block');
        } else {
            $('#nav_C span:first').removeClass('d-block');
            $('#nav_C span:first').addClass('d-none');
        }

        if (i != numPages - 1) {
            $('#nav_C span:last').removeClass('d-none');
            $('#nav_C span:last').addClass('d-block');
        } else {
            $('#nav_C span:last').removeClass('d-block');
            $('#nav_C span:last').addClass('d-none');
        }
    }
    $('#ListTable_C tbody tr').hide();
    $('#ListTable_C tbody tr').slice(i * rowsShown, i * rowsShown + rowsShown).show();

    $('#nav_C a').removeClass('active');
    $('#C_' + i + '').addClass('active');
    var startItem = i * rowsShown;
    var endItem = startItem + rowsShown;
    $('#ListTable_C tbody tr').css('opacity', '0.0').hide().slice(startItem, endItem).
        css('display', 'table-row').animate({ opacity: 1 }, 300);
}

function Previous() {
    var index = document.getElementsByClassName("active").item(0).innerHTML;
    SelectPage_C(parseInt(index) - 2);
}

function Next() {
    var index = document.getElementsByClassName("active").item(0).innerHTML;
    SelectPage_C(parseInt(index));
}


function dayInMonth(month, year, day) {
    return new Date(year, month, day);
}

function getDateFormat(date) {
    var day = date.getDate();
    var displayMonth = date.getMonth() + 1;
    var year = date.getFullYear();
    day = day < 10 ? '0' + day : day;
    displayMonth = displayMonth < 10 ? '0' + displayMonth : displayMonth;
    return `${day}/${displayMonth}/${year}`;
}

//function getMonthlyReport() {
//    $.get("/api/MonthlyReport", data = {
//        month: $("#Months").val(),
//        year: $("#Years").val()
//    }, function (response) {
//        bindMonthlyReport(response);
//    });
//}

//function bindMonthlyReport(result) {
//    html = "";
//    var totalUsed = 0;
//    var month = $("#Months").val();
//    var year = $("#Years").val();

//    $('#monthlyReportTimeHeader').html("Tháng " + month + " năm " + year);

//    $("#listMonthlyReport").html("");
//    $.each(result, function (index, model) {
//        var idx = index + 1;
//        totalUsed += model.used;

//        html += "<tr>";

//        html += "<td style='text-align: end;' data-a-h='center' data-b-a-s='thin' data-t='n'>" + idx + "</td>";
//        html += "<td data-a-h='center' data-b-a-s='thin'>" + model.company + "</td>";
//        html += "<td style='text-align: end;' data-a-h='right' data-b-a-s='thin' data-t='n'>" + model.used + "</td>";

//        html += "</tr>";

//    });
//    $("#listMonthlyReport").append(html);
//    $("#hiddenTotalUsedMonthly").html(totalUsed);

//    TableToExcel.convert(document.getElementById("MonthlyReportTable"), {
//        name: "Table.xlsx",
//        sheet: {
//            name: "Sheet1"
//        }
//    });
//} 


function getDailyReport() {
    var data = {
        companyID: getInt($("#Companies_C").val()),
        month: getInt($("#Months_C").val()),
        year: getInt($("#Years_C").val())
    }
    $.ajax({
        type: 'POST',
        url: '/api/DailyReport',
        contentType: 'application/json',
        data: JSON.stringify(data)
    }).done(function (response) {
        bindDailyReport(response);
    });

}

function bindDailyReport(result) {
    html = "";
    var idx;
    var totalUsed = 0;
    var dateTotalUsed;
    var compTotalUsed = Array(result.companyList.length).fill(0);
    var month = $("#Months_C").val() - 1;
    var date = dayInMonth(month, $("#Years_C").val(), 1);
    var fromDate = getDateFormat(date);
    var toDate = getDateFormat(dayInMonth(month + 1, $("#Years_C").val(), 0));

    var columnWidth = "10, 25";
    $.each(result.companyList, function () {
        columnWidth += ", 15";
    });
    columnWidth += ",18";
    $('#DailyReportTable').attr("data-cols-width", columnWidth);
    $('#dailyReportHeader').attr("colspan", 3 + result.companyList.length);
    $('#dailyReportTimeHeader').attr("colspan", 3 + result.companyList.length);
    $('#dailyReportTimeHeader').html("Từ " + fromDate + " đến " + toDate);


    $("#listDailyReport").html("");

    html += "<tr>";

    html += "<td rowspan='2' data-a-h='center' data-a-v='middle' data-b-a-s='thin' data-f-bold='true'>STT</td>";
    html += "<td rowspan='2' data-a-h='center' data-a-v='middle' data-b-a-s='thin' data-f-bold='true'>Ngày</td>";

    html += "<td colspan=" + result.companyList.length + " data-a-h='center' data-a-v='middle' data-b-a-s='thin' data-f-bold='true'>Phiếu cơm đã sử dụng</td>";

    html += "<td rowspan='2' data-a-h='center' data-a-v='middle' data-b-a-s='thin' data-f-bold='true'>Tổng cộng</td>";

    html += "</tr>";

    html += "<tr>";

    $.each(result.companyList, function (compIdx, comp) {

        html += "<td data-a-h='center' data-b-a-s='thin' data-f-bold='true'>" + comp.id + "</td>";

    });

    html += "</tr>";

    while (date.getMonth() == month) {
        dateTotalUsed = 0;
        idx = date.getDate() - 1;

        var time = getDateFormat(date);

        html += "<tr>";

        html += "<td data-a-h='center' data-b-a-s='thin' data-t='n'>" + (idx + 1) + "</td>";
        html += "<td data-a-h='center' data-b-a-s='thin'>" + time + "</td>";
        $.each(result.companyList, function (compIdx, comp) {
            dateTotalUsed += result.usedList[compIdx][idx];
            compTotalUsed[compIdx] += result.usedList[compIdx][idx];

            html += "<td data-a-h='right' data-b-a-s='thin' data-t='n'>" + result.usedList[compIdx][idx] + "</td>";

        });

        html += "<td data-a-h='right' data-b-a-s='thin' data-t='n'>" + dateTotalUsed + "</td>";

        html += "</tr>";

        totalUsed += dateTotalUsed;
        date.setDate(date.getDate() + 1);
    }

    html += "<tr>";

    html += "<th colspan='2' data-f-bold='true' data-a-h='center' data-b-a-s='thin'>Tổng cộng</th>";
    $.each(result.companyList, function (compIdx, comp) {

        html += "<th data-f-bold='true' data-a-h='right' data-t='n' data-b-a-s='thin'>" + compTotalUsed[compIdx] + "</th>";

    });
    html += "<th data-f-bold='true' data-a-h='right' data-t='n' data-b-a-s='thin'>" + totalUsed + "</th>";

    html += "</tr>";

    $("#listDailyReport").append(html);
    $("#hiddenTotalUsedDaily").html(totalUsed);


    var html = "";
    var idx = 0;
    $.each(result.companyList, function (compIdx, comp) {
        idx = compIdx + 1;

        html += "<tr>";

        html += "<td style='text-align: center;' data-a-h='center' data-b-a-s='thin' data-t='n'>" + idx + "</td>";
        html += "<td style='text-align: start; padding-left: 2rem !important' data-a-h='start' data-b-a-s='thin' data-t='n'>" + comp.name + "</td>";

        html += "<td style='text-align: center;' data-a-h='right' data-b-a-s='thin' data-t='n'>" + compTotalUsed[compIdx] + "</td>";
        //html += "<td data-exclude='true'>" + model.type + "</td>";

        html += "</tr>";
    });
    var rowsTotal = result.companyList.length;
    var numPages = Math.ceil(rowsTotal / rowsShown);
    if (rowsTotal % 10 != 0) {
        for (var i = rowsTotal % 10; i < 10; i++) {
            var idx = idx + 1;
            html += "<tr data-exclude='true' >";

            html += "<td style='text-align: center;'>" + idx + "</td>";
            html += "<td></td>";
            html += "<td></td>";

            html += "</tr>";
        }
    }


    $("#totalUsed_C").html(totalUsed);
    $("#listReport_C").empty();
    $("#listReport_C").append(html);
    $("#nav_C").empty();
    if (rowsTotal == 0) {
        $("#ListTable_C").addClass("h-100");
    } else {
        $("#ListTable_C").removeClass('h-100');
    }

    rowsTotal = rowsTotal - 1;
    //if (rowsTotal > 1) {
    //    $('#nav_C').append('<em class="d-none d-sm-inline pl-2" style="left: 0; position: absolute; margin-left: 6rem;">Tổng số kết quả:'
    //        + '<em class="d-none d-sm-inline" style="font-weight:bold;"> ' + rowsTotal + '</em></em> ');
    //}
    $('#nav_C').append('<span onclick="Previous()" class="d-none p-2 span-link" href="#" ">Quay lại</span>');
    for (i = 0; i < numPages; i++) {
        if (i == 2) {
            $('#nav_C').append('<span id="decoratorL_C" class="d-none">...</span> ');
        } else if (i == numPages - 2) {
            $('#nav_C').append('<span id="decoratorR_C" class="d-none">...</span> ');
        }
        var pageNum = i + 1;
        $('#nav_C').append('<a onclick="SelectPage_C(' + i + ')" class="p-2" href="#" rel="' + i + '" id="C_' + i + '">' + pageNum + '</a> ');
    }
    $('#nav_C').append('<span onclick="Next()" class="p-2 span-link" href="#" ">Tiếp theo</span>');
    if (numPages < 2) {
        $('#nav_C span:last').removeClass('d-block');
        $('#nav_C span:last').addClass('d-none');
    }

    $('#nav_C a').addClass('d-none');
    for (j = 0; j < 5; j++) {
        var nav = $('#C_' + j.toString());
        nav.removeClass('d-none');
        nav.addClass('d-block');
    }

    $('#ListTable_C tbody tr').hide();
    $('#ListTable_C tbody tr').slice(0, rowsShown).show();
    $('#nav_C a:first').addClass('active');

    if (numPages == 6) {
        $('#nav_C a:first').removeClass('d-none');
        $('#nav_C a:first').addClass('d-block');
    } else if (numPages > 6) {
        $('#decoratorR_C').removeClass('d-none');
        $('#decoratorR_C').addClass('d-block');
        $('#nav_C a:last').removeClass('d-none');
        $('#nav_C a:last').addClass('d-block');
    }
    //if (numPages > 7) {
    //    $('#nav_C').append('<input id="jumpId" style="width:50px;"></input>');
    //    $('#nav_C').append('<span onclick="PageJump()" class="p-2 span-link" href="#" ">Đến</span>');
    //}
    $('#nav_C a').bind('click', function () {
        $('#nav_C a').removeClass('active');
        $(this).addClass('active');
        var currPage = $(this).attr('rel');
        var startItem = currPage * rowsShown;
        var endItem = startItem + rowsShown;
        $('#ListTable_C tbody tr').css('opacity', '0.0').hide().slice(startItem, endItem).
            css('display', 'table-row').animate({ opacity: 1 }, 300);
    });

}

function printDailyReport() {
    var curDate = new Date();

    TableToExcel.convert(document.getElementById("DailyReportTable"), {
        name: "CompanyReport_" + moment(curDate).format('DD_MM_YYYY_HH_mm_ss') + ".xlsx",
        sheet: {
            name: "Sheet1"
        }
    });
}