﻿var curAction = "";
var curHolidayId = -1;
var curIndex = -1;

function tempAlert(msg, duration) {
    var content = document.getElementById("content");
    content.innerHTML = msg;
    setTimeout(function () {
        window.location.href = "#";
    }, duration);
    window.location.href = "#popup_holiday"
}

$("#FromDate").change(function () {
    $('#ToDate').attr('min', moment($('#FromDate').val()).format("YYYY-MM-DD"));
});

$("#ToDate").change(function () {
    $('#FromDate').attr('max', moment($('#ToDate').val()).format("YYYY-MM-DD"));
});

$(document).ready(function () {
    document.getElementById('FromDate').valueAsDate = new Date();
    document.getElementById('ToDate').valueAsDate = new Date();

    $('main').after('<div id="nav" class="nav_H d-flex justify-content-center pt-0 page-nav"></div>');

    $("#btnLogin").click(function () {
        var data = {
            pw: $("#holidaysLogin").val()
        }
        $.ajax({
            type: 'POST',
            url: '/api/HolidaysLogin',
            contentType: 'application/json',
            data: JSON.stringify(data)
        }).done(function (response) {
            if (response == "Success!") {
                $("#loginModal").modal('hide');
                switch (window.curAction) {
                    case "Add":
                        {
                            DoAddHoliday();
                            break;
                        }
                    case "Edit":
                        {
                            EditHoliday(window.curIndex);
                            break;
                        }
                    case "Remove":
                        {
                            RemoveHoliday(window.curHolidayId, window.curIndex);
                        }
                }
            }
        }).fail(function (xhr, status, error) {
            console.log("Lele");
            if (xhr.responseText.includes("Failed!")) {
                tempAlert("Sai mật khẩu nhân sự, vui lòng nhập lại!", 2000);
            }

        });

    });

    $("#holidaysLogin").keypress(function (event) {
        if (event.keyCode === 13) {
            event.preventDefault();
            $("#btnLogin").click();
        }
    });

    $("#btnSubmit").click(function () {
        var data = {
            Id: parseInt($("#editingId").val()),
            FromDate: $("#FromDate").val(),
            ToDate: $("#ToDate").val(),
            Description: $("#Description").val(),
        }
        if (data.Id == '') data.Id = 0;

        if ($("#isEditing").html() == "false") {
            $.ajax({
                type: 'POST',
                url: '/api/Holidays',
                contentType: 'application/json',
                data: JSON.stringify(data)
            }).done(function (response) {
                tempAlert("Thêm ngày nghỉ thành công!", 2000);
                getHolidays("load");

            }).fail(function (xhr, status, error) {
                if (xhr.responseText.includes("Holiday existed!")) {
                    tempAlert("Ngày nghỉ đã tồn tại!", 2000);
                } else if (xhr.responseText.includes("Out of range date!")) {
                    tempAlert("Ngày nghỉ quá dài!", 2000);
                } else if (xhr.responseText.includes("Invalid holiday!")) {
                    tempAlert("Ngày nghỉ không hợp lệ!", 2000);
                } else {
                    tempAlert("Lỗi không xác định!", 2000);
                }

            });
        } else {
            $.ajax({
                type: 'PUT',
                url: '/api/Holidays',
                contentType: 'application/json',
                data: JSON.stringify(data)
            }).done(function (response) {
                tempAlert("Cập nhật ngày nghỉ thành công!", 2000);
                getHolidays("load");

            }).fail(function (xhr, status, error) {
                if (xhr.responseText.includes("Holiday existed!")) {
                    tempAlert("Ngày nghỉ đã tồn tại!", 2000);
                } else if (xhr.responseText.includes("Out of range date!")) {
                    tempAlert("Ngày nghỉ quá dài!", 2000);
                } else if (xhr.responseText.includes("Invalid holiday!")) {
                    tempAlert("Ngày nghỉ không hợp lệ!", 2000);
                } else {
                    tempAlert("Lỗi không xác định!", 2000);
                }

            });
            $("#isEditing").html("false");

        }
        $("#editModal").modal('hide');

        //setTimeout(function () {
        //    location.reload()
        //}, 3000);       

    });

    $("#btnHideModal").click(function () {
        $("#isEditing").html("false");
        $("#editModal").modal('hide');
    });

    $("#btnRemoveYes").click(function () {
        var data = {
            Id: $("#editingId").val()
        }
        $.ajax({
            type: 'DELETE',
            url: '/api/Holidays',
            contentType: 'application/json',
            data: JSON.stringify(data)
        }).done(function (response) {
            tempAlert("Xóa ngày nghỉ thành công!", 2000);
            getHolidays("load");
        }).fail(function (xhr, status, error) {
            if (xhr.responseText.includes("Holiday not found!")) {
                tempAlert("Ngày nghỉ không tồn tại!", 2000);
            } else {
                tempAlert("Lỗi không xác định!", 2000);
            }
        });
        $("#holidayRemoveModal").modal('hide');

        //setTimeout(function () {
        //    location.reload()
        //}, 3000);        
    });

    $("#btnRemoveNo").click(function () {
        $("#holidayRemoveModal").modal('hide');
    });
    getHolidays("load");

});

function getHolidays(mode, i = 0) {
    $.get("/api/Holidays", data = {}, function (response) {
        if (mode == "load") {
            bindHolidays(response);
        } else if (mode == "edit") {
            InitEdit(i, response);
        }
    });
}

var rowsShown = 8;

function bindHolidays(result) {
    let width = screen.width;

    var html = "";
    var idx = 0;
    $("#listHolidays").html("");
    $.each(result,
        function (h_index, holiday) {
            idx = h_index;
            html += "<tr>";

            html += "<td style='text-align: start;'>"
            html += holiday.description + "</td>"
            html += "<td style='text-align: start;'>" + moment(holiday.fromDate).format('DD/MM/YYYY') + " " + holiday.fromDay + "</td>";
            html += "<td style='text-align: start;'>" + moment(holiday.toDate).format('DD/MM/YYYY') + " " + holiday.toDay + "</td>";

            html += "<td style='text-align: center;'>" + holiday.totalDays + "</td>";
            html += "<td class='btn-icon d-none d-md-table-cell' style='text-align: center;' onclick='EditHoliday(" + holiday.id + ")'>" +
                "<i class='fa fa-pencil' id='sortTh'></i> </td>";
            html += "<td class='btn-icon d-none d-md-table-cell' style='text-align: center;' onclick='RemoveHoliday(" + holiday.id + "," + idx + ")'>" +
                "<i class='fa fa-trash' id='sortTh'></i> </td>";
            html += "<td colspan='2' class='d-table-cell d-md-none'>  <div class='btn-icon' style='text-align: center;' onclick='EditHoliday(" + holiday.id + ")'>" +
                "<i class='fa fa-pencil' id='sortTh'></i> </div>";
            html += "<div class='btn-icon' style='text-align: center;' onclick='RemoveHoliday(" + holiday.id + "," + idx + ")'>" +
                "<i class='fa fa-trash' id='sortTh'></i> </div></td>";
            html += "</tr>";

        });


    var rowsTotal = result.length;
    var numPages = Math.ceil(rowsTotal / rowsShown);
    if (rowsTotal % rowsShown != 0) {
        for (var i = rowsTotal % rowsShown; i < rowsShown; i++) {
            idx = idx + 1;
            html += "<tr>";
            html += "<td style='color: transparent'>NULL</td>"
            html += "<td ></td>";
            html += "<td ></td>";
            html += "<td ></td>";
            html += "<td class='btn-icon'></td>";
            html += "<td class='btn-icon'></td>";
            html += "</tr>";
        }
    }
    if (rowsTotal > 1) {
        $('#nav').append('<em class="d-none d-sm-inline pl-2" style="left: 0; position: absolute;">Tổng số kết quả:' +
            '<em class="d-none d-sm-inline" style="font-weight:bold;"> ' + rowsTotal + '</em></em>');
    }

    $("#listHolidays").empty();
    $("#listHolidays").append(html);
    $("#totalHolidays").html(rowsTotal);
    $("#nav").empty();
    if (rowsTotal == 0) {
        $("#ListTable").addClass("h-100");
    } else {
        $("#ListTable").removeClass('h-100');
    }

    $('#nav').append('<span onclick="Previous()" class="d-none p-2 span-link" href="#" ">Quay lại</span>');
    for (i = 0; i < numPages; i++) {
        if (i == 2) {
            $('#nav').append('<span id="decoratorL" class="d-none p-2">...</span> ');
        } else if (i == numPages - 2) {
            $('#nav').append('<span id="decoratorR" class="d-none p-2">...</span> ');
        }
        var pageNum = i + 1;
        $('#nav').append('<a onclick="SelectPage(' + i + ')" class="p-2" href="#" rel="' + i + '" id="' + i + '">' + pageNum + '</a> ');
    }
    $('#nav').append('<span onclick="Next()" class="p-2 span-link" href="#" ">Tiếp theo</span>');
    if (numPages < 2) {
        $('#nav span:last').removeClass('d-block');
        $('#nav span:last').addClass('d-none');
    }

    $('#nav a').addClass('d-none');
    for (j = 0; j < 5; j++) {
        var nav = $('#' + j.toString());
        nav.removeClass('d-none');
        nav.addClass('d-block');
    }

    $('#ListTable_Holiday tbody tr').hide();
    $('#ListTable_Holiday tbody tr').slice(0, rowsShown).show();
    $('#nav a:first').addClass('active');
    if (numPages == 6) {
        $('#nav a:first').removeClass('d-none');
        $('#nav a:first').addClass('d-block');
    } else if (numPages > 6) {
        $('#decoratorR').removeClass('d-none');
        $('#decoratorR').addClass('d-block');
        $('#nav a:last').removeClass('d-none');
        $('#nav a:last').addClass('d-block');
    }
    //if (numPages > 7) {
    //    $('#nav').append('<input id="jumpId" style="width:50px;"></input>');
    //    $('#nav').append('<span onclick="PageJump()" class="p-2 span-link" href="#" ">Đến</span>');
    //}
    $('#nav a').bind('click', function () {
        $('#nav a').removeClass('active');
        $(this).addClass('active');
        var currPage = $(this).attr('rel');
        var startItem = currPage * rowsShown;
        var endItem = startItem + rowsShown;
        $('#ListTable_Holiday tbody tr').css('opacity', '0.0').hide().slice(startItem, endItem).
            css('display', 'table-row').animate({ opacity: 1 }, 300);
    });
}

function SelectPage(i) {
    if (isNaN(i)) return false;
    var rowsTotal = $('#ListTable_Holiday tbody tr').length;
    var numPages = Math.ceil(rowsTotal / rowsShown);

    var startIdx = i - 2;
    var endIdx = i + 3;

    if (startIdx < 0) {
        endIdx -= startIdx;
        startIdx = 0;
    }

    if (endIdx > numPages) {
        startIdx -= endIdx - numPages;
        endIdx = numPages;
    }

    $('#nav a').removeClass('d-block');
    $('#nav a').addClass('d-none');
    if (i > 3) {
        if (numPages > 6) {
            $('#decoratorL').removeClass('d-none');
            $('#decoratorL').addClass('d-block');
        }
        $('#nav a:first').removeClass('d-none');
        $('#nav a:first').addClass('d-block');
    } else if (i == 3) {
        $('#decoratorL').removeClass('d-block');
        $('#decoratorL').addClass('d-none');
        $('#nav a:first').removeClass('d-none');
        $('#nav a:first').addClass('d-block');
    } else {
        $('#decoratorL').removeClass('d-block');
        $('#decoratorL').addClass('d-none');
    }

    if (i < numPages - 4) {
        if (numPages > 6) {
            $('#decoratorR').removeClass('d-none');
            $('#decoratorR').addClass('d-block');
            $('#nav a:last').removeClass('d-none');
            $('#nav a:last').addClass('d-block');
        }
    } else if (i == numPages - 4) {
        $('#decoratorR').removeClass('d-block');
        $('#decoratorR').addClass('d-none');
        $('#nav a:last').removeClass('d-none');
        $('#nav a:last').addClass('d-block');
    } else {
        $('#decoratorR').removeClass('d-block');
        $('#decoratorR').addClass('d-none');
    }
    for (j = startIdx; j < endIdx; j++) {
        var nav = $('#' + j.toString());
        nav.removeClass('d-none');
        nav.addClass('d-block');
    }

    if (i != 0) {
        $('#nav span:first').removeClass('d-none');
        $('#nav span:first').addClass('d-block');
    } else {
        $('#nav span:first').removeClass('d-block');
        $('#nav span:first').addClass('d-none');
    }

    if (i != numPages - 1) {
        $('#nav span:last').removeClass('d-none');
        $('#nav span:last').addClass('d-block');
    } else {
        $('#nav span:last').removeClass('d-block');
        $('#nav span:last').addClass('d-none');
    }
    $('#ListTable_Holiday tbody tr').hide();
    $('#ListTable_Holiday tbody tr').slice(i * rowsShown, i * rowsShown + rowsShown).show();

    $('#nav a').removeClass('active');
    $('#' + i + '').addClass('active');
    var startItem = i * rowsShown;
    var endItem = startItem + rowsShown;
    $('#ListTable_Holiday tbody tr').css('opacity', '0.0').hide().slice(startItem, endItem).
        css('display', 'table-row').animate({ opacity: 1 }, 300);
}

function PageJump() {
    SelectPage(parseInt(document.getElementById("jumpId").value));
}

function Previous() {
    var index = document.getElementsByClassName("active").item(0).innerHTML;
    SelectPage(parseInt(index) - 2);
}

function Next() {
    var index = document.getElementsByClassName("active").item(0).innerHTML;
    SelectPage(parseInt(index));
}

function HolidaysLogin() {
    $.ajax({
        type: 'GET',
        url: '/api/HolidaysLogin',
        contentType: 'application/json',
        data: {}
    }).done(function (response) {
        if (response != "Logged!") {
            $("#loginModal").modal('show');
        } else {
            switch (window.curAction) {
                case 'Add':
                    {
                        DoAddHoliday();
                        break;
                    }
                case 'Edit':
                    {
                        DoEditHoliday(window.curIndex);
                        break;
                    }
                case 'Remove':
                    {
                        DoRemoveHoliday(window.curHolidayId, window.curIndex);
                    }
            }
        }
    });

}

function AddHoliday() {
    window.curAction = 'Add';
    HolidaysLogin();
}

function DoAddHoliday() {
    document.getElementById('FromDate').valueAsDate = new Date();
    document.getElementById('ToDate').valueAsDate = new Date();
    $("#Description").val("");
    $("#editModal").modal('show');
}

function EditHoliday(idx) {
    window.curAction = 'Edit';
    window.curIndex = idx;
    HolidaysLogin();
}

function DoEditHoliday(idx) {
    $("#isEditing").html("true");
    $("#editingId").val(idx);
    getHolidays("edit", idx);
}

function RemoveHoliday(holidayId, idx) {
    window.curAction = "Remove"
    window.curIndex = idx;
    window.curHolidayId = holidayId;
    HolidaysLogin();
}

function DoRemoveHoliday(holidayId, idx) {
    var title = document.getElementsByTagName("tr")[idx + 1].getElementsByTagName("td")[0].innerHTML;
    $("#removeTitle").html("Bạn có muốn xóa ngày nghỉ " + title + " không ?");
    $("#editingId").val(holidayId);
    $("#holidayRemoveModal").modal('show');
}

function InitEdit(i, holidays) {
    var selectedHoliday;
    for (var idx = 0; idx < holidays.length; idx++) {
        if (holidays[idx].id == i) {
            selectedHoliday = holidays[idx];
            break;
        }
    }
    document.getElementById('FromDate').valueAsDate = moment(new Date(selectedHoliday.fromDate)).add(1, 'd').toDate();
    document.getElementById('ToDate').valueAsDate = moment(new Date(selectedHoliday.toDate)).add(1, 'd').toDate();

    document.querySelectorAll('input[type=date]').forEach(inp => {
        inp.setAttribute(
            "data-date",
            moment(inp.value)
                .format(inp.getAttribute("data-date-format")));
    });

    $('#ToDate').attr('min', moment($('#FromDate').val()).format("YYYY-MM-DD"));
    $('#FromDate').attr('max', moment($('#ToDate').val()).format("YYYY-MM-DD"));

    $("#Description").val(selectedHoliday.description);
    $("#editModal").modal('show');

}