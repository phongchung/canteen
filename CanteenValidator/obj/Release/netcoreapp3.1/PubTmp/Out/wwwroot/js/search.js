﻿//$("#Days").change(function () {
//    searchTransactions();
//});

//$("#Months").change(function () {
//    searchTransactions();
//});

//$("#Years").change(function () {
//    searchTransactions();
//});

//$("#Types").change(function () {
//    searchTransactions();
//});


$(document).ready(function () {
    document.getElementById('FromDate').valueAsDate = new Date();
    document.getElementById('ToDate').valueAsDate = new Date();

    $("#Companies").change(function () {
        searchTransactions();
    });

    $("#Departments").change(function () {
        searchTransactions();
    });

    $("#FromDate").change(function () {
        $('#ToDate').attr('min', moment($('#FromDate').val()).format("YYYY-MM-DD"));
        searchTransactions();
    });

    $("#ToDate").change(function () {
        $('#FromDate').attr('max', moment($('#ToDate').val()).format("YYYY-MM-DD"));
        searchTransactions();
    });


    const staffName = document.getElementById("StaffName");
    staffName.addEventListener('input', function () {
        searchTransactions();
    });

    $('#ListTable').after('<div id="nav" class="d-flex justify-content-center pt-0 page-nav"></div>');
    getStaffNames();
    searchTransactions();
});

function getStaffNames() {
    $.get("/api/StaffNames", data = {}, function (response) {
        var staffNames = [];
        $.each(response, function (index, staff) {
            staffNames.push(staff.name);
        });
        autocomplete(document.getElementById("StaffName"), staffNames);

    });
}

var rowsShown = 10;

function searchTransactions() {

    var data = {
        companyID: getInt($("#Companies").val()),
        departmentID: getInt($("#Departments").val()),
        staffName: $("#StaffName").val(),
        fromDate: $("#FromDate").val(),
        toDate: $("#ToDate").val()
        //typeID: $("#Types").val()
    }
    $.ajax({
        type: 'POST',
        url: '/api/Search',
        contentType: 'application/json',
        data: JSON.stringify(data)
    }).done(function (response) {
        bindSearch(response);
    });
}


function bindSearch(result) {
    var html = "";
    var idx = 0;
    $("#listSearch").html("");
    $.each(result.transactionsViewModel,
        function (index, model) {
            idx = idx + 1;
            html += "<tr>";

            html += "<td style='text-align: center;' data-a-h='center' data-b-a-s='thin' data-t='n'>" + idx + "</td>";
            html += "<td style='text-align: center;' data-a-h='center' data-b-a-s='thin'>" + model.company + "</td>";
            html += "<td data-a-h='left' data-b-a-s='thin'>" + model.department + "</td>";
            html += "<td data-a-h='left' data-b-a-s='thin' class='name'>" + model.name + "</td>";
            //html += "<td style='text-align: end;'>" + model.used + "</td>";
            //html += "<td class='d-none d-md-table-cell' style='text-align: end;'>" + model.available + "</td>";
            //html += "<td style='text-align: end;'>" + model.type + "</td>";
            html += "<td style='text-align: center;' data-a-h='center' data-b-a-s='thin'>" + moment(model.date).format('DD/MM/YYYY HH:mm:ss') + "</td>";
            //html += "<td class='d-none d-md-table-cell' style='text-align: end;' data-a-h='right' data-b-a-s='thin' data-t='n'" + model.available + "</td>";

            html += "</tr>";

        });
    var rowsTotal = result.transactionsViewModel.length;

    if (rowsTotal % 10 != 0) {
        for (var i = rowsTotal % 10; i < 10; i++) {
            var idx = idx + 1;
            html += "<tr data-exclude='true' >";

            html += "<td style='text-align: center;'>" + idx + "</td>";
            html += "<td></td>";
            html += "<td></td>";
            html += "<td></td>";
            html += "<td></td>";

            html += "</tr>";
        }
    }
    $("#listSearch").empty();
    $("#listSearch").append(html);
    $("#nav").empty();
    if (rowsTotal == 0) {
        $("#ListTable").addClass("h-100");
    } else {
        $("#ListTable").removeClass('h-100');
    }

    var numPages = Math.ceil(rowsTotal / rowsShown);
    $("#totalTransactions").html(rowsTotal);
    //if (rowsTotal > 1) {
    //    $('#nav').append('<em class="d-none d-sm-inline pl-2" style="left: 0; position: absolute;">Tổng số kết quả:'
    //        + '<em class="d-none d-sm-inline" style="font-weight:bold;"> ' + rowsTotal + '</em></em>');
    //}

    $('#nav').append('<span onclick="Previous()" class="d-none p-2 span-link" href="#" ">Quay lại</span>');
    for (i = 0; i < numPages; i++) {
        if (i == 2) {
            $('#nav').append('<span id="decoratorL" class="d-none p-2">...</span> ');
        } else if (i == numPages - 2) {
            $('#nav').append('<span id="decoratorR" class="d-none p-2">...</span> ');
        }
        var pageNum = i + 1;
        $('#nav').append('<a onclick="SelectPage(' + i + ')" class="p-2" href="#" rel="' + i + '" id="' + i + '">' + pageNum + '</a> ');
    }
    $('#nav').append('<span onclick="Next()" class="p-2 span-link" href="#" ">Tiếp theo</span>');
    if (numPages < 2) {
        $('#nav span:last').removeClass('d-block');
        $('#nav span:last').addClass('d-none');
    }

    $('#nav a').addClass('d-none');
    for (j = 0; j < 5; j++) {
        var nav = $('#' + j.toString());
        nav.removeClass('d-none');
        nav.addClass('d-block');
    }

    $('#ListTable tbody tr').hide();
    $('#ListTable tbody tr').slice(0, rowsShown).show();
    $('#nav a:first').addClass('active');
    if (numPages == 6) {
        $('#nav a:first').removeClass('d-none');
        $('#nav a:first').addClass('d-block');
    } else if (numPages > 6) {
        $('#decoratorR').removeClass('d-none');
        $('#decoratorR').addClass('d-block');
        $('#nav a:last').removeClass('d-none');
        $('#nav a:last').addClass('d-block');
    }
    //if (numPages > 7) {
    //    $('#nav').append('<input id="jumpId" style="width:50px;"></input>');
    //    $('#nav').append('<span onclick="PageJump()" class="p-2 span-link" href="#" ">Đến</span>');
    //}
    $('#nav a').bind('click', function () {
        $('#nav a').removeClass('active');
        $(this).addClass('active');
        var currPage = $(this).attr('rel');
        var startItem = currPage * rowsShown;
        var endItem = startItem + rowsShown;
        $('#ListTable tbody tr').css('opacity', '0.0').hide().slice(startItem, endItem).
            css('display', 'table-row').animate({ opacity: 1 }, 300);
    });
    sortPaginatedTable('ListTable', 4, 'string', 2);
}

function SelectPage(i) {
    if (isNaN(i)) return false;
    var rowsTotal = $('#ListTable tbody tr').length;
    var numPages = Math.ceil(rowsTotal / rowsShown);

    var startIdx = i - 2;
    var endIdx = i + 3;

    if (startIdx < 0) {
        endIdx -= startIdx;
        startIdx = 0;
    }

    if (endIdx > numPages) {
        startIdx -= endIdx - numPages;
        endIdx = numPages;
    }

    $('#nav a').removeClass('d-block');
    $('#nav a').addClass('d-none');
    if (i > 3) {
        if (numPages > 6) {
            $('#decoratorL').removeClass('d-none');
            $('#decoratorL').addClass('d-block');
        }
        $('#nav a:first').removeClass('d-none');
        $('#nav a:first').addClass('d-block');
    } else if (i == 3) {
        $('#decoratorL').removeClass('d-block');
        $('#decoratorL').addClass('d-none');
        $('#nav a:first').removeClass('d-none');
        $('#nav a:first').addClass('d-block');
    } else {
        $('#decoratorL').removeClass('d-block');
        $('#decoratorL').addClass('d-none');
    }

    if (i < numPages - 4) {
        if (numPages > 6) {
            $('#decoratorR').removeClass('d-none');
            $('#decoratorR').addClass('d-block');
            $('#nav a:last').removeClass('d-none');
            $('#nav a:last').addClass('d-block');
        }
    } else if (i == numPages - 4) {
        $('#decoratorR').removeClass('d-block');
        $('#decoratorR').addClass('d-none');
        $('#nav a:last').removeClass('d-none');
        $('#nav a:last').addClass('d-block');
    } else {
        $('#decoratorR').removeClass('d-block');
        $('#decoratorR').addClass('d-none');
    }
    for (j = startIdx; j < endIdx; j++) {
        var nav = $('#' + j.toString());
        nav.removeClass('d-none');
        nav.addClass('d-block');
    }

    if (i != 0) {
        $('#nav span:first').removeClass('d-none');
        $('#nav span:first').addClass('d-block');
    } else {
        $('#nav span:first').removeClass('d-block');
        $('#nav span:first').addClass('d-none');
    }

    if (i != numPages - 1) {
        $('#nav span:last').removeClass('d-none');
        $('#nav span:last').addClass('d-block');
    } else {
        $('#nav span:last').removeClass('d-block');
        $('#nav span:last').addClass('d-none');
    }
    $('#ListTable tbody tr').hide();
    $('#ListTable tbody tr').slice(i * rowsShown, i * rowsShown + rowsShown).show();

    $('#nav a').removeClass('active');
    $('#' + i + '').addClass('active');
    var startItem = i * rowsShown;
    var endItem = startItem + rowsShown;
    $('#ListTable tbody tr').css('opacity', '0.0').hide().slice(startItem, endItem).
        css('display', 'table-row').animate({ opacity: 1 }, 300);
}

function PageJump() {
    SelectPage(parseInt(document.getElementById("jumpId").value));
}

function Previous() {
    var index = document.getElementsByClassName("active").item(0).innerHTML;
    SelectPage(parseInt(index) - 2);
}

function Next() {
    var index = document.getElementsByClassName("active").item(0).innerHTML;
    SelectPage(parseInt(index));
}

function dayInMonth(month, year, day) {
    return new Date(year, month, day);
}

function getDateFormat(date) {
    var day = date.getDate();
    var displayMonth = date.getMonth() + 1;
    var year = date.getFullYear();
    day = day < 10 ? '0' + day : day;
    displayMonth = displayMonth < 10 ? '0' + displayMonth : displayMonth;
    return `${day}/${displayMonth}/${year}`;
}

function autocomplete(inp, arr) {
    /*the autocomplete function takes two arguments,
    the text field element and an array of possible autocompleted values:*/
    var currentFocus;
    /*execute a function when someone writes in the text field:*/
    inp.addEventListener("input", function (e) {
        var a, b, i, val = this.value;
        /*close any already open lists of autocompleted values*/
        closeAllLists();
        if (!val) { return false; }
        currentFocus = -1;
        /*create a DIV element that will contain the items (values):*/
        a = document.createElement("DIV");
        a.setAttribute("id", this.id + "autocomplete-list");
        a.setAttribute("class", "autocomplete-items");
        /*append the DIV element as a child of the autocomplete container:*/
        this.parentNode.appendChild(a);
        /*for each item in the array...*/
        for (i = 0; i < arr.length; i++) {
            var searchPos = arr[i].toLowerCase().search(val.toLowerCase());
            /*check if the item starts with the same letters as the text field value:*/
            if (searchPos != -1) {
                /*create a DIV element for each matching element:*/
                b = document.createElement("DIV");
                /*make the matching letters bold:*/
                b.innerHTML += arr[i].substring(0, searchPos) + "<strong>" + arr[i].substring(searchPos, val.length + searchPos) + "</strong>" + arr[i].substring(val.length + searchPos, arr[i].length);
                /*insert a input field that will hold the current array item's value:*/
                b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
                /*execute a function when someone clicks on the item value (DIV element):*/
                b.addEventListener("click", function (e) {
                    /*insert the value for the autocomplete text field:*/
                    inp.value = this.getElementsByTagName("input")[0].value;
                    /*close the list of autocompleted values,
                    (or any other open lists of autocompleted values:*/
                    closeAllLists();
                    searchTransactions();
                });
                a.appendChild(b);
            }
        }
    });
    /*execute a function presses a key on the keyboard:*/
    inp.addEventListener("keydown", function (e) {
        var x = document.getElementById(this.id + "autocomplete-list");
        if (x) x = x.getElementsByTagName("div");
        if (e.keyCode == 40) {
            /*If the arrow DOWN key is pressed,
            increase the currentFocus variable:*/
            currentFocus++;
            /*and and make the current item more visible:*/
            addActive(x);
        } else if (e.keyCode == 38) { //up
            /*If the arrow UP key is pressed,
            decrease the currentFocus variable:*/
            currentFocus--;
            /*and and make the current item more visible:*/
            addActive(x);
        } else if (e.keyCode == 13) {
            /*If the ENTER key is pressed, prevent the form from being submitted,*/
            e.preventDefault();
            if (currentFocus > -1) {
                /*and simulate a click on the "active" item:*/
                if (x) x[currentFocus].click();
            }
        }
    });

    function addActive(x) {
        /*a function to classify an item as "active":*/
        if (!x) return false;
        /*start by removing the "active" class on all items:*/
        removeActive(x);
        if (currentFocus >= x.length) currentFocus = 0;
        if (currentFocus < 0) currentFocus = (x.length - 1);
        /*add class "autocomplete-active":*/
        x[currentFocus].classList.add("autocomplete-active");
    }

    function removeActive(x) {
        /*a function to remove the "active" class from all autocomplete items:*/
        for (var i = 0; i < x.length; i++) {
            x[i].classList.remove("autocomplete-active");
        }
    }

    function closeAllLists(elmnt) {
        /*close all autocomplete lists in the document,
        except the one passed as an argument:*/
        var x = document.getElementsByClassName("autocomplete-items");
        for (var i = 0; i < x.length; i++) {
            if (elmnt != x[i] && elmnt != inp) {
                x[i].parentNode.removeChild(x[i]);
            }
        }
    }
    /*execute a function when someone clicks in the document:*/
    document.addEventListener("click", function (e) {
        closeAllLists(e.target);
    });
}

function getStaffReport() {
    var curDate = new Date();
    var fromDate = moment($("#FromDate").val()).format('DD/MM/YYYY');
    var toDate = moment($("#ToDate").val()).format('DD/MM/YYYY');

    $('#StatisticsReportTimeHeader').html("Từ " + fromDate + " đến " + toDate);

    TableToExcel.convert(document.getElementById("ListTable"), {
        name: "StatisticsReport_" + moment(curDate).format('DD_MM_YYYY_HH_mm_ss') + ".xlsx",
        sheet: {
            name: "Sheet1"
        }
    });
}