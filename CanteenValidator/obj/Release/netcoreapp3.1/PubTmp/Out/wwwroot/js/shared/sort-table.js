var dir = "desc";
function sortPaginatedTable(table_name, n, type = 'string', skipStart = 0, skipEnd = 0) {
    //console.time('doSomething');
    try {
        var table, rows, i;
        table = document.getElementById(table_name);
        var index = document.getElementsByClassName("active").item(0).innerHTML;
        var comp = new Intl.Collator('vn');
        var max_idx = 0, min_idx = 0;
        var values = [];

        rows = table.rows;
        if (type == "string") {
            values.push("");

            for (var o = 1 + skipStart; o < rows.length - skipEnd; o++) {
                //console.log(o);
                if (rows[o].getElementsByTagName("TD")[1].innerHTML != "") {
                    values.push(rows[o].getElementsByTagName("TD")[n].innerHTML);
                }
                else {
                    values.push("-1");
                }
            }
            /* Loop through all table rows (except the
            first, which contains table headers): */
            if (window.dir == "desc") {
                for (i = 1 + skipStart; i < rows.length - 1 - skipEnd; i++) {
                    max_idx = i;

                    for (j = i + 1; j < rows.length - skipEnd; j++) {
                        if (comp.compare(values[max_idx - skipStart], values[j - skipStart]) < 0) {
                            if (values[j - skipStart] != "-1") {
                                max_idx = j;
                            }
                        }
                    }
                    if (max_idx != i) {
                        rows[i].parentNode.insertBefore(rows[max_idx], rows[i]);
                        values.splice(i - skipStart, 0, values[max_idx]);
                        values.splice(max_idx + 1 - skipStart, 1);
                    }
                    if (n != 0) rows[i].getElementsByTagName("TD")[0].innerHTML = i - skipStart;

                }
                window.dir = "asc";
            }
            else if (window.dir == "asc") {
                for (i = 1 + skipStart; i < rows.length - 1 - skipEnd; i++) {
                    min_idx = i;

                    for (j = i + 1; j < rows.length - skipEnd; j++) {
                        if (comp.compare(values[min_idx - skipStart], values[j - skipStart]) > 0) {
                            if (values[j - skipStart] != "-1") {
                                min_idx = j;
                            }
                        }
                    }
                    if (min_idx != i) {
                        rows[i].parentNode.insertBefore(rows[min_idx], rows[i]);
                        values.splice(i - skipStart, 0, values[min_idx]);
                        values.splice(min_idx + 1 - skipStart, 1);
                    }
                    if (n != 0) rows[i].getElementsByTagName("TD")[0].innerHTML = i - skipStart;
                }
                window.dir = "desc";
            }
        }
        else {
            values.push(-1);
            //console.log(rows);
            for (var o = 1 + skipStart; o < rows.length - skipEnd; o++) {
                //console.log(o);
                if (rows[o].getElementsByTagName("TD")[1].innerHTML != "") {
                    values.push(parseInt(rows[o].getElementsByTagName("TD")[n].innerHTML));
                }
                else {
                    values.push(-1);
                }
            }

            /* Loop through all table rows (except the
            first, which contains table headers): */
            if (window.dir == "desc") {
                for (i = 1 + skipStart; i < rows.length - 1 - skipEnd; i++) {
                    max_idx = i;

                    for (j = i + 1; j < rows.length - skipEnd; j++) {
                        if (values[max_idx - skipStart] < values[j - skipStart]) {
                            if (values[j - skipStart] != -1) {
                                max_idx = j;
                            }
                        }
                    }
                    if (max_idx != i) {
                        rows[i].parentNode.insertBefore(rows[max_idx], rows[i]);
                        values.splice(i - skipStart, 0, values[max_idx]);
                        values.splice(max_idx + 1 - skipStart, 1);
                    }
                    if (n != 0) rows[i].getElementsByTagName("TD")[0].innerHTML = i - skipStart;
                }
                window.dir = "asc";
            }
            else if (window.dir == "asc") {
                for (i = 1 + skipStart; i < rows.length - 1 - skipEnd; i++) {

                    min_idx = i;

                    for (j = i + 1; j < rows.length - skipEnd; j++) {
                        if (values[min_idx - skipStart] > values[j - skipStart]) {
                            if (values[j - skipStart] != -1) {
                                min_idx = j;
                            }
                        }
                    }
                    if (min_idx != i) {
                        rows[i].parentNode.insertBefore(rows[min_idx], rows[i]);
                        values.splice(i - skipStart, 0, values[min_idx]);
                        values.splice(min_idx + 1 - skipStart, 1);
                    }
                    if (n != 0) rows[i].getElementsByTagName("TD")[0].innerHTML = i - skipStart;
                }
                window.dir = "desc";
            }
        }
        if (n != 0) rows[rows.length - 1 - skipEnd].getElementsByTagName("TD")[0].innerHTML = rows.length - 1 - skipStart - skipEnd;

        //console.timeEnd('doSomething');

        SelectPage(index - 1);
    }
    catch (err) {

    }
}

function sortTable(n, type = 'string') {
    try {
        var table, rows, i;
        table = document.getElementById("ListIndex");
        var comp = new Intl.Collator('vn');
        var max_idx = 0, min_idx = 0;
        var values = [];
        var dir = "desc";

        rows = table.rows;
        if (type == "string") {
            values.push("");
            for (i = 1; i < rows.length; i++) {
                values.push(rows[i].getElementsByTagName("TD")[n].innerHTML);
            }
            /* Loop through all table rows (except the
            first, which contains table headers): */
            if (dir == "desc") {
                for (i = 1; i < rows.length - 1; i++) {
                    max_idx = i;

                    for (j = i + 1; j < rows.length; j++) {
                        if (comp.compare(values[max_idx], values[j]) < 0) {
                            // If so, mark as a switch and break the loop:
                            max_idx = j;
                        }
                    }
                    if (max_idx != i) {
                        rows[i].parentNode.insertBefore(rows[max_idx], rows[i]);
                        values.splice(i, 0, values[max_idx]);
                        values.splice(max_idx + 1, 1);
                    }
                }
                dir = "asc";
            }
            else if (dir == "asc") {
                for (i = 1; i < rows.length - 1; i++) {

                    min_idx = i;

                    for (j = i + 1; j < rows.length; j++) {
                        if (comp.compare(values[min_idx], values[j]) > 0) {
                            // If so, mark as a switch and break the loop:
                            min_idx = j;
                        }
                    }
                    if (min_idx != i) {
                        rows[i].parentNode.insertBefore(rows[min_idx], rows[i]);
                        values.splice(i, 0, values[min_idx]);
                        values.splice(min_idx + 1, 1);
                    }
                }
                dir = "desc";
            }
        }
        else {
            values.push(-1);
            for (i = 1; i < rows.length; i++) {
                values.push(parseInt(rows[i].getElementsByTagName("TD")[n].innerHTML));
            }

            /* Loop through all table rows (except the
            first, which contains table headers): */
            if (dir == "desc") {
                for (i = 1; i < rows.length - 1; i++) {
                    max_idx = i;

                    for (j = i + 1; j < rows.length; j++) {
                        if (values[max_idx] < values[j]) {
                            // If so, mark as a switch and break the loop:
                            max_idx = j;
                        }
                    }
                    if (max_idx != i) {
                        rows[i].parentNode.insertBefore(rows[max_idx], rows[i]);
                        values.splice(i, 0, values[max_idx]);
                        values.splice(max_idx + 1, 1);
                    }
                }
                dir = "asc";
            }
            else if (dir == "asc") {
                for (i = 1; i < rows.length - 1; i++) {

                    min_idx = i;

                    for (j = i + 1; j < rows.length; j++) {
                        if (values[min_idx] > values[j]) {
                            // If so, mark as a switch and break the loop:
                            min_idx = j;
                        }
                    }
                    if (min_idx != i) {
                        rows[i].parentNode.insertBefore(rows[min_idx], rows[i]);
                        values.splice(i, 0, values[min_idx]);
                        values.splice(min_idx + 1, 1);
                    }
                }
                dir = "desc";
            }
        }

        //console.timeEnd('doSomething');
    }
    catch (err) {

    }
}