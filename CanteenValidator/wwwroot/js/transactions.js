﻿Object.defineProperty(HTMLMediaElement.prototype, 'playing', {
    get: function () {
        return !!(this.currentTime > 0 && !this.paused && !this.ended && this.readyState > 2);
    }
})

$(document).ready(function () {
    var videoHeight = document.getElementsByClassName("videoContainer")[0].offsetHeight;
    var videoWidth = document.getElementsByClassName("videoContainer")[0].offsetWidth;
    var borderHeight = 0.17 * videoHeight;
    var borderWidth = (videoWidth - 0.66 * videoHeight)/2;

    console.log(videoHeight);
    document.getElementById("qr-shaded-region").style.borderWidth = borderHeight + "px " + borderWidth + "px";

    $(window).resize(function () {
        document.getElementById("qr-shaded-region").style.borderWidth = borderHeight + "px " + borderWidth + "px";
    });


    if (window.localStorage.getItem("counter") == null) {
        $("#selectCounterModal").modal('show');
    } else {
        document.getElementById('btnShowModal').innerHTML = window.localStorage.getItem("counter");
    }

    loadTransactions();

    $("#success-alert").hide();

    $("#btnShowModal").click(function () {
        $("#selectCounterModal").modal('show');
    });

    $("#btnSubmit").click(function () {
        $("#selectCounterModal").modal('hide');
    });

    $("#btnHideModal").click(function () {
        $("#selectCounterModal").modal('hide');
    });

});

$("#btnShowModal").change(function () {
    loadTransactions();
});

function tempAlert(msg, duration) {
    var content = document.getElementById("content_announce");
    content.innerHTML = msg;
    setTimeout(function () {
        window.location.href = "#";
    }, duration);
    window.location.href = "#popup_announce"
}

function loadTransactions() {
    $.get("/api/Transactions",
        data = { ID: document.getElementById("btnShowModal").innerHTML },
        function (response) {
            bindTransactions(response);
        });
}

function ImageExist(url) {
    var img = new Image();
    img.src = url;
    return img.height != 0;
}

function bindTransactions(response) {
    //console.log("Bind");
    var html = "";
    $("#listTransacions").html("");
    var count = 0;
    var curDate = new Date();

    $.each(response.transactions,
        function (index, transaction) {

            if (index == response.transactions.length - 1) {
                html += "<tr class='recent'  id=" + index + " value=" + transaction.id + ">";

                if (ImageExist("image/" + transaction.staffID + ".jpg")) {
                    $("#image").attr("src", "image/" + transaction.staffID + ".jpg");
                } else {
                    $("#image").attr("src", "image/user.png");
                }
                $("#name").html(transaction.name);
                $("#company").html(transaction.company);
                $("#department").html(transaction.department);
                // $("#available").html(transaction.available);
                $("#checkDate").html(moment(transaction.date).format('HH:mm:ss'));
            } else html += "<tr id=" + index + ">";

            html += "<td style='text-align: center;' value=" + transaction.company + ">" + transaction.company + "</td>";
            html += "<td class='text-link' style='padding-left: 1vw!important; padding-right: 1vw!important'' value=" + transaction.staffID + " href='#' onclick='searchSpecificTransaction(" + index + ", 1)'>" + transaction.name + "</td>";
            html += "<td style='text-align: center;' value=" + transaction.date + ">" + moment(transaction.date).format('HH:mm:ss') + "</td>";
            //html += "<td style='text-align: end;'>" + transaction.used + "</td>";
            // html += "<td style='text-align: center;'>" + transaction.available + "</td>";
            //html += "<td>" + transaction.type + "</td>";

            html += "</tr>";
            count++;
            //if (count == 10) return false;
        });
    var rowsTotal = response.transactions.length;
    if (rowsTotal < 1) {
        $("#name").html("Họ và tên");
        $("#company").html("Công ty");
        $("#department").html("Phòng ban");
        // $("#available").html("x");
        $("#checkDate").html("");
    }
    if (rowsTotal % 10 != 0) {
        for (var i = rowsTotal % 10; i < 10; i++) {
            var idx = idx + 1;
            html += "<tr>";

            html += "<td></td>";
            // html += "<td></td>";
            html += "<td></td>";
            html += "<td style='color: transparent;'>0</td>";

            html += "</tr>";
        }
    }

    $("#listTransactions").empty();
    $("#listTransactions").append(html);
    $("#totalUsed_I").html(response.count);

    sortTable(2);

    // Blink the first table row to indicate
    $('tbody tr:first').addClass('blink_me');
}

delay = false;
var delayInMilliseconds = 3000; //3 second
var success = document.getElementById('success_sound');
var outOfTickets = document.getElementById('outOfTickets_sound');
var invalidId = document.getElementById('invalidId_sound');

function addTransactions(request) {
    if (delay == false) {
        delay = true;

        var data = {
            qrcode: request,
            date: new Date(),
            counterID: parseInt(document.getElementById("btnShowModal").innerHTML)
        }

        window.location.href = "#popup_loading";

        $.ajax({
            type: 'PUT',
            url: 'api/Transactions',
            contentType: 'application/json',
            data: JSON.stringify(data)
        }).done(function (response) {
            if (response == "Success!") {
                success.play();
                //snap();
                loadTransactions();
                setTimeout(function () {
                    tempAlert("Thanh toán thành công!", 2000);
                }, 500);
                $('.btn-success').prop('disabled', false);
            }
        }).fail(function (xhr, status, error) {
            window.location.href = "#";
            if (xhr.responseText.includes("Out of tickets!")) {
                outOfTickets.play();
                tempAlert("Đã hết lần ăn hôm nay!", 2000);
            } else if (xhr.responseText.includes("ID not found!")) {
                invalidId.play();
                setTimeout(function () {
                    tempAlert("Không tìm được thông tin người dùng!", 2000);
                }, 500);
            } else {
                invalidId.play();
                setTimeout(function () {
                    tempAlert("Mã QR không hợp lệ!", 2000);
                }, 500);
            }

        });


        setTimeout(function () {
            delay = false;
        }, delayInMilliseconds);
    }
}

function searchSpecificTransaction(index, type) {
    var row = document.getElementById(index).getElementsByTagName("td");

    _company = row[0].getAttribute('value');
    _staffID = row[1].getAttribute('value');
    _date = row[2].getAttribute('value');

    var data = {
        company: _company,
        staffID: parseInt(_staffID),
        date: _date
    }

    $.ajax({
        type: 'POST',
        url: 'api/SearchSpecificTransaction',
        contentType: 'application/json',
        data: JSON.stringify(data)
    }).done(function (response) {
        var url = $('#RedirectTo').val();
        location.href = url;
    });
}

function undoTransaction() {
    var value = document.getElementsByClassName('recent')[0].getAttribute('value');

    var data = {}
    data.id = parseInt(value);
    $.ajax({
        type: 'DELETE',
        url: 'api/Transactions',
        contentType: 'application/json',
        data: JSON.stringify(data),
        datatype: "JSON",
    }).done(function (response) {
        if (response == "Success") {
            success.play();
            loadTransactions();
            tempAlert("Hoàn tác thành công!", 2000);
            $('.btn-success').prop('disabled', true);
        } else if (response == "Failed") {
            invalidId.play();
            tempAlert("Hoàn tác không thành công!", 2000);
        }
    });
}

var video = document.querySelector('video');
//var canvas = document.querySelector('canvas');
//var context = canvas.getContext('2d');
var w, h, ratio;

//add loadedmetadata which will helps to identify video attributes
//video.addEventListener('loadedmetadata', function () {
//    ratio = video.videoWidth / video.videoHeight;
//    w = 640;
//    h = 480;
//    canvas.width = w;
//    canvas.height = h;
//}, false);

function snap() {
    context.fillRect(0, 0, w, h);
    context.drawImage(video, 0, 0, w, h);
    var link = document.getElementById('link');
    link.setAttribute('download', 'MintyPaper.jpg');
    link.setAttribute('href', canvas.toDataURL("image/jpg").replace("image/jpg", "image/octet-stream"));
    link.click();
}

//const interval = setInterval(function () {
//    var status = document.getElementById('videoStatus');
//    var stream = document.getElementById('video');
//    if (stream.playing) {
//        status.innerHTML = "Online";
//        status.setAttribute("class", "dot_green");
//        status.style.color = "green"
//    }
//    else {
//        status.innerHTML = "Offline";
//        status.setAttribute("class", "dot_red");
//        status.style.color = "red"
//    }
//}, 3000);

//setInterval(interval);

//function ok() {
//    document.getElementById('btnShowModal').innerHTML = document.getElementById('selectCounter').value
//    counter(document.getElementById('selectCounter').value)
//}

//function genderChanged(obj) {
//    var message = document.getElementById('btnShowModal');
//    var value = obj.value;

//    if (value === '1') {
//        message.innerHTML = "1";
//        counter(value)
//    }
//    else if (value === '2') {
//        message.innerHTML = "2";
//        counter(value);
//    }
//    loadTransactions();
//}

function SetCounter(a) {
    document.getElementById("btnShowModal").innerHTML = a;
    window.localStorage.setItem("counter", a);

    loadTransactions();
}

var counter = function (a) {

    var data = { ID: a };
    $.ajax({
        type: 'PUT',
        url: 'api/counter',
        contentType: 'application/json',
        data: JSON.stringify(data)
    }).done(function (response) {
        if (response == false) {
            $("#selectCounterModal").modal('show');
        }
    });
}