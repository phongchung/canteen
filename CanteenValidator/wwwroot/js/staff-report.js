﻿$(document).ready(function () {

    document.getElementById('FromDate_S').valueAsDate = new Date();
    document.getElementById('ToDate_S').valueAsDate = new Date();

    $("input[type=date]").on("change", function () {
        this.setAttribute(
            "data-date",
            moment(this.value)
                .format(this.getAttribute("data-date-format"))
        )
    }).trigger("change")
});

$("#Companies_S").change(function () {
    getStaffReport();
});

$("#Departments_S").change(function () {
    getStaffReport();
});

//$("#Types").change(function () {
//    getStaffReport();
//});

$("#FromDate_S").change(function () {
    $('#ToDate_S').attr('min', moment($('#FromDate_S').val()).format("YYYY-MM-DD"));
    getStaffReport();
});

$("#ToDate_S").change(function () {
    $('#FromDate_S').attr('max', moment($('#ToDate_S').val()).format("YYYY-MM-DD"));
    getStaffReport();
});

$(document).ready(function () {
    $('#ListTable_S').after('<div id="nav_S" class="d-flex justify-content-center pt-0 page-nav"></div>');
    getStaffReport();
});

var rowsShown = 10;

function getStaffReport() {
    var data = {
        companyID: getInt($("#Companies_S").val()),
        departmentID: getInt($("#Departments_S").val()),
        //typeID: $("#Types").val(),
        fromDate: $("#FromDate_S").val(),
        toDate: $("#ToDate_S").val()
    }
    $.ajax({
        type: 'POST',
        url: '/api/StaffReport',
        contentType: 'application/json',
        data: JSON.stringify(data)
    }).done(function (response) {
        bindStaffReport(response);
    });
}


function bindStaffReport(result) {
    var html = "";
    var totalUsed = 0;
    // var totalAvailable = 0;
    var idx = 0;
    $.each(result, function (index, model) {
        idx = index + 1;
        totalUsed += model.used;
        // totalAvailable += model.available;

        html += "<tr>";

        html += "<td style='text-align: center;' data-a-h='center' data-b-a-s='thin' data-t='n'>" + idx + "</td>";
        html += "<td style='text-align: center;' data-a-h='center' data-b-a-s='thin'>" + model.company + "</td>";
        html += "<td data-exclude='false'>" + model.department + "</td>";
        html += "<td data-b-a-s='thin' class='name'>" + model.name + "</td>";

        html += "<td style='text-align: center;' data-a-h='right' data-b-a-s='thin' data-t='n'>" + model.used + "</td>";
        // html += "<td style='text-align: center;' data-exclude='true' data-b-a-s='thin'>" + model.available + "</td>";
        //html += "<td data-exclude='true'>" + model.type + "</td>";

        html += "</tr>";

    });
    var rowsTotal = result.length;
    var numPages = Math.ceil(rowsTotal / rowsShown);

    if (rowsTotal % 10 != 0) {
        for (var i = rowsTotal % 10; i < 10; i++) {
            var idx = idx + 1;
            html += "<tr data-exclude='true' >";

            html += "<td style='text-align: center;'>" + idx + "</td>";
            html += "<td></td>";
            html += "<td></td>";
            html += "<td></td>";
            html += "<td></td>";

            html += "</tr>";
        }
    }


    $("#totalUsed_S").html(totalUsed);
    // $("#totalAvailable_S").html(totalAvailable);
    $("#hiddenTotalUsed_S").html(totalUsed);
    // $("#hiddenTotalAvailable_S").html(totalAvailable);
    $("#listReport_S").empty();
    $("#listReport_S").append(html);
    $("#nav_S").empty();
    if (rowsTotal == 0) {
        $("#ListTable_S").addClass("h-100");
    } else {
        $("#ListTable_S").removeClass('h-100');
    }

    if (rowsTotal > 0) {
        $('#nav_S').append('<em class="d-none d-md-inline">Tổng số kết quả:' +
            '<span class="d-none d-md-inline" style="font-weight:bold;"> ' + rowsTotal + '</span></em> ');
    }
    $('#nav_S').append('<span onclick="Previous()" class="d-none p-2 span-link" href="#" ">Quay lại</span>');
    for (i = 0; i < numPages; i++) {
        if (i == 2) {
            $('#nav_S').append('<span id="decoratorL_S" class="d-none">...</span> ');
        } else if (i == numPages - 2) {
            $('#nav_S').append('<span id="decoratorR_S" class="d-none">...</span> ');
        }
        var pageNum = i + 1;
        $('#nav_S').append('<a onclick="SelectPage_S(' + i + ')" class="p-2" href="#" rel="' + i + '" id="S_' + i + '">' + pageNum + '</a> ');
    }
    $('#nav_S').append('<span onclick="Next()" class="p-2 span-link" href="#" ">Tiếp theo</span>');
    if (numPages < 2) {
        $('#nav_S span:last').removeClass('d-block');
        $('#nav_S span:last').addClass('d-none');
    }

    $('#nav_S a').addClass('d-none');
    for (j = 0; j < 5; j++) {
        var nav = $('#S_' + j.toString());
        nav.removeClass('d-none');
        nav.addClass('d-block');
    }

    $('#ListTable_S tbody tr').hide();
    $('#ListTable_S tbody tr').slice(0, rowsShown).show();
    $('#nav_S a:first').addClass('active');

    if (numPages == 6) {
        $('#nav_S a:first').removeClass('d-none');
        $('#nav_S a:first').addClass('d-block');
    } else if (numPages > 6) {
        $('#decoratorR_S').removeClass('d-none');
        $('#decoratorR_S').addClass('d-block');
        $('#nav_S a:last').removeClass('d-none');
        $('#nav_S a:last').addClass('d-block');
    }
    //if (numPages > 7) {
    //    $('#nav_S').append('<input id="jumpId" style="width:50px;"></input>');
    //    $('#nav_S').append('<span onclick="PageJump()" class="p-2 span-link" href="#" ">Đến</span>');
    //}
    $('#nav_S a').bind('click', function () {
        $('#nav_S a').removeClass('active');
        $(this).addClass('active');
        var currPage = $(this).attr('rel');
        var startItem = currPage * rowsShown;
        var endItem = startItem + rowsShown;
        $('#ListTable_S tbody tr').css('opacity', '0.0').hide().slice(startItem, endItem).
            css('display', 'table-row').animate({ opacity: 1 }, 300);
    });
}

function SelectPage_S(i) {
    if (isNaN(i)) return false;
    var rowsTotal = $('#ListTable_S tbody tr').length;
    var numPages = Math.ceil(rowsTotal / rowsShown);

    var startIdx = i - 2;
    var endIdx = i + 3;

    if (startIdx < 0) {
        endIdx -= startIdx;
        startIdx = 0;
    }

    if (endIdx > numPages) {
        startIdx -= endIdx - numPages;
        endIdx = numPages;
    }

    $('#nav_S a').removeClass('d-block');
    $('#nav_S a').addClass('d-none');

    if (numPages > 6) {
        if (i > 3) {
            $('#decoratorL_S').removeClass('d-none');
            $('#decoratorL_S').addClass('d-block');
            $('#nav_S a:first').removeClass('d-none');
            $('#nav_S a:first').addClass('d-block');
        } else if (i == 3) {
            $('#decoratorL_S').removeClass('d-block');
            $('#decoratorL_S').addClass('d-none');
            $('#nav_S a:first').removeClass('d-none');
            $('#nav_S a:first').addClass('d-block');
        } else {
            $('#decoratorL_S').removeClass('d-block');
            $('#decoratorL_S').addClass('d-none');
        }
    }

    if (i < numPages - 4) {
        if (numPages > 6) {
            $('#decoratorR_S').removeClass('d-none');
            $('#decoratorR_S').addClass('d-block');
            $('#nav_S a:last').removeClass('d-none');
            $('#nav_S a:last').addClass('d-block');
        }
    } else if (i == numPages - 4) {
        $('#decoratorR_S').removeClass('d-block');
        $('#decoratorR_S').addClass('d-none');
        $('#nav_S a:last').removeClass('d-none');
        $('#nav_S a:last').addClass('d-block');
    } else {
        $('#decoratorR_S').removeClass('d-block');
        $('#decoratorR_S').addClass('d-none');
    }
    for (j = startIdx; j < endIdx; j++) {
        var nav = $('#S_' + j.toString());
        nav.removeClass('d-none');
        nav.addClass('d-block');
    }

    if (numPages > 5) {
        if (i != 0) {
            $('#nav_S span:first').removeClass('d-none');
            $('#nav_S span:first').addClass('d-block');
        } else {
            $('#nav_S span:first').removeClass('d-block');
            $('#nav_S span:first').addClass('d-none');
        }

        if (i != numPages - 1) {
            $('#nav_S span:last').removeClass('d-none');
            $('#nav_S span:last').addClass('d-block');
        } else {
            $('#nav_S span:last').removeClass('d-block');
            $('#nav_S span:last').addClass('d-none');
        }
    }
    $('#ListTable_S tbody tr').hide();
    $('#ListTable_S tbody tr').slice(i * rowsShown, i * rowsShown + rowsShown).show();

    $('#nav_S a').removeClass('active');
    $('#S_' + i + '').addClass('active');
    var startItem = i * rowsShown;
    var endItem = startItem + rowsShown;
    $('#ListTable_S tbody tr').css('opacity', '0.0').hide().slice(startItem, endItem).
        css('display', 'table-row').animate({ opacity: 1 }, 300);
}

function Previous() {
    var index = document.getElementsByClassName("active").item(0).innerHTML;
    SelectPage_S(parseInt(index) - 2);
}

function Next() {
    var index = document.getElementsByClassName("active").item(0).innerHTML;
    SelectPage_S(parseInt(index));
}


function dayInMonth(month, year, day) {
    return new Date(year, month, day);
}

function getDateFormat(date) {
    var day = date.getDate();
    var displayMonth = date.getMonth() + 1;
    var year = date.getFullYear();
    day = day < 10 ? '0' + day : day;
    displayMonth = displayMonth < 10 ? '0' + displayMonth : displayMonth;
    return `${day}/${displayMonth}/${year}`;
}


function exportStaffReport() {
    var curDate = new Date();
    var fromDate = moment($("#FromDate_S").val()).format('DD/MM/YYYY');
    var toDate = moment($("#ToDate_S").val()).format('DD/MM/YYYY');

    $('#staffReportTimeHeader').html("Từ " + fromDate + " đến " + toDate);

    TableToExcel.convert(document.getElementById("ListTable_S"), {
        name: "StaffReport_" + moment(curDate).format('DD_MM_YYYY_HH_mm_ss') + ".xlsx",
        sheet: {
            name: "Sheet1"
        }
    });
}