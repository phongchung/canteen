﻿////const { each } = require("jquery");

var curAction = "Add";

function tempAlert(msg, duration) {
    var content = document.getElementById("content");
    content.innerHTML = msg;
    setTimeout(function () {
        window.location.href = "#";
    }, duration);
    window.location.href = "#popup_announce"
}

$(document).ready(function () {
    document.getElementById('startOfDate').valueAsDate = new Date();
    const staffName = document.getElementById("StaffName");

    $("input[type=radio]").change(function () {
        switch (this.id) {
            case "select-email":
                $("#choice-email").removeClass("d-none");
                document.getElementById("email").required = true;
                $("#choice-id").addClass("d-none");
                document.getElementById("id").required = false;
                break;
            case "select-id":
                $("#choice-email").addClass("d-none");
                document.getElementById("email").required = false;
                $("#choice-id").removeClass("d-none");
                document.getElementById("id").required = true;
                break;
        }
    });

    $("#btnConfirm").click(function () {
        if (window.curAction == "Add") {
            var data = {
                name: $("#name").val(),
                companyID: getInt($('#companyID').val()),
                departmentID: getInt($('#departmentID').val()),
                role: $("#role").val(),
                startOfDate: $('#startOfDate').val(),
                staffType: getInt($('input[name=staffType]:checked', '#staff-form').val()),
                email: $("#email").val(),
                id: $("#id").val(),

            }
            $.ajax({
                type: 'POST',
                url: '/api/Staff',
                contentType: 'application/json',
                data: JSON.stringify(data)
            }).done(function (response) {
                tempAlert("Thêm nhân viên thành công!", 2000);
                SearchStaff();
            }).fail(function (xhr, status, error) {
                if (xhr.responseText.includes("Staff not found!")) {
                    tempAlert("Nhân viên không tồn tại!", 2000);
                } else {
                    tempAlert("Lỗi không xác định!", 2000);
                }
            });
        }
        else if (window.curAction == "Remove") {
            var data = {
                Id: getInt($("#editingId").val())
            }
            $.ajax({
                type: 'DELETE',
                url: '/api/Staff',
                contentType: 'application/json',
                data: JSON.stringify(data)
            }).done(function (response) {
                tempAlert("Xóa nhân viên thành công!", 2000);
                SearchStaff();
            }).fail(function (xhr, status, error) {
                if (xhr.responseText.includes("Staff not found!")) {
                    tempAlert("Nhân viên không tồn tại!", 2000);
                } else {
                    tempAlert("Lỗi không xác định!", 2000);
                }
            });
        }
        else if (window.curAction == "Edit") {
            var data = {
                staffID: getInt($("#editingId").val()),
                name: $("#name").val(),
                companyID: getInt($('#companyID').val()),
                departmentID: getInt($('#departmentID').val()),
                role: $("#role").val(),
                startOfDate: $('#startOfDate').val(),
                staffType: getInt($('input[name=staffType]:checked', '#staff-form').val()),
                email: $("#email").val(),
                id: $("#id").val(),    

            }
            $.ajax({
                type: 'PUT',
                url: '/api/Staff',
                contentType: 'application/json',
                data: JSON.stringify(data)
            }).done(function (response) {
                tempAlert("Cập nhật nhân viên thành công!", 2000);
                SearchStaff();
            }).fail(function (xhr, status, error) {
                if (xhr.responseText.includes("Staff not found!")) {
                    tempAlert("Nhân viên không tồn tại!", 2000);
                } else {
                    tempAlert("Lỗi không xác định!", 2000);
                }
            });
        }
        $("#editModal").removeClass('backdrop');

        $("#editModal").modal('hide');
        $("#staffCheck").modal('hide');

    });

    $("#btnCancel").click(function () {
        $("#editModal").removeClass('backdrop');
        $("#staffCheck").modal('hide');
    });

    staffName.addEventListener('input', function () {
        SearchStaff();
    });

    $('#ListTable').after('<div id="nav" class="d-flex justify-content-center pt-0 page-nav"></div>');
    getStaffNames();
    SearchStaff();

});

// Example starter JavaScript for disabling form submissions if there are invalid fields
(function () {
    'use strict';
    window.addEventListener('load', function () {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function (form) {
            form.addEventListener('submit', function (event) {
                event.preventDefault();
                event.stopPropagation();
                if (form.checkValidity() === false) {
                }
                else {
                    $("#staffCheck").modal('show');
                    $("#editModal").addClass('backdrop');
                }
                form.classList.add('was-validated');
            }, false);
        });
    }, false);
})();


function getStaffNames() {
    $.get("/api/StaffNames", data = {}, function (response) {
        var staffNames = [];
        $.each(response, function (index, staff) {
            staffNames.push(staff.name);
        });
        autocomplete(document.getElementById("StaffName"), staffNames);

    });
}

function SearchStaffByID(id) {
    $("#editingId").val(id);
    var data = {
        ID: getInt(id),
    }
    $.ajax({
        type: 'POST',
        url: '/api/SearchStaffByID',
        contentType: 'application/json',
        /*data: JSON.stringify(data)*/
        data: JSON.stringify(data)
    }).done(function (response) {
        $("#name").val(response.name);
        document.querySelector('#companyID').value = response.companyID;
        document.querySelector('#departmentID').value = response.departmentID;
        $("#role").val(response.role);
        document.getElementById('startOfDate').valueAsDate = new Date(response.startOfDate);
        document.querySelectorAll('input[type=date]').forEach(inp => {
            inp.setAttribute(
                "data-date",
                moment(inp.value)
                    .format(inp.getAttribute("data-date-format")));
        });
        if (response.staffType == 0) {
            $("#choice-email").addClass("d-none");
            document.getElementById("email").required = false;
            $("#choice-id").removeClass("d-none");
            document.getElementById("id").required = true;
            $("#select-id").prop("checked", true);
            $("#select-email").prop("checked", false);
            $("#id").val(response.id);
        }
        else if (response.staffType == 1) {
            $("#choice-email").removeClass("d-none");
            document.getElementById("email").required = true;
            $("#choice-id").addClass("d-none");
            document.getElementById("id").required = false;
            $("#select-id").prop("checked", false);
            $("#select-email").prop("checked", true);
            $("#email").val(response.email);
        }

        $("#editModal").modal('show');
    });
}

var rowsShown = 10;

function SearchStaff() {
    var data = {
        companyID: getInt($("#Companies").val()),
        departmentID: getInt($("#Departments").val()),
        staffName: $("#StaffName").val(),
    }
    $.ajax({
        type: 'POST',
        url: '/api/SearchStaff',
        contentType: 'application/json',
        /*data: JSON.stringify(data)*/
        data: JSON.stringify(data)
    }).done(function (response) {
        bindSearch(response);
    });
}
$("#Companies").change(function () {
    SearchStaff();
});

$("#Departments").change(function () {
    SearchStaff();
});


function bindSearch(result) {
    var html = "";
    var idx = 0;
    $("#listSearch").html("");
    $.each(result.staff,
        function (index, model) {
            idx = idx + 1;
            html += "<tr>";

            html += "<td style='text-align: center;' data-a-h='center' data-b-a-s='thin' data-t='n'>" + idx + "</td>";
            html += "<td data-a-h='left' data-b-a-s='thin' class='name'>" + model.name + "</td>";

            html += "<td data-a-h='left' data-b-a-s='thin'>" + model.email + "</td>";
            html += "<td style='text-align: center;' data-a-h='center' data-b-a-s='thin'>" + model.companyId + "</td>";
            html += "<td data-a-h='left' data-b-a-s='thin' class='name'>" + model.department + "</td>";
            html += "<td style='text-align: center;' data-a-h='center' data-b-a-s='thin'> <a href='/Home/CreateQRCodeq?id=" +
                model.id + "' style='width:50%'><i class='fas fa-qrcode'></i> </td >";
            html += "     <td style='text-align: center;' data-a-h='center' data-b-a-s='thin'> <a href='#' onclick='EditStaff(" + model.id + ")' style='width:50%'><i class='fas fa-edit'></i></a> <a href='#' onclick='RemoveStaff(" + model.id + "," + idx + ")' style='width:50%'><i class='fas fa-trash-alt'></i></a> ";
            //html += "<td style='text-align: end;'>" + model.used + "</td>";
            //html += "<td class='d-none d-md-table-cell' style='text-align: end;'>" + model.available + "</td>";
            //html += "<td style='text-align: end;'>" + model.type + "</td>";
            /*  html += "<td style='text-align: center;' data-a-h='center' data-b-a-s='thin'>" + moment(model.date).format('DD/MM/YYYY HH:mm:ss') + "</td>";*/
            //html += "<td class='d-none d-md-table-cell' style='text-align: end;' data-a-h='right' data-b-a-s='thin' data-t='n'" + model.available + "</td>";

            html += "</tr>";

        });
    var rowsTotal = result.staff.length;

    if (rowsTotal % 10 != 0) {
        for (var i = rowsTotal % 10; i < 10; i++) {
            var idx = idx + 1;
            html += "<tr data-exclude='true' >";
            html += "<td style='text-align: center;' data-a-h='center' data-b-a-s='thin' data-t='n'>" + idx + "</td>";
            html += "<td></td>";
            html += "<td></td>";
            html += "<td></td>";
            html += "<td></td>";
            html += "<td></td>";
            html += "<td></td>";

            html += "</tr>";
        }
    }
    $("#listSearch").empty();
    $("#listSearch").append(html);
    $("#nav").empty();
    if (rowsTotal == 0) {
        $("#ListTable").addClass("h-100");
    } else {
        $("#ListTable").removeClass('h-100');
    }

    var numPages = Math.ceil(rowsTotal / rowsShown);
    $("#totalTransactions").html(rowsTotal);
    //if (rowsTotal > 1) {
    //    $('#nav').append('<em class="d-none d-sm-inline pl-2" style="left: 0; position: absolute;">Tổng số kết quả:'
    //        + '<em class="d-none d-sm-inline" style="font-weight:bold;"> ' + rowsTotal + '</em></em>');
    //}

    $('#nav').append('<span onclick="Previous()" class="d-none p-2 span-link" href="#" ">Quay lại</span>');
    for (i = 0; i < numPages; i++) {
        if (i == 2) {
            $('#nav').append('<span id="decoratorL" class="d-none p-2">...</span> ');
        } else if (i == numPages - 2) {
            $('#nav').append('<span id="decoratorR" class="d-none p-2">...</span> ');
        }
        var pageNum = i + 1;
        $('#nav').append('<a onclick="SelectPage(' + i + ')" class="p-2" href="#" rel="' + i + '" id="' + i + '">' + pageNum + '</a> ');
    }
    $('#nav').append('<span onclick="Next()" class="p-2 span-link" href="#" ">Tiếp theo</span>');
    if (numPages < 2) {
        $('#nav span:last').removeClass('d-block');
        $('#nav span:last').addClass('d-none');
    }

    $('#nav a').addClass('d-none');
    for (j = 0; j < 5; j++) {
        var nav = $('#' + j.toString());
        nav.removeClass('d-none');
        nav.addClass('d-block');
    }

    $('#ListTable tbody tr').hide();
    $('#ListTable tbody tr').slice(0, rowsShown).show();
    $('#nav a:first').addClass('active');
    if (numPages == 6) {
        $('#nav a:first').removeClass('d-none');
        $('#nav a:first').addClass('d-block');
    } else if (numPages > 6) {
        $('#decoratorR').removeClass('d-none');
        $('#decoratorR').addClass('d-block');
        $('#nav a:last').removeClass('d-none');
        $('#nav a:last').addClass('d-block');
    }

    //if (numPages > 7) {
    //    $('#nav').append('<input id="jumpId" style="width:50px;"></input>');
    //    $('#nav').append('<span onclick="PageJump()" class="p-2 span-link" href="#" ">Đến</span>');
    //}
    $('#nav a').bind('click', function () {
        $('#nav a').removeClass('active');
        $(this).addClass('active');
        var currPage = $(this).attr('rel');
        var startItem = currPage * rowsShown;
        var endItem = startItem + rowsShown;
        $('#ListTable tbody tr').css('opacity', '0.0').hide().slice(startItem, endItem).
            css('display', 'table-row').animate({ opacity: 1 }, 300);
    });
}

function SelectPage(i) {
    if (isNaN(i)) return false;
    var rowsTotal = $('#ListTable tbody tr').length;
    var numPages = Math.ceil(rowsTotal / rowsShown);

    var startIdx = i - 2;
    var endIdx = i + 3;

    if (startIdx < 0) {
        endIdx -= startIdx;
        startIdx = 0;
    }

    if (endIdx > numPages) {
        startIdx -= endIdx - numPages;
        endIdx = numPages;
    }

    $('#nav a').removeClass('d-block');
    $('#nav a').addClass('d-none');
    if (i > 3) {
        if (numPages > 6) {
            $('#decoratorL').removeClass('d-none');
            $('#decoratorL').addClass('d-block');
        }
        $('#nav a:first').removeClass('d-none');
        $('#nav a:first').addClass('d-block');
    } else if (i == 3) {
        $('#decoratorL').removeClass('d-block');
        $('#decoratorL').addClass('d-none');
        $('#nav a:first').removeClass('d-none');
        $('#nav a:first').addClass('d-block');
    } else {
        $('#decoratorL').removeClass('d-block');
        $('#decoratorL').addClass('d-none');
    }

    if (i < numPages - 4) {
        if (numPages > 6) {
            $('#decoratorR').removeClass('d-none');
            $('#decoratorR').addClass('d-block');
            $('#nav a:last').removeClass('d-none');
            $('#nav a:last').addClass('d-block');
        }
    } else if (i == numPages - 4) {
        $('#decoratorR').removeClass('d-block');
        $('#decoratorR').addClass('d-none');
        $('#nav a:last').removeClass('d-none');
        $('#nav a:last').addClass('d-block');
    } else {
        $('#decoratorR').removeClass('d-block');
        $('#decoratorR').addClass('d-none');
    }
    for (j = startIdx; j < endIdx; j++) {
        var nav = $('#' + j.toString());
        nav.removeClass('d-none');
        nav.addClass('d-block');
    }

    if (i != 0) {
        $('#nav span:first').removeClass('d-none');
        $('#nav span:first').addClass('d-block');
    } else {
        $('#nav span:first').removeClass('d-block');
        $('#nav span:first').addClass('d-none');
    }

    if (i != numPages - 1) {
        $('#nav span:last').removeClass('d-none');
        $('#nav span:last').addClass('d-block');
    } else {
        $('#nav span:last').removeClass('d-block');
        $('#nav span:last').addClass('d-none');
    }
    $('#ListTable tbody tr').hide();
    $('#ListTable tbody tr').slice(i * rowsShown, i * rowsShown + rowsShown).show();

    $('#nav a').removeClass('active');
    $('#' + i + '').addClass('active');
    var startItem = i * rowsShown;
    var endItem = startItem + rowsShown;
    $('#ListTable tbody tr').css('opacity', '0.0').hide().slice(startItem, endItem).
        css('display', 'table-row').animate({ opacity: 1 }, 300);
}

function PageJump() {
    SelectPage(parseInt(document.getElementById("jumpId").value));
}

function Previous() {
    var index = document.getElementsByClassName("active").item(0).innerHTML;
    SelectPage(parseInt(index) - 2);
}

function Next() {
    var index = document.getElementsByClassName("active").item(0).innerHTML;
    SelectPage(parseInt(index));
}

function dayInMonth(month, year, day) {
    return new Date(year, month, day);
}

function autocomplete(inp, arr) {
    /*the autocomplete function takes two arguments,
    the text field element and an array of possible autocompleted values:*/
    var currentFocus;
    /*execute a function when someone writes in the text field:*/
    inp.addEventListener("input", function (e) {
        var a, b, i, val = this.value;
        /*close any already open lists of autocompleted values*/
        closeAllLists();
        if (!val) { return false; }
        currentFocus = -1;
        /*create a DIV element that will contain the items (values):*/
        a = document.createElement("DIV");
        a.setAttribute("id", this.id + "autocomplete-list");
        a.setAttribute("class", "autocomplete-items");
        /*append the DIV element as a child of the autocomplete container:*/
        this.parentNode.appendChild(a);
        /*for each item in the array...*/
        for (i = 0; i < arr.length; i++) {
            var searchPos = arr[i].toLowerCase().search(val.toLowerCase());
            /*check if the item starts with the same letters as the text field value:*/
            if (searchPos != -1) {
                /*create a DIV element for each matching element:*/
                b = document.createElement("DIV");
                /*make the matching letters bold:*/
                b.innerHTML += arr[i].substring(0, searchPos) + "<strong>" + arr[i].substring(searchPos, val.length + searchPos) + "</strong>" + arr[i].substring(val.length + searchPos, arr[i].length);
                /*insert a input field that will hold the current array item's value:*/
                b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
                /*execute a function when someone clicks on the item value (DIV element):*/
                b.addEventListener("click", function (e) {
                    /*insert the value for the autocomplete text field:*/
                    inp.value = this.getElementsByTagName("input")[0].value;
                    /*close the list of autocompleted values,
                    (or any other open lists of autocompleted values:*/
                    closeAllLists();
                    SearchStaff();
                });
                a.appendChild(b);
            }
        }
    });
    /*execute a function presses a key on the keyboard:*/
    inp.addEventListener("keydown", function (e) {
        var x = document.getElementById(this.id + "autocomplete-list");
        if (x) x = x.getElementsByTagName("div");
        if (e.keyCode == 40) {
            /*If the arrow DOWN key is pressed,
            increase the currentFocus variable:*/
            currentFocus++;
            /*and and make the current item more visible:*/
            addActive(x);
        } else if (e.keyCode == 38) { //up
            /*If the arrow UP key is pressed,
            decrease the currentFocus variable:*/
            currentFocus--;
            /*and and make the current item more visible:*/
            addActive(x);
        } else if (e.keyCode == 13) {
            /*If the ENTER key is pressed, prevent the form from being submitted,*/
            e.preventDefault();
            if (currentFocus > -1) {
                /*and simulate a click on the "active" item:*/
                if (x) x[currentFocus].click();
            }
        }
    });

    function addActive(x) {
        /*a function to classify an item as "active":*/
        if (!x) return false;
        /*start by removing the "active" class on all items:*/
        removeActive(x);
        if (currentFocus >= x.length) currentFocus = 0;
        if (currentFocus < 0) currentFocus = (x.length - 1);
        /*add class "autocomplete-active":*/
        x[currentFocus].classList.add("autocomplete-active");
    }

    function removeActive(x) {
        /*a function to remove the "active" class from all autocomplete items:*/
        for (var i = 0; i < x.length; i++) {
            x[i].classList.remove("autocomplete-active");
        }
    }

    function closeAllLists(elmnt) {
        /*close all autocomplete lists in the document,
        except the one passed as an argument:*/
        var x = document.getElementsByClassName("autocomplete-items");
        for (var i = 0; i < x.length; i++) {
            if (elmnt != x[i] && elmnt != inp) {
                x[i].parentNode.removeChild(x[i]);
            }
        }
    }
    /*execute a function when someone clicks in the document:*/
    document.addEventListener("click", function (e) {
        closeAllLists(e.target);
    });
}

function AddStaff() {
    window.curAction = "Add";
    $("#announceHeader").html("Thêm nhân viên");
    $("#announceTitle").html("Xác nhận thêm nhân viên?");
    $("#name").val("");
    document.querySelector('#companyID').value = "";
    document.querySelector('#departmentID').value = "";
    $("#role").val("");
    document.getElementById('startOfDate').valueAsDate = new Date();
    document.querySelectorAll('input[type=date]').forEach(inp => {
        inp.setAttribute(
            "data-date",
            moment(inp.value)
                .format(inp.getAttribute("data-date-format")));
    });

    $("#select_id").prop("checked", false);
    $("#select_email").prop("checked", true);
    $("#id").val("");
    $("#email").val("");

    $("#editModal").modal('show');
}

function EditStaff(staffId) {
    window.curAction = "Edit";
    $("#announceHeader").html("Chỉnh sửa nhân viên");
    $("#announceTitle").html("Xác nhận cập nhật?");
    var staff = SearchStaffByID(staffId);
    
}

function RemoveStaff(staffId, idx) {
    var title = document.getElementsByTagName("tr")[idx].getElementsByTagName("td")[1].innerHTML;

    window.curAction = "Remove";
    $("#announceHeader").html("Xóa nhân viên");
    $("#announceTitle").html("Bạn có muốn xóa nhân viên " + title + " không ?");
    $("#editingId").val(staffId);
    $("#staffCheck").modal('show');
}

function PatchStaff(staffId) {
    var data = {
        Id: getInt(staffId)
    }
    $.ajax({
        type: 'PATCH',
        url: '/api/Staff',
        contentType: 'application/json',
        data: JSON.stringify(data)
    }).done(function (response) {
        tempAlert("Cập nhật QRCode nhân viên thành công!", 2000);
        SearchStaff();
    }).fail(function (xhr, status, error) {
        if (xhr.responseText.includes("Staff cannot be updated")) {
            tempAlert("Nhân viên này không thể cập nhật QRCode", 2000);
        } else {
            tempAlert("Lỗi không xác định!", 2000);
        }
    });
}

//var pdf, page_section, HTML_Width, HTML_Height, top_left_margin, PDF_Width, PDF_Height, canvas_image_width, canvas_image_height;

//function calculatePDF_height_width(selector, index) {
//    page_section = $(selector).eq(index);
//    HTML_Width = page_section.width();
//    HTML_Height = page_section.height();
//    top_left_margin = 15;
//    PDF_Width = HTML_Width + (top_left_margin * 2);
//    PDF_Height = (PDF_Width * 1.2) + (top_left_margin * 2);
//    canvas_image_width = HTML_Width;
//    canvas_image_height = HTML_Height;
//}

////window.jsPDF = window.jspdf.jsPDF;

////Generate PDF
//function generatePDF() {
//    console.log("Hehe");
//    var pdf = new jsPDF('p', 'pt', [580, 630]);
//    html2canvas($("#test")[0]).then(function (canvas) {
//        document.body.appendChild(canvas);
//        var ctx = canvas.getContext("2d");
//        var imgData = canvas.toDataURL("image/png", 1.0);
//        var width = canvas.width;
//        var height = canvas.clientHeight;
//        for (var i = 0; i < 5; i++) {
//            pdf.addPage(500, 500);
//            pdf.addImage(imgData, 'PNG', top_left_margin, top_left_margin, HTML_Width, HTML_Height);
//        }
//        pdf.save('sample.pdf');

//    });

//};
