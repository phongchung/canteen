﻿var selectedPage;
var getInt = a => isNaN(parseInt(a)) ? -1 : parseInt(a);

function tempAlert(msg, duration) {
    var content = document.getElementById("content_login");
    content.innerHTML = msg;
    setTimeout(function () {
        window.location.href = "#";
    }, duration);
    window.location.href = "#popup_login"
}

const outsideClickListener = (event) => {
    const $target = $(event.target);
    if (!$target.closest(".sidebar").length && $(".sidebar").is(':visible') && !$(".sidebar").hasClass("toggled")) {
        $("body").toggleClass("sidebar-toggled");
        $(".sidebar").toggleClass("toggled");
        $("#page-content-wrapper").toggleClass("toggled");
        if ($(".sidebar").hasClass("toggled")) {
            $('.sidebar .collapse').collapse('hide');
        };
    }
}

const removeClickListener = () => {
    document.removeEventListener('click', outsideClickListener)
}

const addClickListener = () => {
    document.addEventListener('click', outsideClickListener);
}

$(document).ready(function () {
    let width = screen.width;
    if (width < 768) {
        addClickListener();
        $("body").addClass("sidebar-toggled");
        $(".sidebar").addClass("toggled");
        $("#page-content-wrapper").toggleClass("toggled");
        $('.sidebar .collapse').collapse('hide');
    } else {
        //$("body").toggleClass("sidebar-toggled");
        //$(".sidebar").toggleClass("toggled");
        //$("#page-content-wrapper").toggleClass("toggled");
    }
    $("input[type=date]").on("change", function () {
        this.setAttribute(
            "data-date",
            moment(this.value)
                .format(this.getAttribute("data-date-format"))
        )
    }).trigger("change")

    $(".navbar-nav li").removeClass("activated");
    $(".navbar-nav li").eq($("#pageNumber").val()).addClass("activated");

    $("#accordionSidebar").find($("a")).click(function (evt) {
        evt.preventDefault();
        evt.stopPropagation();
        var link = $(this).attr("href");
        if ($(this).attr("id") == "staffListLink")
        {
            staffListLogin();
        }
        else {
            window.location.href = link;
        }
    });

    $("#btnLogin").click(function () {
        var data = {
            pw: $("#staffListLogin").val()
        }
        $.ajax({
            type: 'POST',
            url: '/api/StaffListLogin',
            contentType: 'application/json',
            data: JSON.stringify(data)
        }).done(function (response) {
            if (response == "Success!") {
                $("#loginModal").modal('hide');
                var link = $("#staffListLink").attr("href");
                window.location.href = link;
            }
        }).fail(function (xhr, status, error) {
            if (xhr.responseText.includes("Failed!")) {
                tempAlert("Sai mật khẩu nhân sự, vui lòng nhập lại!", 2000);
            }

        });

    });

    $("#staffListLogin").keypress(function (event) {
        if (event.keyCode === 13) {
            event.preventDefault();
            $("#btnLogin").click();
        }
    });

});


(function ($) {
    "use strict"; // Start of use strict

    // Toggle the side navigation
    $("#sidebarToggle, #sidebarToggleTop").on('click', function (e) {
        $("body").toggleClass("sidebar-toggled");
        $(".sidebar").toggleClass("toggled");
        $("#page-content-wrapper").toggleClass("toggled");
        if ($(".sidebar").hasClass("toggled")) {
            $('.sidebar .collapse').collapse('hide');
        };
    });

    // Close any open menu accordions when window is resized below 768px
    $(window).resize(function () {
        if ($(window).width() < 768) {
            addClickListener();
            $('.sidebar .collapse').collapse('hide');
        } else {
            removeClickListener();
        };
        // Toggle the side navigation when window is resized below 480px
        if ($(window).width() < 480 && !$(".sidebar").hasClass("toggled")) {
            $("body").addClass("sidebar-toggled");
            $(".sidebar").addClass("toggled");
            $("#page-content-wrapper").toggleClass("toggled");
            $('.sidebar .collapse').collapse('hide');
        };
    });

    // Prevent the content wrapper from scrolling when the fixed side navigation hovered over
    $('body.fixed-nav .sidebar').on('mousewheel DOMMouseScroll wheel', function (e) {
        if ($(window).width() > 768) {
            var e0 = e.originalEvent,
                delta = e0.wheelDelta || -e0.detail;
            this.scrollTop += (delta < 0 ? 1 : -1) * 30;
            e.preventDefault();
        }
    });

    // Scroll to top button appear
    $(document).on('scroll', function () {
        var scrollDistance = $(this).scrollTop();
        if (scrollDistance > 100) {
            $('.scroll-to-top').fadeIn();
        } else {
            $('.scroll-to-top').fadeOut();
        }
    });

    // Smooth scrolling using jQuery easing
    $(document).on('click', 'a.scroll-to-top', function (e) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: ($($anchor.attr('href')).offset().top)
        }, 1000, 'easeInOutExpo');
        e.preventDefault();
    });

})(jQuery); // End of use strict

// Close the dropdown if the user clicks outside of it
window.onclick = function (event) {
    if (!event.target.matches('.dropbtn') && !(event.target.matches('.dropspan'))) {
        var dropdowns = document.getElementsByClassName("dropdown-content");
        var i;
        for (i = 0; i < dropdowns.length; i++) {
            var openDropdown = dropdowns[i];
            if (openDropdown.classList.contains('show')) {
                openDropdown.classList.remove('show');
            }
        }
    }
}

$('.dropopen').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    $('.arrow:before').toggleClass('a1');
});

const myFunction = () => {
    document.getElementById("myDropdown").classList.toggle("show");
}

const updateTime = () => {
    var time = moment(new Date()).format('DD/MM/YYYY HH:mm:ss');

    // set CSS variable
    document.documentElement.style.setProperty(`--text`, `'${time}'`);
}

function staffListLogin() {
    $.ajax({
        type: 'GET',
        url: '/api/StaffListLogin',
        contentType: 'application/json',
        data: {}
    }).done(function (response) {
        if (response != "Logged!") {
            $("#loginModal").modal('show');
        } else {
            var link = $("#staffListLink").attr("href");
            window.location.href = link;
        }
    });
}

const activatePage = (x) => {
    window.selectedPage = x;
}

// initial call
updateTime();

// interval to update time
setInterval(updateTime, 1000);

//var home = document.getElementById('home').value;
//var foo = document.getElementById('footer');
//if (home == '1') {
//    foo.hidden = true;
//}
//else {
//    foo.hidden = false;
//}