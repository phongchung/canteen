﻿using Microsoft.Data.SqlClient;
using System;
using System.Threading;

namespace CanteenValidator.Data
{
    public class CustomConnectionChecker
    {

        public static bool IsConnectionEx(string connectionString)
        {
            bool result = false;
            try
            {
                using SqlConnection sqlConn = new SqlConnection(connectionString);
                Thread thread = new Thread(TryOpen);
                ManualResetEvent manualResetEvent = new ManualResetEvent(false);
                thread.Start(new Tuple<SqlConnection, ManualResetEvent>(sqlConn, manualResetEvent));
                result = manualResetEvent.WaitOne(2 * 1000);

                if (!result)
                {
                    thread.Abort();
                }

                sqlConn.Close();
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }

        private static void TryOpen(object input)
        {
            Tuple<SqlConnection, ManualResetEvent> parameters = (Tuple<SqlConnection, ManualResetEvent>)input;

            try
            {
                parameters.Item1.Open();
                parameters.Item1.Close();
                parameters.Item2.Set();
            }
            catch
            {
                // Eat any exception, we're not interested in it
            }
        }
    }
}
