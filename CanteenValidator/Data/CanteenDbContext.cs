﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using CanteenValidator.Models;

#nullable disable

namespace CanteenValidator.Data
{
    public partial class CanteenDbContext : DbContext
    {
        public CanteenDbContext()
        {
        }

        public CanteenDbContext(DbContextOptions<CanteenDbContext> options)
            : base(options)
        {
        }
        public virtual DbSet<Company> Companies { get; set; }
        public virtual DbSet<Ticket> Tickets { get; set; }
        public virtual DbSet<TicketType> TicketTypes { get; set; }
        public virtual DbSet<Transaction> Transactions { get; set; }
        public virtual DbSet<Staff> Staff { get; set; }
        public virtual DbSet<Holiday> Holidays { get; set; }
        public virtual DbSet<WorkDaysOfMonth> WorkDaysOfMonths { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<Company>(entity =>
            {
                entity.ToTable("LS_Company");

                entity.Property(e => e.Id)
                    .HasMaxLength(20)
                    .HasColumnName("ID");

                entity.Property(e => e.Name).HasMaxLength(50);
            });

            modelBuilder.Entity<Transaction>(entity =>
            {
                entity.ToTable("LUN_Transaction");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CheckDate).HasColumnType("datetime");

                entity.Property(e => e.Note).HasMaxLength(50);

                entity.Property(e => e.StaffId).HasColumnName("StaffID");

                entity.Property(e => e.TicketTypeId).HasColumnName("TicketTypeID");
            });

            modelBuilder.Entity<Staff>(entity =>
            {
                entity.ToTable("LS_Staff");

                entity.HasIndex(e => e.QrcodeId, "UQ__Staff__62FECDF30EEFC354")
                    .IsUnique();

                entity.HasIndex(e => e.QRHash, "UQ__Staff__D83AD8128E4F634E")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CompanyId)
                    .HasMaxLength(20)
                    .HasColumnName("CompanyID");

                entity.Property(e => e.Department).HasMaxLength(50);

                entity.Property(e => e.Email).HasMaxLength(50);

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.Property(e => e.QrcodeId)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("QRCodeID");

                entity.Property(e => e.Role).HasMaxLength(20);

                entity.Property(e => e.QRHash).HasMaxLength(20);

                entity.Property(e => e.StartOfDay).HasColumnType("date");

            });

            modelBuilder.Entity<Ticket>(entity =>
            {
                entity.ToTable("LUN_Ticket");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.StaffId).HasColumnName("StaffID");
            });

            modelBuilder.Entity<TicketType>(entity =>
            {
                entity.ToTable("LS_TicketType");

                entity.HasIndex(e => e.Name, "UQ__LS_Ticke__737584F65BBC1B7C")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(20);
            });

            modelBuilder.Entity<Holiday>(entity =>
            {
                entity.ToTable("LS_Holiday");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.FromDate).HasColumnType("date");

                entity.Property(e => e.ToDate).HasColumnType("date");

                entity.Property(e => e.Description).HasMaxLength(50);

            });

            modelBuilder.Entity<WorkDaysOfMonth>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("LS_WorkDaysOfMonth");
            });
            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
