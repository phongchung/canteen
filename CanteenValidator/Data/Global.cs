﻿using CanteenValidator.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CanteenValidator.Data
{
    public class Global
    {
        /// <summary>
        /// Global variable storing companies list.
        /// </summary>
        static List<Company> _companies_list;

        /// <summary>
        /// Get or set the static companies list.
        /// </summary>
        public static List<Company> companies_list
        {
            get
            {
                return _companies_list;
            }
            set
            {
                _companies_list = value;
            }
        }

        /// <summary>
        /// Global variable storing departments list.
        /// </summary>
        static List<string> _departments_list;

        /// <summary>
        /// Get or set the static departments list.
        /// </summary>
        public static List<string> departments_list
        {
            get
            {
                return _departments_list;
            }
            set
            {
                _departments_list = value;
            }
        }

        /// <summary>
        /// Global variable storing staffnames list.
        /// </summary>
        static List<StaffViewModel> _staffnames_list;

        /// <summary>
        /// Get or set the static staffnames list.
        /// </summary>
        public static List<StaffViewModel> staffnames_list
        {
            get
            {
                return _staffnames_list;
            }
            set
            {
                _staffnames_list = value;
            }
        }

        /// <summary>
        /// Global variable storing types list.
        /// </summary>
        static List<TicketType> _types_list;

        /// <summary>
        /// Get or set the static types list.
        /// </summary>
        public static List<TicketType> types_list
        {
            get
            {
                return _types_list;
            }
            set
            {
                _types_list = value;
            }
        }

        /// <summary>
        /// Global variable storing search model.
        /// </summary>
        static SearchModel _searchModel;

        /// <summary>
        /// Get or set the static search model.
        /// </summary>
        public static SearchModel searchModel
        {
            get
            {
                return _searchModel;
            }
            set
            {
                _searchModel = value;
            }
        }

        /// <summary>
        /// Global variable storing holidays login state.
        /// </summary>
        static bool _h_isLogged;

        /// <summary>
        /// Get or set the static holidays login state.
        /// </summary>
        public static bool h_isLogged
        {
            get
            {
                return _h_isLogged;
            }
            set
            {
                _h_isLogged = value;
            }
        }
    }
}
