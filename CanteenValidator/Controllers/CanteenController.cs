﻿using CanteenValidator.Data;
using CanteenValidator.Models;
using CanteenValidator.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CanteenValidator.Controllers
{
    [ApiController]
    public class CanteenController : ControllerBase
    {
        private readonly ICanteenService _canteenService;
        public CanteenController(ICanteenService canteenService)
        {
            _canteenService = canteenService;
        }

        // GET: api/Transactions
        [HttpGet]
        [Route("api/Transactions")]
        public IndexViewModel GetAllTransactions(int ID)
        {
            return _canteenService.GetAllTransactions(ID);
        }

        // PUT: api/Transactions
        [HttpPut]
        [Route("api/Transactions")]
        public async Task<string> AddTransactionsAsync(TransactionsUpdateModel model)
        {
            return await _canteenService.AddTransactionsAsync(model);
        }

        // DELETE: api/Transactions
        [HttpDelete]
        [Route("api/Transactions")]
        public async Task<string> DeleteTransactionAsync(DeleteModel model)
        {
            return await _canteenService.DeleteTransactionAsync(model.id);
        }

        // GET: api/Staff
        [HttpGet]
        [Route("api/Staff")]
        public async Task<IEnumerable<Staff>> GetStaffAsync()
        {
            return await _canteenService.GetStaffAsync();
        }

        // POST: api/Staff
        [HttpPost]
        [Route("api/Staff")]
        public async Task<string> AddStaffAsync(AddStaffViewModel model)
        {
            return await _canteenService.AddStaffAsync(model);
        }

        // PUT: api/Staff
        [HttpPut]
        [Route("api/Staff")]
        public async Task<string> EditStaffAsync(EditStaffViewModel model)
        {
            return await _canteenService.EditStaffAsync(model);
        }

        //// PATCH: api/Staff
        //[HttpPatch]
        //[Route("api/Staff")]
        //public async Task<string> UpdateStaffAsync(StaffViewModel model)
        //{
        //    return await _canteenService.UpdateStaffAsync(model);
        //}


        // DELETE: api/Staff
        [HttpDelete]
        [Route("api/Staff")]
        public async Task<string> DeleteStaffAsync(DeleteModel model)
        {
            return await _canteenService.DeleteStaffAsync(model);
        }

        // GET: api/StaffNames
        [HttpGet]
        [Route("api/StaffNames")]
        public IEnumerable<StaffViewModel> GetStaffNames()
        {
            return _canteenService.GetStaffNames();
        }

        // POST: api/Search
        [HttpPost]
        [Route("api/Search")]
        public SearchModel SearchTransactions(SearchGetModel model)
        {
            return _canteenService.SearchTransactions(model.companyID, model.departmentID, model.staffName, model.fromDate, model.toDate, -1);
        }

        // GET: api/Report
        [HttpPost]
        [Route("api/StaffReport")]
        public List<ReportViewModel> GetStaffReport(StaffReportSearchModel model)
        {
            return _canteenService.GetStaffReport(model.companyID, model.departmentID, -1, model.fromDate, model.toDate);
        }

        // GET: api/CompanyReport
        [HttpGet]
        [Route("api/CompanyReport")]
        public List<CompanyReportViewModel> GetCompanyReport(int companyID, int month, int year)
        {
            return _canteenService.GetCompanyReport(companyID, -1, month, year);
        }

        // GET: api/MonthlyReport
        [HttpGet]
        [Route("api/MonthlyReport")]
        public List<MonthlyReportModel> GetMonthlyReport(int month, int year)
        {
            return _canteenService.GetMonthlyReport(month, year);
        }

        // POST: api/DailyReport
        [HttpPost]
        [Route("api/DailyReport")]
        public async Task<DailyReportModel> GetDailyReportAsync(DailyReportSearchModel model)
        {
            return await _canteenService.GetDailyReportAsync(model.companyID, model.month, model.year);
        }

        // GET: api/Report
        [HttpPost]
        [Route("api/SearchSpecificTransaction")]
        public void SearchSpecificTransaction(SearchSpecificModel model)
        {
            _canteenService.SearchSpecificTransaction(model.company, model.staffID);
        }

        //// GET: api/Holidays
        //[HttpGet]
        //[Route("api/Holidays")]
        //public List<HolidayViewModel> GetHolidays()
        //{
        //    var holidays = _canteenService.GetHolidays();

        //    var holidaysViewModel = new List<HolidayViewModel>();

        //    holidays.ForEach(d =>
        //    {
        //        var holiday = new HolidayViewModel
        //        {
        //            Id = d.Id,
        //            FromDate = d.FromDate,
        //            ToDate = d.ToDate,
        //            Description = d.Description
        //        };

        //        if ((int)d.FromDate.DayOfWeek < 1)
        //        {
        //            holiday.FromDay = "(Chủ nhật)";
        //        }
        //        else holiday.FromDay = "(Thứ " + ((int)d.FromDate.DayOfWeek + 1) + ")";

        //        if ((int)d.ToDate.DayOfWeek < 1)
        //        {
        //            holiday.ToDay = "(Chủ nhật)";
        //        }
        //        else holiday.ToDay = "(Thứ " + ((int)d.ToDate.DayOfWeek + 1) + ")";
        //        holiday.TotalDays = (int)(holiday.ToDate - holiday.FromDate).TotalDays + 1;
        //        holidaysViewModel.Add(holiday);

        //    });
        //    return holidaysViewModel;
        //}

        //// POST: api/Holidays
        //[HttpPost]
        //[Route("api/Holidays")]
        //public async Task<string> AddHolidayAsync(Holiday model)
        //{
        //    Console.WriteLine(Request.HttpContext.Connection.RemoteIpAddress);
        //    return await _canteenService.AddHolidayAsync(model);
        //}

        //// PUT: api/Holidays
        //[HttpPut]
        //[Route("api/Holidays")]
        //public async Task<string> EditHolidayAsync(Holiday model)
        //{
        //    Console.WriteLine(Request.HttpContext.Connection.RemoteIpAddress);
        //    return await _canteenService.EditHolidayAsync(model);
        //}

        //// DELETE: api/Holidays
        //[HttpDelete]
        //[Route("api/Holidays")]
        //public async Task<string> RemoveHolidayAsync(Holiday model)
        //{
        //    Console.WriteLine(Request.HttpContext.Connection.RemoteIpAddress);
        //    return await _canteenService.RemoveHolidayAsync(model);
        //}

        // PUT: api/counter
        [HttpPut]
        [Route("api/counter")]
        public void Counter(CounterModel counter)
        {
            HttpContext.Session.SetInt32("counter", counter.ID);
        }

        // GET: api/counter
        [HttpGet]
        [Route("api/counter")]
        public int? Counter()
        {

            if (string.IsNullOrEmpty(HttpContext.Session.GetInt32("counter").ToString()))
            {
                return -1;
            }

            return HttpContext.Session.GetInt32("counter");
        }

        [HttpPost]
        [Route("api/SearchStaff")]
        public SearchModel SearchStaff(SearchGetModel model)
        {
            return _canteenService.SearchStaff(model.companyID, model.departmentID, model.staffName);
        }

        [HttpPost]
        [Route("api/SearchStaffByID")]
        public AddStaffViewModel SearchStaffByID(StaffViewModel model)
        {
            return _canteenService.SearchStaffByID(model);
        }

        // GET: api/StaffListLogin
        [HttpGet]
        [Route("api/StaffListLogin")]
        public string GetStaffListLoginState()
        {
            return _canteenService.GetStaffListLoginState();
        }

        // POST: api/StaffListLogin
        [HttpPost]
        [Route("api/StaffListLogin")]
        public string SetStaffListLoginState(LoginModel model)
        {
            return _canteenService.SetStaffListLoginState(model);
        }

        //// GET: api/HolidaysLogin
        //[HttpGet]
        //[Route("api/HolidaysLogin")]
        //public string GetHolidaysLoginState()
        //{
        //    return _canteenService.GetHolidaysLoginState();
        //}

        //// POST: api/HolidaysLogin
        //[HttpPost]
        //[Route("api/HolidaysLogin")]
        //public string SetHolidaysLoginState(LoginModel model)
        //{
        //    return _canteenService.SetHolidaysLoginState(model);
        //}


    }
}
