﻿using CanteenValidator.Models;
using CanteenValidator.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CanteenValidator.Controllers
{
    public class ReportController : Controller
    {
        private readonly ICanteenService _canteenService;

        public ReportController(ICanteenService canteenService)
        {
            _canteenService = canteenService;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Index(SearchModel model)
        {
            return View();
        }
    }
}