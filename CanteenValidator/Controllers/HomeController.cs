﻿using CanteenValidator.Data;
using CanteenValidator.Models;
using CanteenValidator.Services;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using PuppeteerSharp;
using PuppeteerSharp.Media;
using QRCoder;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace CanteenValidator.Controllers
{
    public class HomeController : Controller
    {
        private readonly ICanteenService _canteenService;

        static int NumberOfParticularDaysInMonth(int year, int month, DayOfWeek dayOfWeek)
        {
            DateTime startDate = new DateTime(year, month, 1);
            int totalDays = startDate.AddMonths(1).Subtract(startDate).Days;

            int answer = Enumerable.Range(1, totalDays)
                .Select(item => new DateTime(year, month, item))
                .Where(date => date.DayOfWeek == dayOfWeek)
                .Count();

            return answer;
        }

        public HomeController(ICanteenService canteenService)
        {
            _canteenService = canteenService;
        }

        public IActionResult Index()
        {
            Global.searchModel = new SearchModel();
            InitSearchModelAsync();

            return View();
        }

        private void InitSearchModelAsync()
        {
            Global.companies_list = _canteenService.GetCompanies();
            Global.departments_list = _canteenService.GetDepartments();
            Global.types_list = _canteenService.GetTicketTypes();

            List<Company> Companies = Global.companies_list;
            List<string> Departments = Global.departments_list;
            List<TicketType> Types = Global.types_list;

            Global.searchModel.SearchViewModel.Companies = Companies.Distinct().Select((x, idx) => new Company_Search()
            {
                Code = idx,
                Name = x.Id
            }).ToList();

            //Global.searchModel.SearchViewModel.Companies.Insert(0, new Company_Search()
            //{
            //    Code = -1,
            //    Name = "Tất cả công ty"
            //});

            Global.searchModel.SearchViewModel.Departments = Departments.Distinct().Select((x, idx) => new Department()
            {
                Code = idx,
                Name = x
            }).ToList();

            //Global.searchModel.SearchViewModel.Departments.Insert(0, new Department()
            //{
            //    Code = -1,
            //    Name = "Tất cả phòng ban"
            //});
            Global.searchModel.SearchViewModel.Types = Types.Distinct().Select((x, idx) => new ConsumerType()
            {
                Code = x.Id,
                Name = x.Name
            }).ToList();
            Global.searchModel.SearchViewModel.Types.Insert(0, new ConsumerType()
            {
                Code = -1,
                Name = "Đối tượng"
            });

            DateTime localDate = DateTime.Now;

            Global.searchModel.SearchViewModel.SelectedMonthCode = localDate.Month.ToString();
            Global.searchModel.SearchViewModel.SelectedYearCode = localDate.Year.ToString();
        }

        public IActionResult Search()
        {
            return View("Search", Global.searchModel);
        }
        public IActionResult ListStaff()
        {

            return View("ListStaff", Global.searchModel);
        }

        public IActionResult StaffReport()
        {
            return View("StaffReport", Global.searchModel);
        }
        public IActionResult CompanyReport()
        {
            return View("CompanyReport", Global.searchModel);
        }

        public IActionResult Workdays()
        {
            var workDays = _canteenService.GetWorkDays();

            ViewBag.WorkDays = workDays;

            return View("Workdays");
        }

        //public IActionResult Holidays()
        //{
        //    //var holidays = _canteenService.GetHolidays();

        //    //var holidaysViewModel = new List<HolidayViewModel>();

        //    //holidays.ForEach(d =>
        //    //{
        //    //    var holiday = new HolidayViewModel();

        //    //    holiday.FromDate = d.FromDate;
        //    //    holiday.ToDate = d.ToDate;
        //    //    holiday.Description = d.Description;
        //    //    if (d.FromDate.DayOfWeek == 0)
        //    //    {
        //    //        holiday.FromDay = "(Chủ nhật)";
        //    //    }
        //    //    else holiday.FromDay = "(Thứ " + d.FromDate.DayOfWeek + ")";

        //    //    if (d.ToDate.DayOfWeek == 0)
        //    //    {
        //    //        holiday.ToDay = "(Chủ nhật)";
        //    //    }
        //    //    else holiday.ToDay = "(Thứ " + d.ToDate.DayOfWeek + ")";
        //    //    holidaysViewModel.Add(holiday);

        //    //});

        //    //ViewBag.Holidays = holidaysViewModel;

        //    return View("Holidays");
        //}

        public IActionResult Report()
        {
            return View("Report");
        }

        public IActionResult Privacy()
        {
            return View();
        }

        //[HttpPost]
        //public IActionResult Scanned()
        //{
        //    return Content("Success");
        //}
        [HttpGet]
        public async Task<IActionResult> CreateQRCodeqAsync(int id)
        {
            Staff staff = await _canteenService.UpdateStaffAsync(new StaffViewModel()
            {
                ID = id,
            });
            QRCodeGenerator QrGenerator = new QRCodeGenerator();
            QRCodeData QrCodeInfo = QrGenerator.CreateQrCode(staff.QRHash, QRCodeGenerator.ECCLevel.Q);
            QRCode QrCode = new QRCode(QrCodeInfo);
            Bitmap QrBitmap = QrCode.GetGraphic(60);
            byte[] BitmapArray = QrBitmap.BitmapToByteArray();
            string QrUri = string.Format("data:image/png;base64,{0}", Convert.ToBase64String(BitmapArray));


            await new BrowserFetcher().DownloadAsync(BrowserFetcher.DefaultRevision);
            await using var browser = await Puppeteer.LaunchAsync(new LaunchOptions
            {
                Headless = true
            });
            await using var page = await browser.NewPageAsync();
            await page.EmulateMediaTypeAsync(MediaType.Screen);

            await page.SetContentAsync("<div style='display: flex; flex-wrap: wrap; width: 100%; justify-content: space-between'>" +
                                        "<div style='position: relative; border: 2px solid black; margin-bottom: 180px; width:100%; flex: 0 0 45%; max-width: 50%;'>" + 
                                        " <h3 style = 'text-align:center' >" + staff.Name + " </h3 >" +
                                         "<h3 style = 'text-align:center' >" + staff.CompanyId + " </h3 >" +
                                         "<h3 style = 'text-align:center; margin-bottom:5%' >" + staff.Department + "  </h3 >" +
                                          "   <img src = " + QrUri + " style = 'margin:0 auto;display:block;width:240px ' />" +
                                         " </div></div> ");
            //await page.SetContentAsync(" <img src="+QrUri+"  width='100% ' />");
            var pdfContent = await page.PdfStreamAsync(new PdfOptions
            {
                Format = PaperFormat.A4,
                PrintBackground = true
            });
            return File(pdfContent, "application/pdf", "converted.pdf");

        }

        [HttpGet]
        public async Task<IActionResult> CreateQRCodeAllAsync()
        {
            await new BrowserFetcher().DownloadAsync(BrowserFetcher.DefaultRevision);
            await using var browser = await Puppeteer.LaunchAsync(new LaunchOptions
            {
                Headless = true
            });
            await using var page = await browser.NewPageAsync();
            await page.EmulateMediaTypeAsync(MediaType.Screen);
            List<Staff> list = _canteenService.ListStaffType0();
            await _canteenService.UpdateAllStaffAsync();
            string a = "<div style='display: flex; flex-wrap: wrap; width: 100%; justify-content: space-between'>";
            foreach (var item in list)
            {
                QRCodeGenerator QrGenerator = new QRCodeGenerator();
                QRCodeData QrCodeInfo = QrGenerator.CreateQrCode(item.QRHash, QRCodeGenerator.ECCLevel.Q);
                QRCode QrCode = new QRCode(QrCodeInfo);
                Bitmap QrBitmap = QrCode.GetGraphic(60);
                byte[] BitmapArray = QrBitmap.BitmapToByteArray();
                string QrUri = string.Format("data:image/png;base64,{0}", Convert.ToBase64String(BitmapArray));
                a +=   "    <div style='position: relative; border: 2px solid black; margin-bottom: 180px; width:100%; flex: 0 0 45%; max-width: 50%;'>" +
                       "        <h3 style = 'text-align:center' >" + item.Name + " </h3 >" +
                       "        <h3 style = 'text-align:center' >" + item.CompanyId + " </h3 >" +
                       "        <h3 style = 'text-align:center; margin-bottom:4%' >" + item.Department + "  </h3 >" +
                       $"        <img src = {QrUri} style = 'margin: 0 auto;display:block;width:240px ' />" +
                       "    </div> ";
            }
            a += "</div>";
            await page.SetContentAsync(a);
            //await page.SetContentAsync(" <img src="+QrUri+"  width='100% ' />");
            var pdfContent = await page.PdfStreamAsync(new PdfOptions
            {
                Format = PaperFormat.A4,
                PrintBackground = true
            });
            return File(pdfContent, "application/pdf", "Qrcode.pdf");

        }

            
    }
}
public static class BitmapExtension
{
    public static byte[] BitmapToByteArray(this Bitmap bitmap)
    {
        using (MemoryStream ms = new MemoryStream())
        {
            bitmap.Save(ms, ImageFormat.Png);
            return ms.ToArray();
        }
    }
}