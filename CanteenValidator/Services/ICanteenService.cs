﻿using CanteenValidator.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CanteenValidator.Services
{
    public interface ICanteenService
    {
        IndexViewModel GetAllTransactions(int counterID);
        Task<string> AddTransactionsAsync(TransactionsUpdateModel model);
        Task<string> DeleteTransactionAsync(int id);
        Task<IEnumerable<Staff>> GetStaffAsync();
        Task<string> AddStaffAsync(AddStaffViewModel model);
        Task<string> EditStaffAsync(EditStaffViewModel model);
        Task<Staff> UpdateStaffAsync(StaffViewModel model);
        Task<string> UpdateAllStaffAsync();
        Task<string> DeleteStaffAsync(DeleteModel model);
        List<Company> GetCompanies();
        List<string> GetDepartments();
        List<StaffViewModel> GetStaffNames();
        List<TicketType> GetTicketTypes();
        List<WorkDaysOfMonth> GetWorkDays();
        List<Holiday> GetHolidays();
        SearchModel SearchTransactions(int companyID, int departmentID, string staffName, DateTime fromDate, DateTime toDate, int typeID);
        List<ReportViewModel> GetStaffReport(int companyID, int departmentID, int typeID, DateTime fromDate, DateTime toDate);
        List<CompanyReportViewModel> GetCompanyReport(int companyID, int typeID, int month, int year);
        List<MonthlyReportModel> GetMonthlyReport(int month, int year);
        Task<DailyReportModel> GetDailyReportAsync(int companyID, int month, int year);
        void SearchSpecificTransaction(string company, int staffID);
        Task<string> AddHolidayAsync(Holiday model);
        Task<string> EditHolidayAsync(Holiday model);
        Task<string> RemoveHolidayAsync(Holiday model);
        string GetHolidaysLoginState();
        string SetHolidaysLoginState(LoginModel model);
        string GetStaffListLoginState();
        string SetStaffListLoginState(LoginModel model);
        SearchModel SearchStaff(int companyID, int departmentID, string name);
        AddStaffViewModel SearchStaffByID(StaffViewModel model);
        List<Staff> ListStaffType0();
    }
}