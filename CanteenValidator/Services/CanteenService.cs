﻿using CanteenValidator.Data;
using CanteenValidator.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection.Metadata;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace CanteenValidator.Services
{
    public class CanteenService : ICanteenService
    {
        private readonly CanteenDbContext _context;

        public CanteenService(CanteenDbContext context)
        {
            _context = context;
        }

        public IndexViewModel GetAllTransactions(int counterID)
        {
            List<TransactionsViewModel> out_list = new List<TransactionsViewModel>();
            try
            {
                using (var command = _context.Database.GetDbConnection().CreateCommand())
                {

                    command.CommandText = "sp_LUN_GetAllTransactionsInfo";
                    command.CommandType = CommandType.StoredProcedure;

                    _context.Database.OpenConnection();
                    using var result = command.ExecuteReader();
                    while (result.Read())
                    {
                        TransactionsViewModel model = new TransactionsViewModel
                        {
                            ID = result.GetFieldValue<int>(0),
                            Name = result.GetFieldValue<string>(1),
                            Company = result.GetFieldValue<string>(2),
                            Department = result.GetFieldValue<string>(3),
                            Used = result.GetFieldValue<int>(4),
                            Available = result.GetFieldValue<int>(5),
                            Date = result.GetDateTime(6),
                            StaffID = result.GetFieldValue<int>(7),
                            Type = result.GetFieldValue<string>(8),
                            CounterID = result.GetFieldValue<int>(9)
                        };

                        out_list.Add(model);
                    }
                }
                var temp_list = out_list.Where(model =>
                model.CounterID == counterID);
                //out_list = temp_list.Skip(Math.Max(0, temp_list.Count() - 10)).ToList();
                out_list = temp_list.ToList();
                return new IndexViewModel()
                {
                    Count = out_list.Count,
                    Transactions = out_list
                };
            }
            catch (Exception ex)
            {
                Console.Write("Error info:" + ex.Message);
                throw new Exception(ex.Message);
            }
        }

        public async Task<IEnumerable<Staff>> GetStaffAsync()
        {
            return await _context.Staff.ToListAsync();
        }
        public async Task<string> AddStaffAsync(AddStaffViewModel model)
        {
            try
            {
                var curTime = DateTime.Now;
                var staff = _context.Staff.FirstOrDefault(x => x.Email == (model.staffType == 0 ? model.id : model.email));
                if (staff != null)
                {
                    throw new DuplicateNameException("Staff existed!");
                }

                Staff curStaff = new Staff()
                {
                    QRHash = MD5Hash((model.staffType == 0 ? model.id : model.email) + curTime.Day.ToString() +
                                                curTime.Month.ToString() + curTime.Year.ToString()),
                    Name = model.name,
                    QrcodeId = model.staffType == 0 ? model.id : model.email,
                    Email = model.staffType == 0 ? model.id : model.email,
                    CompanyId = Global.companies_list[model.companyID].Id,
                    Department = Global.departments_list[model.departmentID],
                    Role = model.role,
                    StartOfDay = model.startOfDate,
                    StaffType = model.staffType
                };
                _context.Staff.Add(curStaff);
                await _context.SaveChangesAsync();
                return "Success!";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<Staff> UpdateStaffAsync(StaffViewModel model)
        {
            try
            {
                var curTime = DateTime.Now;
                Staff staff = _context.Staff.FirstOrDefault(x => x.Id == model.ID);
                if(staff.StaffType == 1)
                {
                    
                }
                else
                {

                    staff.QRHash = MD5Hash(staff.Email + curTime.Day.ToString() +
                                                curTime.Month.ToString() + curTime.Year.ToString());
                    _context.Staff.Update(staff);
                    await _context.SaveChangesAsync();
                }
                return staff;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<string> UpdateAllStaffAsync()
        {
            try
            {
                var curTime = DateTime.Now;
                List<Staff> staff_list = _context.Staff.Where(x => x.StaffType == 0).ToList();
                foreach(var staff in staff_list)
                { 
                    staff.QRHash = MD5Hash(staff.Email + curTime.Day.ToString() +
                                                curTime.Month.ToString() + curTime.Year.ToString());
                    _context.Staff.Update(staff);
                }
                await _context.SaveChangesAsync();

                return "Success!";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<string> EditStaffAsync(EditStaffViewModel model)
        {
            try
            {
                Staff staff = _context.Staff.FirstOrDefault(x => x.Id == model.staffID);
                var curTime = DateTime.Now;

                if(staff == null)
                {
                    throw new ArgumentNullException("Staff not found!");
                }
                else
                {
                    staff.Name = model.name;
                    staff.CompanyId = Global.companies_list[model.companyID].Id;
                    staff.Department = Global.departments_list[model.departmentID];
                    staff.StaffType = model.staffType;
                    staff.Email = model.staffType == 0 ? model.id : model.email;
                    staff.Role = model.role;
                    staff.StartOfDay = model.startOfDate;
                    staff.QrcodeId = model.staffType == 0 ? model.id : model.email;
                    staff.QRHash = MD5Hash((model.staffType == 0 ? model.id : model.email) + curTime.Day.ToString() +
                                                curTime.Month.ToString() + curTime.Year.ToString());
                    _context.Staff.Update(staff);
                    await _context.SaveChangesAsync();

                    return "Success!";
                }
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<string> DeleteStaffAsync(DeleteModel model)
        {

            try
            {
                var staff = _context.Staff.FirstOrDefault(s => s.Id == model.id);

                if (staff == null)
                {
                    throw new ArgumentException("Staff not found!");
                }

                _context.Staff.Remove(staff);

                await _context.SaveChangesAsync();
                return "Success!";

            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public static string MD5Hash(string input)
        {
            StringBuilder hash = new StringBuilder();
            MD5CryptoServiceProvider md5provider = new MD5CryptoServiceProvider();
            byte[] bytes = md5provider.ComputeHash(new UTF8Encoding().GetBytes(input));

            for (int i = 0; i < bytes.Length; i++)
            {
                hash.Append(bytes[i].ToString("x2"));
            }
            return hash.ToString();
        }

        public async Task<string> AddTransactionsAsync(TransactionsUpdateModel model)
        {
            char[] separator = { '|' };
            var curTime = DateTime.Now;
            //Int32 count = 2;
            //String[] args = model.QRCode.Split(separator, count, StringSplitOptions.None);
            Staff staff = _context.Staff.FirstOrDefault(x => x.QRHash == model.QRCode);

            try
            {
                if (staff != null)
                {
                    var hasEaten = _context.Transactions.FirstOrDefault(x => x.StaffId == staff.Id && x.CheckDate.Day == curTime.Day);
                    //var ticketTypeId = _context.TicketTypes.FirstOrDefault(x => x.Name == args[1]);

                    if (hasEaten == null)
                    {
                        //if (args[1] == "Cá nhân")
                        //{
                        //    staffTicket.Used += 1;
                        //    staffTicket.Available -= 1;
                        //    _context.Tickets.Update(staffTicket);
                        //}
                        //staffTicket.Used += 1;
                        //staffTicket.Available -= 1;
                        //_context.Tickets.Update(staffTicket);
                        var transaction = new Transaction()
                        {
                            StaffId = staff.Id,
                            CheckDate = curTime,
                            Month = curTime.Month,
                            Year = curTime.Year,
                            Used = 1,
                            Available = 0,
                            TicketTypeId = 1,
                            CounterID = model.CounterID
                            //Note = args[3]
                        };

                        //if (args[1] == "Khách")
                        //{
                        //    transaction.Used = int.Parse(args[2]);
                        //    transaction.Available = 0;
                        //    transaction.TicketTypeId = 3;
                        //}
                        //else if(args[2] == "Ngoài giờ")
                        //{
                        //    transaction.Used = int.Parse(args[2]);
                        //    transaction.Available = 0;
                        //    transaction.TicketTypeId = 2;
                        //}


                        _context.Transactions.Add(transaction);
                        await _context.SaveChangesAsync();
                        return "Success!";
                    }
                    else
                    {
                        throw new ArgumentOutOfRangeException("Out of tickets!");
                    }

                }
                else
                {
                    throw new ArgumentException("ID not found!");
                }
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public async Task<string> DeleteTransactionAsync(int id)
        {
            int curMonth = DateTime.Now.Month;
            int curYear = DateTime.Now.Year;

            var query = from s in _context.Staff
                        join t in _context.Transactions on s.Id equals t.StaffId
                        where t.Id == id
                        select new Transaction()
                        {
                            CheckDate = t.CheckDate,
                            StaffId = s.Id,
                            Used = t.Used,
                            Available = t.Available,
                            TicketTypeId = t.TicketTypeId
                        };
            var result = await query.ToListAsync();

            try
            {

                //var staff = _context.Staff.FirstOrDefault(x => x.Id == result[0].StaffId);
                // if (staff != null)
                //{
                //    if (result[0].TicketTypeId == 1)
                //    {
                //        var staffTicket = _context.Tickets.FirstOrDefault(x => x.GrantedMonth == curMonth &&
                //                                        x.GrantedYear == curYear &&
                //                                        x.StaffId == staff.Id);
                //        staffTicket.Used -= 1;
                //        staffTicket.Available += 1;
                //        _context.Tickets.Update(staffTicket);
                //    }
                //}

                _context.Remove(result[0]);

                await _context.SaveChangesAsync();
                return "Success!";
            }
            catch (Exception ex)
            {
                Console.Write("Error info:" + ex.Message);
            }
            return "Failed!";

        }
        public List<Company> GetCompanies()
        {
            var query = from c in _context.Companies
                        select new Company()
                        {
                            Id = c.Id,
                            Name = c.Name
                        };
            return query.ToList();

        }
        public List<string> GetDepartments()
        {
            List<string> out_list = new List<string>();
            using (var command = _context.Database.GetDbConnection().CreateCommand())
            {
                command.CommandText = "sp_LUN_GetDepartments";
                command.CommandType = CommandType.StoredProcedure;
                _context.Database.OpenConnection();
                using var result = command.ExecuteReader();
                while (result.Read())
                {
                    out_list.Add(result.GetString(0));
                }
            }

            return out_list;
        }
        public List<StaffViewModel> GetStaffNames()
        {
            List<StaffViewModel> out_list = new List<StaffViewModel>();
            using (var command = _context.Database.GetDbConnection().CreateCommand())
            {
                command.CommandText = "sp_LUN_GetStaffNames";
                command.CommandType = CommandType.StoredProcedure;
                _context.Database.OpenConnection();
                using var result = command.ExecuteReader();
                while (result.Read())
                {
                    StaffViewModel model = new StaffViewModel
                    {
                        ID = result.GetFieldValue<int>(0),
                        Name = result.GetString(1)
                    };
                    out_list.Add(model);
                }
            }

            return out_list;
        }
        public List<TicketType> GetTicketTypes()
        {
            var query = from c in _context.TicketTypes
                        select new TicketType()
                        {
                            Id = c.Id,
                            Name = c.Name
                        };
            return query.ToList();
        }
        public List<WorkDaysOfMonth> GetWorkDays()
        {
            var query = from d in _context.WorkDaysOfMonths
                        select new WorkDaysOfMonth()
                        {
                            WorkMonth = d.WorkMonth,
                            WorkDays = d.WorkDays
                        };
            return query.ToList();
        }
        public List<Holiday> GetHolidays()
        {
            var query = from d in _context.Holidays
                        select new Holiday()
                        {
                            Id = d.Id,
                            FromDate = d.FromDate,
                            ToDate = d.ToDate,
                            Description = d.Description
                        };
            return query.ToList();
        }
        public SearchModel SearchTransactions(int companyID, int departmentID, string staffName, DateTime fromDate, DateTime toDate, int typeID)
        {
            var transactionsResult = new List<TransactionsViewModel>();
            using (var command = _context.Database.GetDbConnection().CreateCommand())
            {
                command.CommandText = "sp_LUN_SearchTransactions";
                command.CommandType = CommandType.StoredProcedure;

                var parameter_company = command.CreateParameter();
                parameter_company.ParameterName = "@Company";
                parameter_company.Value = "";
                if (companyID != -1)
                {
                    parameter_company.Value = Global.companies_list[companyID].Id;
                }

                var parameter_department = command.CreateParameter();
                parameter_department.ParameterName = "@Department";
                parameter_department.Value = "";
                if (departmentID != -1)
                {
                    parameter_department.Value = Global.departments_list[departmentID];
                }

                var parameter_staff = command.CreateParameter();
                parameter_staff.ParameterName = "@StaffName";
                parameter_staff.Value = "";
                if (staffName != null)
                {
                    parameter_staff.Value = staffName;
                }
                var parameter_fromDate = command.CreateParameter();
                parameter_fromDate.ParameterName = "@FromDate";
                parameter_fromDate.Value = fromDate;

                var parameter_toDate = command.CreateParameter();
                parameter_toDate.ParameterName = "@ToDate";
                parameter_toDate.Value = toDate;

                var parameter_type = command.CreateParameter();
                parameter_type.ParameterName = "@TypeId";
                parameter_type.Value = typeID;

                command.Parameters.Add(parameter_company);
                command.Parameters.Add(parameter_department);
                command.Parameters.Add(parameter_staff);
                command.Parameters.Add(parameter_fromDate);
                command.Parameters.Add(parameter_toDate);
                command.Parameters.Add(parameter_type);

                _context.Database.OpenConnection();
                using var result = command.ExecuteReader();
                while (result.Read())
                {
                    TransactionsViewModel model = new TransactionsViewModel
                    {
                        Company = result.GetFieldValue<string>(0),
                        Department = result.GetFieldValue<string>(1),
                        Name = result.GetFieldValue<string>(2),
                        Date = result.GetDateTime(3),
                        Used = result.GetFieldValue<int>(4),
                        Available = result.GetFieldValue<int>(5),
                        Type = result.GetFieldValue<string>(6)
                    };

                    transactionsResult.Add(model);
                }
            }

            Global.searchModel.TransactionsViewModel = transactionsResult;

            return Global.searchModel;
        }

        public List<ReportViewModel> GetStaffReport(int companyID, int departmentID, int typeID, DateTime fromDate, DateTime toDate)
        {
            List<ReportViewModel> out_list = new List<ReportViewModel>();
            using (var command = _context.Database.GetDbConnection().CreateCommand())
            {
                command.CommandText = "sp_LUN_GetReport";
                command.CommandType = CommandType.StoredProcedure;

                var parameter_company = command.CreateParameter();
                parameter_company.ParameterName = "@Company";
                parameter_company.Value = "";
                if (companyID != -1)
                {
                    parameter_company.Value = Global.companies_list[companyID].Id;
                }
                var parameter_department = command.CreateParameter();
                parameter_department.ParameterName = "@Department";
                parameter_department.Value = "";
                if (departmentID != -1)
                {
                    parameter_department.Value = Global.departments_list[departmentID];
                }

                var parameter_type = command.CreateParameter();
                parameter_type.ParameterName = "@TypeId";
                parameter_type.Value = typeID;

                var parameter_fromDate = command.CreateParameter();
                parameter_fromDate.ParameterName = "@FromDate";
                parameter_fromDate.Value = fromDate;

                var parameter_toDate = command.CreateParameter();
                parameter_toDate.ParameterName = "@ToDate";
                parameter_toDate.Value = toDate;

                command.Parameters.Add(parameter_company);
                command.Parameters.Add(parameter_department);
                command.Parameters.Add(parameter_type);
                command.Parameters.Add(parameter_fromDate);
                command.Parameters.Add(parameter_toDate);

                _context.Database.OpenConnection();
                using var result = command.ExecuteReader();
                while (result.Read())
                {
                    ReportViewModel report = new ReportViewModel
                    {
                        Name = result.GetFieldValue<string>(1),
                        Company = result.GetFieldValue<string>(2),
                        Department = result.GetFieldValue<string>(3),
                        Used = result.GetFieldValue<int>(4),
                        Available = result.GetFieldValue<int>(5),
                        Type = result.GetFieldValue<string>(6)
                    };

                    out_list.Add(report);
                }
            }

            return out_list;
        }

        public List<CompanyReportViewModel> GetCompanyReport(int companyID, int typeID, int month, int year)
        {
            List<CompanyReportViewModel> out_list = new List<CompanyReportViewModel>();
            using (var command = _context.Database.GetDbConnection().CreateCommand())
            {
                //command.CommandText = "sp_LUN_GetCompanyReport";
                //command.CommandType = CommandType.StoredProcedure;

                //var parameter_company = command.CreateParameter();
                //parameter_company.ParameterName = "@Company";
                //parameter_company.Value = "";
                //if (companyID != -1)
                //{
                //    parameter_company.Value = Global.companies_list[companyID].Id;
                //}
                //var parameter_department = command.CreateParameter();
                //parameter_department.ParameterName = "@Department";
                //parameter_department.Value = "";
                //if (departmentID != -1)
                //{
                //    parameter_department.Value = Global.departments_list[departmentID];
                //}

                //var parameter_type = command.CreateParameter();
                //parameter_type.ParameterName = "@TypeId";
                //parameter_type.Value = typeID;

                //var parameter_fromDate = command.CreateParameter();
                //parameter_fromDate.ParameterName = "@FromDate";
                //parameter_fromDate.Value = fromDate;

                //var parameter_toDate = command.CreateParameter();
                //parameter_toDate.ParameterName = "@ToDate";
                //parameter_toDate.Value = toDate;

                //command.Parameters.Add(parameter_company);
                //command.Parameters.Add(parameter_department);
                //command.Parameters.Add(parameter_type);
                //command.Parameters.Add(parameter_fromDate);
                //command.Parameters.Add(parameter_toDate);

                //_context.Database.OpenConnection();
                //using (var result = command.ExecuteReader())
                //{
                //    while (result.Read())
                //    {
                //        ReportViewModel report = new ReportViewModel();

                //        report.Name = result.GetFieldValue<string>(1);
                //        report.Company = result.GetFieldValue<string>(2);
                //        report.Department = result.GetFieldValue<string>(3);
                //        report.Used = result.GetFieldValue<int>(4);
                //        report.Available = result.GetFieldValue<int>(5);
                //        report.Type = result.GetFieldValue<string>(6);

                //        out_list.Add(report);
                //    }
                //}
            }

            return out_list;
        }

        public List<MonthlyReportModel> GetMonthlyReport(int month, int year)
        {
            List<MonthlyReportModel> out_list = new List<MonthlyReportModel>();
            using (var command = _context.Database.GetDbConnection().CreateCommand())
            {
                command.CommandText = "sp_LUN_GetMonthlyReport";
                command.CommandType = CommandType.StoredProcedure;
                var parameter_month = command.CreateParameter();
                parameter_month.ParameterName = "@Month";
                parameter_month.Value = month;

                var parameter_year = command.CreateParameter();
                parameter_year.ParameterName = "@Year";
                parameter_year.Value = year;

                command.Parameters.Add(parameter_month);
                command.Parameters.Add(parameter_year);

                _context.Database.OpenConnection();
                using var result = command.ExecuteReader();
                while (result.Read())
                {
                    MonthlyReportModel model = new MonthlyReportModel
                    {
                        Company = result.GetFieldValue<string>(0),
                        Used = result.GetFieldValue<int>(1)
                    };

                    out_list.Add(model);
                }
            }

            return out_list;
        }

        public async Task<DailyReportModel> GetDailyReportAsync(int companyID, int month, int year)
        {
            try
            {
                DailyReportModel result = new DailyReportModel();
                if (companyID != -1)
                {
                    result.CompanyList = Global.companies_list.Where(c => c.Id == Global.companies_list[companyID].Id).ToList();
                }
                else
                {
                    result.CompanyList = Global.companies_list;
                }
                int index;
                int totalDay = DateTime.DaysInMonth(year, month);

                var query = from s in _context.Staff
                            join t in _context.Transactions on s.Id equals t.StaffId
                            select new TransactionsViewModel()
                            {
                                Company = s.CompanyId,
                                Department = s.Department,
                                Name = s.Name,
                                Date = t.CheckDate,
                                Used = t.Used,
                                Available = t.Available
                            };

                var transactionsResult = new List<TransactionsViewModel>();

                transactionsResult = await query.Where(x => x.Date.Month == month &&
                                                                x.Date.Year == year).ToListAsync();

                for (index = 0; index < result.CompanyList.Count; index++)
                {
                    List<int> used = new List<int>(new int[totalDay]);
                    transactionsResult.ForEach(x =>
                    {
                        if (x.Company == result.CompanyList[index].Id)
                        {
                            used[x.Date.Day - 1] += 1;
                        }
                    });
                    result.UsedList.Add(used);
                }

                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public void SearchSpecificTransaction(string company, int staffID)
        {
            try
            {
                for (int i = 0; i < Global.companies_list.Count; i++)
                {
                    if (Global.companies_list[i].Id == company)
                    {
                        Global.searchModel.SearchViewModel.SelectedCompanyCode = i.ToString();
                        break;
                    }
                }
                var staff = _context.Staff.FirstOrDefault(x => x.Id == staffID);

                for (int i = 0; i < Global.departments_list.Count; i++)
                {
                    if (Global.departments_list[i] == staff.Department)
                    {
                        Global.searchModel.SearchViewModel.SelectedDepartmentCode = i.ToString();
                        break;
                    }
                }

                Global.searchModel.SearchViewModel.SelectedNameCode = staff.Name;
            }
            catch (Exception ex)
            {
                throw new ArgumentNullException(ex.Message);
            }

        }

        public async Task<string> AddHolidayAsync(Holiday model)
        {
            try
            {
                double count;
                bool existed = false;
                Holiday curHoliday;

                if (model.ToDate.Subtract(model.FromDate).TotalDays < 0)
                {
                    throw new ArgumentException("Invalid holiday!");
                }
                var holidays = GetHolidays();
                var temp_holiday = new Holiday();
                for (int i = 0; i < holidays.Count; i++)
                {
                    curHoliday = holidays[i];
                    if (curHoliday.Description != temp_holiday.Description)
                    {
                        temp_holiday.FromDate = curHoliday.FromDate;
                        temp_holiday.ToDate = curHoliday.ToDate;
                        if (curHoliday.ToDate.Day == DateTime.DaysInMonth(curHoliday.ToDate.Year, curHoliday.ToDate.Month))
                        {
                            continue;
                        }
                    }
                    else
                    {
                        if (curHoliday.ToDate.Day == DateTime.DaysInMonth(curHoliday.ToDate.Year, curHoliday.ToDate.Month))
                        {
                            continue;
                        }
                        else
                        {
                            temp_holiday.ToDate = curHoliday.ToDate;
                        }
                    }

                    if ((temp_holiday.FromDate <= model.FromDate && temp_holiday.ToDate >= model.FromDate) ||
                        (temp_holiday.FromDate <= model.ToDate && temp_holiday.ToDate >= model.ToDate))
                    {
                        existed = true;
                    }
                }
                if (existed)
                {
                    throw new ArgumentException("Holiday existed!");
                }

                count = model.ToDate.Subtract(model.FromDate).TotalDays;
                if (count > 90)
                {
                    throw new ArgumentException("Out of range date!");
                }

                _context.Holidays.Add(new Holiday
                {
                    FromDate = model.FromDate,
                    ToDate = model.ToDate,
                    Description = model.Description
                });

                await _context.SaveChangesAsync();
                return "Success!";

            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
        public async Task<string> EditHolidayAsync(Holiday model)
        {
            try
            {
                double count = 0;
                bool existed = false;
                Holiday curHoliday;

                if (model.ToDate.Subtract(model.FromDate).TotalDays < 0)
                {
                    throw new ArgumentException("Invalid holiday!");
                }
                var holidays = GetHolidays();

                var temp_holiday = new Holiday();
                for (int i = 0; i < holidays.Count; i++)
                {
                    if (holidays[i].Id == model.Id)
                    {
                        continue;
                    }
                    curHoliday = holidays[i];
                    if (curHoliday.Description != temp_holiday.Description)
                    {
                        temp_holiday.FromDate = curHoliday.FromDate;
                        temp_holiday.ToDate = curHoliday.ToDate;
                        count = temp_holiday.ToDate.Subtract(temp_holiday.FromDate).TotalDays;
                        if (curHoliday.ToDate.Day == DateTime.DaysInMonth(curHoliday.ToDate.Year, curHoliday.ToDate.Month))
                        {
                            continue;
                        }
                    }
                    else
                    {
                        if (curHoliday.ToDate.Day == DateTime.DaysInMonth(curHoliday.ToDate.Year, curHoliday.ToDate.Month))
                        {
                            count += curHoliday.ToDate.Day;
                            continue;
                        }
                        else
                        {
                            count += curHoliday.ToDate.Day;
                            temp_holiday.ToDate = curHoliday.ToDate;
                        }
                    }

                    if ((temp_holiday.FromDate <= model.FromDate && temp_holiday.ToDate >= model.FromDate) ||
                        (temp_holiday.FromDate <= model.ToDate && temp_holiday.ToDate >= model.ToDate))
                    {
                        existed = true;
                    }
                }
                if (existed)
                {
                    throw new ArgumentException("Holiday existed!");
                }

                if (count > 90)
                {
                    throw new ArgumentException("Out of range date!");
                }

                var holiday = holidays.FirstOrDefault(d => d.Id == model.Id);
                holiday = model;

                _context.Update(holiday);

                await _context.SaveChangesAsync();
                return "Success!";

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<string> RemoveHolidayAsync(Holiday model)
        {

            try
            {
                var holiday = _context.Holidays.FirstOrDefault(d => d.Id == model.Id);

                if (holiday == null)
                {
                    throw new ArgumentException("Holiday not found!");
                }

                _context.Remove(holiday);

                await _context.SaveChangesAsync();
                return "Success!";

            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public string GetHolidaysLoginState()
        {
            try
            {
                if (Global.h_isLogged)
                {
                    return "Logged!";
                }
                else return "Not logged!";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string SetHolidaysLoginState(LoginModel model)
        {
            try
            {

                if (model.pw == "itd_canteen_2021")
                {
                    Global.h_isLogged = true;
                    return "Success!";
                }
                else throw new ArgumentException("Failed!");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string GetStaffListLoginState()
        {
            try
            {
                if (Global.h_isLogged)
                {
                    return "Logged!";
                }
                else return "Not logged!";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string SetStaffListLoginState(LoginModel model)
        {
            try
            {

                if (model.pw == "itd_canteen_2021")
                {
                    Global.h_isLogged = true;
                    return "Success!";
                }
                else throw new ArgumentException("Failed!");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public SearchModel SearchStaff(int companyID, int departmentID, string name)
        {
            var list = _context.Staff.ToList();
            string company = "";
            string department = "";
            if ((name == null || name == "") && departmentID == -1 && companyID == -1)
            {
                Global.searchModel.Staff = list;
                return Global.searchModel;
            }

            if (companyID != -1)
            {
                company = Global.companies_list[companyID].Id;
            }
            if (departmentID != -1)
            {
                department = Global.departments_list[departmentID];
            }
            var temp = from s in _context.Staff
                       where (company == "" || company == s.CompanyId) && (department == "" || department == s.Department)
                       select s;

            if (name != null && name != "")
            {
                Global.searchModel.Staff = temp.Where(x => x.Name.ToLower().Contains(name.ToLower())).ToList();

                return Global.searchModel;
            }
            else
            {
                Global.searchModel.Staff = temp.ToList();
            }
            return Global.searchModel;
        }

        public AddStaffViewModel SearchStaffByID(StaffViewModel model)
        {
            try
            {
                Staff staff = _context.Staff.FirstOrDefault(x => x.Id == model.ID);
                int _companyID = 0, _departmentID = 0;
                for (int i = 0; i < Global.companies_list.Count; i++)
                {
                    if (Global.companies_list[i].Id == staff.CompanyId)
                    {
                        _companyID = i;
                        break;
                    }
                }
                for (int i = 0; i < Global.departments_list.Count; i++)
                {
                    if (Global.departments_list[i] == staff.Department)
                    {
                        _departmentID = i;
                        break;
                    }
                }

                return new AddStaffViewModel()
                {
                    name = staff.Name,
                    companyID = _companyID,
                    departmentID = _departmentID,
                    email = staff.StaffType == 0 ? "" : staff.Email,
                    id = staff.StaffType == 0 ? staff.Email : "",
                    staffType = staff.StaffType,
                    startOfDate = (DateTime)staff.StartOfDay,
                    role = staff.Role
                };
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Staff> ListStaffType0()
        {
            return _context.Staff.Where(x => x.StaffType == 0).ToList();
        }
    }
}
